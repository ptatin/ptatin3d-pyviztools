
import numpy as np
from time import perf_counter

import pyviztools as pvt
from pyviztools import simple_vts_writer
from pyviztools.ptatin_utils import *


def test_1():
  mx = 51
  my = 51
  mz = 51
  mesh = pvt.FEMeshQ1Vec(3, mx, my, mz)



  t0 = perf_counter()
  mesh.create_coor()
  dx = 1.0/float(mx-1)
  n = 0
  for k in range(mz+1):
    for j in range(my+1):
      for i in range(mx+1):
        mesh.coor[n,0] = i * dx
        mesh.coor[n,1] = j * dx
        mesh.coor[n,2] = k * dx
        n += 1
  t1 = perf_counter()
  print('done coor. time',t1-t0)

  mesh.coor[:,1] += 0.05 * np.sin(mesh.coor[:,0] * np.pi * 4.12)
  mesh.coor[:,2] += 0.075 * np.cos(mesh.coor[:,1] * np.pi * 2.32)
  mesh.coor[:,0] += 0.025 * np.sin(mesh.coor[:,1] * np.pi * 2.32) * np.cos(mesh.coor[:,2] * np.pi * 7.32)

  """
  t0 = perf_counter()
  mesh.create_e2v()
  t1 = perf_counter()
  print('done e2v. time',t1-t0)
  #print(mesh.element)
  """

  t0 = perf_counter()
  mesh.create_e2v()
  t1 = perf_counter()
  print('done e2v. time',t1-t0)
  #print(mesh.element)


  phi = mesh.create_vertex_field(dof=1)

  phi[:] = mesh.coor[:,0] * mesh.coor[:,0] + mesh.coor[:,1] * mesh.coor[:,2]
  phi[:] += 0.5 * np.sin(3.2 * mesh.coor[:,0]*np.pi) + np.exp(mesh.coor[:,2])


  #phi_c = mesh.interp2cell(phi)

  t0 = perf_counter()
  grad_phi_c = mesh.interpgrad2cell(phi)
  t1 = perf_counter()
  print('done interpgrad2cell. time',t1-t0)

  #simple_vts_writer("grid.vts", mesh, cellData={"phi_c": phi_c}, pointData={"phi": phi})

  simple_vts_writer("grid.vts", mesh,
                   cellData={
                    "dphi_x": grad_phi_c[:,0],
                    "dphi_y": grad_phi_c[:,1],
                    "dphi_z": grad_phi_c[:,2] },
                   pointData={
                    "phi": phi }
                   )


def test_interp():
  mx = 25
  my = 25
  mz = 25
  t_mesh = pvt.FEMeshQ1Vec(3, mx, my, mz)
  
  t0 = perf_counter()
  t_mesh.create_coor()
  dx = 1.0/float(t_mesh.m)
  n = 0
  for k in range(t_mesh.p+1):
    for j in range(t_mesh.n+1):
      for i in range(t_mesh.m+1):
        t_mesh.coor[n,0] = i * dx
        t_mesh.coor[n,1] = j * dx
        t_mesh.coor[n,2] = k * dx
        n += 1
  t1 = perf_counter()
  print('[t_mesh] done coor. time',t1-t0)

  #t_mesh.coor[:,1] += 0.05 * np.sin(t_mesh.coor[:,0] * np.pi * 4.12)
  #t_mesh.coor[:,2] += 0.075 * np.cos(t_mesh.coor[:,1] * np.pi * 2.32)
  #t_mesh.coor[:,0] += 0.025 * np.sin(t_mesh.coor[:,1] * np.pi * 2.32) * np.cos(t_mesh.coor[:,2] * np.pi * 7.32)

  t0 = perf_counter()
  t_mesh.create_e2v()
  t1 = perf_counter()
  print('[t_mesh] done e2v. time',t1-t0)

  mesh = pvt.FEMeshQ1Vec(3, 2*mx, 2*my, 2*mz)

  t0 = perf_counter()
  mesh.create_coor()
  dx = 1.0/float(mesh.m)
  n = 0
  for k in range(mesh.p+1):
    for j in range(mesh.n+1):
      for i in range(mesh.m+1):
        mesh.coor[n,0] = i * dx
        mesh.coor[n,1] = j * dx
        mesh.coor[n,2] = k * dx
        n += 1
  t1 = perf_counter()
  print('[v_mesh] done coor. time',t1-t0)

  #mesh.coor[:,1] += 0.05 * np.sin(mesh.coor[:,0] * np.pi * 4.12)
  #mesh.coor[:,2] += 0.075 * np.cos(mesh.coor[:,1] * np.pi * 2.32)
  #mesh.coor[:,0] += 0.025 * np.sin(mesh.coor[:,1] * np.pi * 2.32) * np.cos(mesh.coor[:,2] * np.pi * 7.32)
  
  t0 = perf_counter()
  mesh.create_e2v()
  t1 = perf_counter()
  print('[v_mesh] done e2v. time',t1-t0)
  
  
  temperature_q1 = t_mesh.create_vertex_field(dof=1)
  
  temperature_q1[:] = t_mesh.coor[:,0] * t_mesh.coor[:,0] + t_mesh.coor[:,1] * t_mesh.coor[:,2]
  temperature_q1[:] += 0.5 * np.sin(3.2 * t_mesh.coor[:,0]*np.pi) + np.exp(t_mesh.coor[:,2])
  #temperature_q1[:] = t_mesh.coor[:,1]
  
  simple_vts_writer("t_grid.vts", t_mesh,
                   pointData={
                   "temperature": temperature_q1 }
                   )


  t0 = perf_counter()
  temperature = t_mesh.interp_coarse_to_fine(temperature_q1, mesh)
  t1 = perf_counter()
  print('done interp_coarse_to_fine. time',t1-t0)


  simple_vts_writer("v_grid.vts", mesh,
                   pointData={
                   "temperature": temperature }
                   )

def test_extract_topo():
  mx = 32
  my = 34
  mz = 35
  mesh = pvt.FEMeshQ1Vec(3, mx, my, mz)
  
  
  
  t0 = perf_counter()
  mesh.create_coor()
  dx = 1.0/float(mx-1)
  n = 0
  for k in range(mz+1):
    for j in range(my+1):
      for i in range(mx+1):
        mesh.coor[n,0] = i * dx
        mesh.coor[n,1] = j * dx
        mesh.coor[n,2] = k * dx
        n += 1
  t1 = perf_counter()
  print('done coor. time',t1-t0)

  mesh.coor[:,1] += 0.05 * np.sin(mesh.coor[:,0] * np.pi * 4.12)
  mesh.coor[:,2] += 0.075 * np.cos(mesh.coor[:,1] * np.pi * 2.32)
  mesh.coor[:,0] += 0.025 * np.sin(mesh.coor[:,1] * np.pi * 2.32) * np.cos(mesh.coor[:,2] * np.pi * 7.32)
  
  t0 = perf_counter()
  mesh.create_e2v()
  t1 = perf_counter()
  print('done e2v. time',t1-t0)
  
  tcoor, tsize = mesh.extract_subset_face("jmax")
  
  simple_vts_writer("topo.vts", mesh, cellData=None, pointData=None)
  #simple_vts_writer("topo.vts", {"coor":mesh.coor, "nodesize":[mesh.m+1,mesh.n+1,mesh.p+1]}, cellData=None, pointData=None)

  simple_vts_writer("topo2.vts", {"coor":tcoor, "nodesize":tsize}, cellData=None, pointData=None)

  mapcoor, mapsize, topo = extract_topography(mesh)
  simple_vts_writer("topo3.vts", {"coor":mapcoor, "nodesize":mapsize}, cellData=None, pointData={"topo": topo})

  tcoor, tsize = mesh.extract_subset([0,-1], [20,-1], [0,-1])
  simple_vts_writer("subset.vts", {"coor":tcoor, "nodesize":tsize}, cellData=None, pointData=None)

  tcoor, tsize, _, field = mesh.extract_subset([0,-1], [20,-1], [0,-1], sample_rate=(4,2,2), pointData=mesh.coor[:,0])
  simple_vts_writer("subset2.vts", {"coor":tcoor, "nodesize":tsize}, cellData=None, pointData={"x": field})

  tcoor, tsize, _, field = mesh.extract_subset([mx,mx+1], [0,-1], [0,-1], sample_rate=(1,1,1), pointData=mesh.coor[:,0])
  simple_vts_writer("subset3.vts", {"coor":tcoor, "nodesize":tsize}, cellData=None, pointData={"x": field})


def test_vec():
  mx = 240
  my = 260
  mz = 280
  mesh = pvt.FEMeshQ1Vec(3, mx, my, mz)
  
  mesh.create_coor()
  dx = 1.0/float(mx-1)
  n = 0
  for k in range(mz+1):
    for j in range(my+1):
      for i in range(mx+1):
        mesh.coor[n,0] = i * dx
        mesh.coor[n,1] = j * dx
        mesh.coor[n,2] = k * dx
        n += 1

  mesh.create_e2v()

  
  phi = mesh.create_vertex_field(dof=1)
  phi[:] = mesh.coor[:,0] * mesh.coor[:,0] + mesh.coor[:,1] * mesh.coor[:,2]

  phi3 = mesh.create_vertex_field(dof=3)
  phi3[:, 0] = mesh.coor[:,0] * mesh.coor[:,0] + mesh.coor[:,1] * mesh.coor[:,2]
  phi3[:, 1] = mesh.coor[:,1] * mesh.coor[:,1]
  phi3[:, 2] = mesh.coor[:,2] * mesh.coor[:,2]

  phi5 = mesh.create_vertex_field(dof=5)
  for i in range(5):
    phi5[:, i] = mesh.coor[:,0] * mesh.coor[:,0] + mesh.coor[:,1] * mesh.coor[:,2]


  simple_vts_writer("grid.vts", mesh,
                   pointData={
                   "phi3": phi3,
                   "phi5": phi5}
                   )

  fcell = mesh.interp2cell(phi) # pass
  fcell3 = mesh.interp2cell(phi3) # pass
  #simple_vts_writer("grid.vts", mesh, cellData={"fc3":fcell3})

  fvert = mesh.cell_field_to_point_field(fcell)
  fvert3 = mesh.cell_field_to_point_field(fcell3)
  simple_vts_writer("grid.vts", mesh, cellData={"fc":fcell},pointData={"fv3":fvert3})

  grad = list()

  g = mesh.interpgrad2cell(phi3[:, 0])
  #simple_vts_writer("grid.vts", mesh, cellData={"gxx":g[:,0],"gxy":g[:,1],"gxz":g[:,2]})
  grad.append(g[:, 0])
  grad.append(g[:, 1])
  grad.append(g[:, 2])

  g = mesh.interpgrad2cell(phi3[:, 1])
  grad.append(g[:, 0])
  grad.append(g[:, 1])
  grad.append(g[:, 2])

  g = mesh.interpgrad2cell(phi3[:, 2])
  grad.append(g[:, 0])
  grad.append(g[:, 1])
  grad.append(g[:, 2])
  #simple_vts_writer("grid.vts", mesh, cellData={"gxx":grad[0],"gxy":grad[1],"gxz":grad[2]})
  simple_vts_writer("grid.vts", mesh, cellData={
                   "gxx":grad[0],"gxy":grad[1],"gxz":grad[2],
                   "gyx":grad[3],"gyy":grad[4],"gyz":grad[5],
                   "gzx":grad[6],"gzy":grad[7],"gzz":grad[8]})

  gg = mesh.compute_derivatives(phi)
  gg = mesh.compute_derivatives(phi3)
  simple_vts_writer("grid.vts", mesh, cellData={"gx":gg[0], "gy":gg[1], "gz":gg[2]})


if __name__ == '__main__':

  test_1()
  test_interp()
  test_extract_topo()
  test_vec()
  
