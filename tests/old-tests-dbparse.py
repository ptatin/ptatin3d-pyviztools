
import numpy as np
import sys, os, json
from time import perf_counter
import inspect

from pyviztools.dbparse import dtype_MPntStd, dtype_MPntPEnergy, dtype_MPntPStokes, dtype_MPntPStokesPl
from pyviztools.dbparse import pTatinDataBucket, pTatinSwarmDB
from pyviztools.dbparse import SwarmMask_Threshold_region_index

root = os.path.dirname(os.path.abspath(__file__))


def test_1():
  dir = os.path.join(root, "data/EPSL_POLC_100__checkpoints__initial_condition__r1")

  # Parse a JSON file and determine the mesh size for the Q2 mesh
  jfile = open(os.path.join(dir, "materialpoint_db.json"), "r")
  js = json.load(jfile)
  jfile.close()

  fname = None
  for f in js["DataBucket"]["fields"]:
    print(f["fieldName"])
    fname = f["fileName"]

  _npoints = js["DataBucket"]["partition"]["length"]
  npoints = 0
  for v in _npoints:
    npoints += v

  print('filename',fname)
  print('npoints',npoints)

  fname = os.path.join(dir,"materialpoint_db_data.bin")

  known_file_endian = "little"
  dtype_swarm = dict()
  dtype_swarm["MPntStd"]       = dtype_MPntStd(known_file_endian)
  dtype_swarm["MPntPEnergy"]   = dtype_MPntPEnergy(known_file_endian)
  dtype_swarm["MPntPStokes"]   = dtype_MPntPStokes(known_file_endian)
  dtype_swarm["MPntPStokesPl"] = dtype_MPntPStokesPl(known_file_endian)

  fp = open(fname, "rb")

  """
  field0 = js["DataBucket"]["fields"][0]
  fieldname0 = field0["fieldName"]
  print('loading',fieldname0,'--> npoints:',npoints)

  # need to read header: long int (nbytes)
  header_bytes = np.dtype("int64").itemsize
  data_swarm["MPntStd"] = np.fromfile(fp, dtype=dtype_swarm["MPntStd"], offset=header_bytes, count=npoints)
  """

  data_swarm = dict()
  fields = js["DataBucket"]["fields"]
  for f in fields:
    fieldname = f["fieldName"]
    print('loading',fieldname,'--> npoints (domain):',npoints)

    # need to read header: long int (nbytes)
    header_bytes = np.dtype("int64").itemsize
    
    rcnt = 0
    for rank_points in js["DataBucket"]["partition"]["length"]:
      print('  loading [rank',rcnt,'] --> (sub-domanin) npoints:',rank_points)
      data_swarm[fieldname] = np.fromfile(fp, dtype=dtype_swarm[fieldname], offset=header_bytes, count=rank_points)
      rcnt += 1

  fp.close()

  print(data_swarm)

  #print(data_swarm["MPntStd"]["wil"])



def test_2():
  dir = os.path.join(root, "data/EPSL_POLC_100__checkpoints__initial_condition__r1")
  
  swarm = pTatinSwarmDB()
  
  #swarm.load(os.path.join(dir, "materialpoint_db.json"))
  swarm.load(os.path.join(dir, "materialpoint_db.json"), ["MPntStd", "MPntPStokes"])
  
  #print(swarm.data_swarm)
  #print(swarm.data_swarm["MPntStd"][0])
  #print(swarm.data_swarm["MPntPEnergy"][0])

  #SwarmMask_Threshold_region_index(swarm, range(0,3))
  #SwarmMask_Threshold_region_index(swarm, 55)
  SwarmMask_Threshold_region_index(swarm, [0,3,4])

  swarm2 = swarm.extract_subset_from_mask()
  print(swarm2.data)
  print(swarm2.npoints)

  #swarm2 = swarm.extract_from_mask()
  #print(swarm2)
  #print(swarm2["MPntStd"])
  #print(swarm2["MPntPStokes"][0])



def test_3():
  dir = os.path.join(root, "data/EPSL_POLC_100__checkpoints__initial_condition__r1")
  
  swarm = pTatinSwarmDB()
  #swarm.load(os.path.join(dir, "materialpoint_db.json"), None)
  swarm.load(os.path.join(dir, "materialpoint_db.json"), ["MPntStd", "MPntPStokes"])
  
  print(swarm.db.data)
  print(swarm.db.data["MPntStd"][0])
  #print(swarm.db.data["MPntPEnergy"][0])

  mask = swarm.get_mask()
  mask[0:5] = True
  #print('m',mask[0:5])
  swarm.reset_mask()
  #print('m',mask[0:5])
  
  # Select points based on region
  select = [0, 3, 4]
  region = swarm.db.data["MPntStd"]["region"]
  for m in select:
    _idx = np.nonzero(region == m)
    mask[_idx] = True

  cnt = np.count_nonzero(swarm.mask == True)
  print('cnt',cnt)

  swarm2 = swarm.extract_subset_from_mask()
  print(swarm2.data)
  print(swarm2.npoints)



def test_4():
  
  dir = os.path.join(root, "data/EPSL_POLC_100__checkpoints__initial_condition__r1") # serial
  dir = os.path.join(root, "data/EPSL_POLC_100__checkpoints__initial_condition__r4") # parallel
  
  db = pTatinDataBucket()
  db.parse_json(os.path.join(dir,"materialpoint_db.json"))
  
  known_file_endian = sys.byteorder
  
  db.dtype_field["MPntStd"]       = dtype_MPntStd(known_file_endian)
  db.dtype_field["MPntPEnergy"]   = dtype_MPntPEnergy(known_file_endian)
  db.dtype_field["MPntPStokes"]   = dtype_MPntPStokes(known_file_endian)
  db.dtype_field["MPntPStokesPl"] = dtype_MPntPStokesPl(known_file_endian)
  
  print(db)
  
  print('db.data')
  print(db.data)
  
  db.load()
  print(db.data)
  print('[\"MPntStd\"][\"pid\"][0]', db.data["MPntStd"]["pid"][0])
  print('[\"MPntPStokes\"][\"eta\"][0]', db.data["MPntPStokes"]["eta"][0])
  print('[\"MPntPStokes\"][\"rho\"][0]', db.data["MPntPStokes"]["rho"][0])



def test_4_memmap(npoint_buffer_size = 1000000):
  
  dir = os.path.join(root, "data/EPSL_POLC_100__checkpoints__initial_condition__r1") # serial
  dir = os.path.join(root, "data/EPSL_POLC_100__checkpoints__initial_condition__r4") # parallel
  
  db = pTatinDataBucket()
  db.parse_json(os.path.join(dir,"materialpoint_db.json"))
  
  known_file_endian = sys.byteorder

  db.dtype_field["MPntStd"]       = dtype_MPntStd(known_file_endian)
  db.dtype_field["MPntPEnergy"]   = dtype_MPntPEnergy(known_file_endian)
  db.dtype_field["MPntPStokes"]   = dtype_MPntPStokes(known_file_endian)
  db.dtype_field["MPntPStokesPl"] = dtype_MPntPStokesPl(known_file_endian)
    
  #print(db)
  
  print('db.data')
  print(db.data)

  # load out of core
  def load_out_of_core(db, members=None):
    _members = list()
    if members is None: # load everything
      for f in db.field_name:
        _members.append(f)
    else:
      for m in members: # load a subset of the data
        _members.append(m)
    
    print('members',_members)

    datafile = db.field_filename[0]
    _datafile = os.path.basename(datafile)
    _path = os.path.dirname(db.json_filename)
    datafilename = os.path.join(_path, _datafile)
    print('memmap',datafilename)

    data_dict = dict()

    for fieldname in _members:
      
      byte_offset = 0
      for i in range(len(db.field_name)):
        regfield = db.field_name[i]
        header_bytes = np.dtype("int64").itemsize
        byte_offset += header_bytes
        if fieldname == regfield:
          break
        byte_offset += db.field_atomic_size[i] * db.npoints
      print('  loading', fieldname, 'byte offset', byte_offset)

      data = np.memmap(datafilename, dtype=db.dtype_field[fieldname], mode='r', offset=byte_offset, shape=db.npoints)
      data_dict[fieldname] = data
    
    
    return data_dict

  points = load_out_of_core(db)

  print('[\"MPntStd\"][\"pid\"][0]', points["MPntStd"]["pid"][0])
  print('[\"MPntPStokes\"][\"eta\"][0]', points["MPntPStokes"]["eta"][0])
  print('[\"MPntPStokes\"][\"rho\"][0]', points["MPntPStokes"]["rho"][0])

  for f in db.field_name:
    print('field',f,'indices [0,4)')
    print(points[f][0:4])

  # traverse mem-mapped file, loading chunksize points at a time
  chunksize = np.int64(npoint_buffer_size)
  if chunksize > db.npoints:
    chunksize = db.npoints
  nchunks = np.int64(db.npoints / chunksize)
  rem = db.npoints - chunksize * nchunks
  if rem > 0:
    nchunks += 1

  ranges = dict()
  ranges["cx"] = np.asarray([1.0e32, -1.0e32])
  ranges["cy"] = np.asarray([1.0e32, -1.0e32])
  ranges["cz"] = np.asarray([1.0e32, -1.0e32])
  ranges["region"] = np.asarray([1e4, -1e4], dtype=np.int32)

  t0 = perf_counter()
  for c in range(nchunks):
    start = c * chunksize
    end  = start + chunksize
    if end > db.npoints:
      end = db.npoints
    
    # Define indices to fetch from the mem-mapped file
    # Note that we use explict indices to force a copy of the data from file
    # into memory. If you used slice notation, then we might only obtain a view into the file.
    # A view is likely slow to work with since it will be performing many more file accesses.
    indices = np.linspace(start, end-1, end-start, dtype=np.int64)

    #print(start,end,end-start)
    #print(indices)

    # Field by field
    """
    x_copy = np.zeros(end-start, dtype=np.float64)
    y_copy = np.zeros(end-start, dtype=np.float64)
    z_copy = np.zeros(end-start, dtype=np.float64)
    region_copy = np.zeros(end-start, dtype=np.int32)

    x_copy[:] = points["MPntStd"]["coor"][indices, 0]
    y_copy[:] = points["MPntStd"]["coor"][indices, 1]
    z_copy[:] = points["MPntStd"]["coor"][indices, 2]
    region_copy[:] = points["MPntStd"]["region"][indices]
    """

    # All of MPntStd
    points_chunk = np.zeros(end-start, dtype=db.dtype_field["MPntStd"])
    points_chunk[:] = points["MPntStd"][indices]

    # Compute min, max of coords, and region
    ranges["cx"][0] = min(ranges["cx"][0], np.min(points_chunk["coor"][:, 0]))
    ranges["cx"][1] = max(ranges["cx"][1], np.max(points_chunk["coor"][:, 0]))

    ranges["cy"][0] = min(ranges["cy"][0], np.min(points_chunk["coor"][:, 1]))
    ranges["cy"][1] = max(ranges["cy"][1], np.max(points_chunk["coor"][:, 1]))

    ranges["cz"][0] = min(ranges["cz"][0], np.min(points_chunk["coor"][:, 2]))
    ranges["cz"][1] = max(ranges["cz"][1], np.max(points_chunk["coor"][:, 2]))

    ranges["region"][0] = min(ranges["region"][0], np.min(points_chunk["region"][:]))
    ranges["region"][1] = max(ranges["region"][1], np.max(points_chunk["region"][:]))

  t1 = perf_counter()

  print(ranges)
  print('Traversal time',t1-t0,'(sec) for npoints =',db.npoints, 'using a buffer size of',chunksize, 'nchunks =',nchunks)



def test_5_memmap(npoint_buffer_size = 1000000):
  
  dir = os.path.join(root, "data/EPSL_POLC_100__checkpoints__initial_condition__r1") # serial
  dir = os.path.join(root, "data/EPSL_POLC_100__checkpoints__initial_condition__r4") # parallel
  
  db = pTatinDataBucket()
  db.parse_json(os.path.join(dir,"materialpoint_db.json"))
  
  known_file_endian = sys.byteorder
  
  db.dtype_field["MPntStd"]       = dtype_MPntStd(known_file_endian)
  db.dtype_field["MPntPEnergy"]   = dtype_MPntPEnergy(known_file_endian)
  db.dtype_field["MPntPStokes"]   = dtype_MPntPStokes(known_file_endian)
  db.dtype_field["MPntPStokesPl"] = dtype_MPntPStokesPl(known_file_endian)
  
  #print(db)
  
  print('db.data')
  print(db.data)
  
  db.load_out_of_core()
  
  print('[\"MPntStd\"][\"pid\"][0]', db.data["MPntStd"]["pid"][0])
  print('[\"MPntPStokes\"][\"eta\"][0]', db.data["MPntPStokes"]["eta"][0])
  print('[\"MPntPStokes\"][\"rho\"][0]', db.data["MPntPStokes"]["rho"][0])
  
  for f in db.field_name:
    print('field',f,'indices [0,4)')
    print(db.data[f][0:4])
  
  db.load_out_of_core(members=["MPntPStokes"])
  print('db.data')
  print(db.data)
  
  print('[\"MPntPStokes\"][\"eta\"][0]', db.data["MPntPStokes"]["eta"][0])
  print('[\"MPntPStokes\"][\"rho\"][0]', db.data["MPntPStokes"]["rho"][0])
  
  for f in ["MPntPStokes"]:
    print('field',f,'indices [0,4)')
    print(db.data[f][0:4])



def test_3_ooc():
  dir = os.path.join(root, "data/EPSL_POLC_100__checkpoints__initial_condition__r4") # parallel
  
  swarm = pTatinSwarmDB()
  #swarm.load(os.path.join(dir, "materialpoint_db.json"), None, out_of_core=True)
  swarm.load(os.path.join(dir, "materialpoint_db.json"), ["MPntStd", "MPntPStokes"], out_of_core=True)
  print(swarm.db)
  
  print(swarm.db.data)
  print(swarm.db.data["MPntStd"][0])
  #print(swarm.db.data["MPntPEnergy"][0])
  
  mask = swarm.get_mask()
  swarm.reset_mask()
  
  # Select points based on region
  select = [0, 3]
  region = swarm.db.data["MPntStd"]["region"]
  for m in select:
    _idx = np.nonzero(region == m) # this many trigger a complete load into memory
    mask[_idx] = True
  
  cnt = np.count_nonzero(swarm.mask == True)
  print('cnt',cnt)

  swarm2 = swarm.extract_subset_from_mask()
  print(swarm2.data)
  print(swarm2.npoints)
  print(swarm2.db)
  print(swarm2.db.data["MPntStd"][0])

  def operation_demo_fetch_region_0(_indices, _points, _mask):
    print('batch size:',len(_indices))
    print(_indices[0:4],'...',_indices[-4::])
    #_mask[:] = True

    region = _points["MPntStd"]["region"]
    _idx = np.nonzero(region == 0)
    _mask[_idx] = True
  
  mask = swarm2.get_mask()
  mask[:]= False

  mask = swarm2.create_mask()
  swarm2.reset_mask(mask)
  swarm2.filter_iterator(operation_demo_fetch_region_0, mask=mask, batch_size=1000)
  swarm3 = swarm2.extract_subset_from_mask(mask)

  print(swarm3.data)
  print(swarm3.npoints)

  mask = swarm2.get_mask()
  print(mask);



if __name__ == '__main__':
  
  test_1()
  test_2()
  test_3()
  
  test_4()
  test_4_memmap()
  """
  test_4_memmap(npoint_buffer_size = 1000)
  test_4_memmap(npoint_buffer_size = 10000)
  test_4_memmap(npoint_buffer_size = 1000000)
  test_4_memmap(npoint_buffer_size = 10000000)
  """

  test_5_memmap()
  test_3_ooc()
