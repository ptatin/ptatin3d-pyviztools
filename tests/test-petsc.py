
#from pyviztools import petsc as petsc
import pyviztools.petsc as petsc


p = petsc.PETSc()

p.configure_from_package()
print(p)

# Export a petsc_conf.py file
p.emit_conf()
