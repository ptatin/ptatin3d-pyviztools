
import numpy as np
from time import perf_counter
import inspect

import cviztools as cvt



def test_debug():
  A = np.zeros(shape=(2, 3, 3))
  A[0][0][0] = 3
  A[0][0][1] = 1
  A[0][1][1] = 3
  A[0][2][2] = 3
  A[0][2][1] = 1

  A[1][0][0] = 2
  A[1][1][1] = 2
  A[1][2][2] = 2
  A[1][2][1] = 1
  print('A',A)


  AI = np.empty_like(A)
  for i in range(3):
    print('cross',i-2,i-1)
    c0 = i-2
    c1 = i-1
    if c0 < 0: c0 = 3+c0
    if c1 < 0: c1 = 3+c1
    print('->',c0,c1)
    
    AI[...,:,i] = np.cross(A[...,i-2,:], A[...,i-1,:])

  print(AI)
  x1x2 = AI[...,:,0]
  print('x1x2',x1x2)

  x0 = A[...,:,0]
  print('x0.shape',x0.shape)
  print('x0',x0)

  d0 = np.asarray([np.dot(x1x2[0], x0[0]), np.dot(x1x2[1], x0[1])])
  print(d0)

  print(np.linalg.inv(A[0]))
  print(np.linalg.inv(A[1]))

  print(AI[1]/d0[1])

  print(x1x2.shape)
  print(x0.shape)
  #de = np.dot( A[0,:,0],  AI[0,:,0])
  #de = np.dot( A[...,None,None],  AI)
  #print(de)


  print(AI/d0[...,None,None])

  print('x0',x0[0],x0[1])
  #de = np.linalg.dot( x1x2[...,:], x0[...,:])
  print(np.einsum('ij,ij->i',x1x2, x0))

  print('in',stacked_inverse_3x3(A))


def test_adj():
  A = np.asarray([[ 1.63941369e-01, -3.41976309e-02,  5.55047160e-03],
                  [-4.51332520e-03,  1.66666667e-01, -5.86337251e-02],
                  [ 2.33098733e-03,  2.77555756e-17,  1.66666667e-01]])
  print('A',A)
  print('iA',np.linalg.inv(A))

  B = np.zeros(shape=(1,3,3))
  B[0,:,:] = A
  print('iA',stacked_inverse_3x3(B))




def test_1():
  mx = 51
  my = 51
  mz = 51
  mesh = FEMeshQ1(3, mx, my, mz)



  t0 = perf_counter()
  mesh.create_coor()
  dx = 1.0/float(mx-1)
  n = 0
  for k in range(mz+1):
    for j in range(my+1):
      for i in range(mx+1):
        mesh.coor[n,0] = i * dx
        mesh.coor[n,1] = j * dx
        mesh.coor[n,2] = k * dx
        n += 1
  t1 = perf_counter()
  print('done coor. time',t1-t0)

  mesh.coor[:,1] += 0.05 * np.sin(mesh.coor[:,0] * np.pi * 4.12)
  mesh.coor[:,2] += 0.075 * np.cos(mesh.coor[:,1] * np.pi * 2.32)
  mesh.coor[:,0] += 0.025 * np.sin(mesh.coor[:,1] * np.pi * 2.32) * np.cos(mesh.coor[:,2] * np.pi * 7.32)

  """
  t0 = perf_counter()
  mesh.create_e2v()
  t1 = perf_counter()
  print('done e2v. time',t1-t0)
  #print(mesh.element)
  """

  t0 = perf_counter()
  mesh.create_e2v_fast()
  t1 = perf_counter()
  print('done e2v_fast. time',t1-t0)
  #print(mesh.element)


  phi = mesh.create_vertex_field(dof=1)

  phi[:] = mesh.coor[:,0] * mesh.coor[:,0] + mesh.coor[:,1] * mesh.coor[:,2]
  phi[:] += 0.5 * np.sin(3.2 * mesh.coor[:,0]*np.pi) + np.exp(mesh.coor[:,2])


  #phi_c = mesh.interp2cell_naive(phi)

  #phi_c = mesh.interp2cell_vec(phi, nvec=10000)


  t0 = perf_counter()
  #grad_phi_c = mesh.interpgrad2cell_naive(phi)
  #grad_phi_c = mesh.interpgrad2cell_vec(phi, nvec=10000)
  grad_phi_c = mesh.interpgrad2cell_vec2(phi, nvec=10000)
  t1 = perf_counter()
  print('done interpgrad2cell. time',t1-t0)

  for bs in [500, 1000, 2000, 6000, 8000, 10000, 60000]:
    t0 = perf_counter()
    #grad_phi_c = mesh.interpgrad2cell_vec2(phi, nvec=bs)
    t1 = perf_counter()
    #print('bs =',bs, 'done interpgrad2cell_naive. time',t1-t0,'(sec)')


  #simple_vts_writer("grid.vts", mesh, cellData={"phi_c": phi_c}, pointData={"phi": phi})

  simple_vts_writer("grid.vts", mesh,
                   cellData={
                    "dphi_x": grad_phi_c[:,0],
                    "dphi_y": grad_phi_c[:,1],
                    "dphi_z": grad_phi_c[:,2] },
                   pointData={
                    "phi": phi }
                   )


def test_interp():
  mx = 25
  my = 25
  mz = 25
  t_mesh = FEMeshQ1(3, mx, my, mz)
  
  t0 = perf_counter()
  t_mesh.create_coor()
  dx = 1.0/float(t_mesh.m)
  n = 0
  for k in range(t_mesh.p+1):
    for j in range(t_mesh.n+1):
      for i in range(t_mesh.m+1):
        t_mesh.coor[n,0] = i * dx
        t_mesh.coor[n,1] = j * dx
        t_mesh.coor[n,2] = k * dx
        n += 1
  t1 = perf_counter()
  print('[t_mesh] done coor. time',t1-t0)

  #t_mesh.coor[:,1] += 0.05 * np.sin(t_mesh.coor[:,0] * np.pi * 4.12)
  #t_mesh.coor[:,2] += 0.075 * np.cos(t_mesh.coor[:,1] * np.pi * 2.32)
  #t_mesh.coor[:,0] += 0.025 * np.sin(t_mesh.coor[:,1] * np.pi * 2.32) * np.cos(t_mesh.coor[:,2] * np.pi * 7.32)

  t0 = perf_counter()
  t_mesh.create_e2v_fast()
  t1 = perf_counter()
  print('[t_mesh] done e2v_fast. time',t1-t0)

  mesh = FEMeshQ1(3, 2*mx, 2*my, 2*mz)

  t0 = perf_counter()
  mesh.create_coor()
  dx = 1.0/float(mesh.m)
  n = 0
  for k in range(mesh.p+1):
    for j in range(mesh.n+1):
      for i in range(mesh.m+1):
        mesh.coor[n,0] = i * dx
        mesh.coor[n,1] = j * dx
        mesh.coor[n,2] = k * dx
        n += 1
  t1 = perf_counter()
  print('[v_mesh] done coor. time',t1-t0)

  #mesh.coor[:,1] += 0.05 * np.sin(mesh.coor[:,0] * np.pi * 4.12)
  #mesh.coor[:,2] += 0.075 * np.cos(mesh.coor[:,1] * np.pi * 2.32)
  #mesh.coor[:,0] += 0.025 * np.sin(mesh.coor[:,1] * np.pi * 2.32) * np.cos(mesh.coor[:,2] * np.pi * 7.32)
  
  t0 = perf_counter()
  mesh.create_e2v_fast()
  t1 = perf_counter()
  print('[v_mesh] done e2v_fast. time',t1-t0)
  
  
  temperature_q1 = t_mesh.create_vertex_field(dof=1)
  
  temperature_q1[:] = t_mesh.coor[:,0] * t_mesh.coor[:,0] + t_mesh.coor[:,1] * t_mesh.coor[:,2]
  temperature_q1[:] += 0.5 * np.sin(3.2 * t_mesh.coor[:,0]*np.pi) + np.exp(t_mesh.coor[:,2])
  #temperature_q1[:] = t_mesh.coor[:,1]
  
  simple_vts_writer("t_grid.vts", t_mesh,
                   pointData={
                   "temperature": temperature_q1 }
                   )


  t0 = perf_counter()
  temperature = t_mesh.interp_coarse_to_fine(temperature_q1, mesh)
  #temperature = t_mesh.interp_coarse_to_fine_fast(temperature_q1, mesh)
  t1 = perf_counter()
  print('done interp_coarse_to_fine. time',t1-t0)


  simple_vts_writer("v_grid.vts", mesh,
                   pointData={
                   "temperature": temperature }
                   )

def test_extract_topo():
  mx = 32
  my = 34
  mz = 35
  mesh = FEMeshQ1(3, mx, my, mz)
  
  
  
  t0 = perf_counter()
  mesh.create_coor()
  dx = 1.0/float(mx-1)
  n = 0
  for k in range(mz+1):
    for j in range(my+1):
      for i in range(mx+1):
        mesh.coor[n,0] = i * dx
        mesh.coor[n,1] = j * dx
        mesh.coor[n,2] = k * dx
        n += 1
  t1 = perf_counter()
  print('done coor. time',t1-t0)

  mesh.coor[:,1] += 0.05 * np.sin(mesh.coor[:,0] * np.pi * 4.12)
  mesh.coor[:,2] += 0.075 * np.cos(mesh.coor[:,1] * np.pi * 2.32)
  mesh.coor[:,0] += 0.025 * np.sin(mesh.coor[:,1] * np.pi * 2.32) * np.cos(mesh.coor[:,2] * np.pi * 7.32)
  
  t0 = perf_counter()
  mesh.create_e2v_fast()
  t1 = perf_counter()
  print('done e2v_fast. time',t1-t0)
  
  tcoor, tsize = mesh.extract_subset_face("jmax")
  
  simple_vts_writer("topo.vts", mesh, cellData=None, pointData=None)
  #simple_vts_writer("topo.vts", {"coor":mesh.coor, "nodesize":[mesh.m+1,mesh.n+1,mesh.p+1]}, cellData=None, pointData=None)

  simple_vts_writer("topo2.vts", {"coor":tcoor, "nodesize":tsize}, cellData=None, pointData=None)

  mapcoor, mapsize, topo = extract_topography(mesh)
  simple_vts_writer("topo3.vts", {"coor":mapcoor, "nodesize":mapsize}, cellData=None, pointData={"topo": topo})

  tcoor, tsize = mesh.extract_subset([0,-1], [20,-1], [0,-1])
  simple_vts_writer("subset.vts", {"coor":tcoor, "nodesize":tsize}, cellData=None, pointData=None)

  tcoor, tsize, _, field = mesh.extract_subset([0,-1], [20,-1], [0,-1], sample_rate=(4,2,2), pointData=mesh.coor[:,0])
  simple_vts_writer("subset2.vts", {"coor":tcoor, "nodesize":tsize}, cellData=None, pointData={"x": field})

  tcoor, tsize, _, field = mesh.extract_subset([mx,mx+1], [0,-1], [0,-1], sample_rate=(1,1,1), pointData=mesh.coor[:,0])
  simple_vts_writer("subset3.vts", {"coor":tcoor, "nodesize":tsize}, cellData=None, pointData={"x": field})


def test_vec():
  mx = 240
  my = 260
  mz = 280
  mesh = FEMeshQ1(3, mx, my, mz)
  
  mesh.create_coor()
  dx = 1.0/float(mx-1)
  n = 0
  for k in range(mz+1):
    for j in range(my+1):
      for i in range(mx+1):
        mesh.coor[n,0] = i * dx
        mesh.coor[n,1] = j * dx
        mesh.coor[n,2] = k * dx
        n += 1

  mesh.create_e2v_fast()

  
  phi = mesh.create_vertex_field(dof=1)
  phi[:] = mesh.coor[:,0] * mesh.coor[:,0] + mesh.coor[:,1] * mesh.coor[:,2]

  phi3 = mesh.create_vertex_field(dof=3)
  phi3[:, 0] = mesh.coor[:,0] * mesh.coor[:,0] + mesh.coor[:,1] * mesh.coor[:,2]
  phi3[:, 1] = mesh.coor[:,1] * mesh.coor[:,1]
  phi3[:, 2] = mesh.coor[:,2] * mesh.coor[:,2]

  phi5 = mesh.create_vertex_field(dof=5)
  for i in range(5):
    phi5[:, i] = mesh.coor[:,0] * mesh.coor[:,0] + mesh.coor[:,1] * mesh.coor[:,2]


  simple_vts_writer("grid.vts", mesh,
                   pointData={
                   "phi3": phi3,
                   "phi5": phi5}
                   )

  fcell = mesh.interp2cell_vec(phi) # pass
  fcell3 = mesh.interp2cell_vec(phi3) # pass
  #simple_vts_writer("grid.vts", mesh, cellData={"fc3":fcell3})

  fvert = mesh.cell_field_to_point_field(fcell)
  fvert3 = mesh.cell_field_to_point_field(fcell3)
  simple_vts_writer("grid.vts", mesh, cellData={"fc":fcell},pointData={"fv3":fvert3})

  grad = list()

  g = mesh.interpgrad2cell_vec2(phi3[:, 0])
  #simple_vts_writer("grid.vts", mesh, cellData={"gxx":g[:,0],"gxy":g[:,1],"gxz":g[:,2]})
  grad.append(g[:, 0])
  grad.append(g[:, 1])
  grad.append(g[:, 2])

  g = mesh.interpgrad2cell_vec2(phi3[:, 1])
  grad.append(g[:, 0])
  grad.append(g[:, 1])
  grad.append(g[:, 2])

  g = mesh.interpgrad2cell_vec2(phi3[:, 2])
  grad.append(g[:, 0])
  grad.append(g[:, 1])
  grad.append(g[:, 2])
  #simple_vts_writer("grid.vts", mesh, cellData={"gxx":grad[0],"gxy":grad[1],"gxz":grad[2]})
  simple_vts_writer("grid.vts", mesh, cellData={
                   "gxx":grad[0],"gxy":grad[1],"gxz":grad[2],
                   "gyx":grad[3],"gyy":grad[4],"gyz":grad[5],
                   "gzx":grad[6],"gzy":grad[7],"gzz":grad[8]})

  gg = mesh.compute_derivatives(phi)
  gg = mesh.compute_derivatives(phi3)
  simple_vts_writer("grid.vts", mesh, cellData={"gx":gg[0], "gy":gg[1], "gz":gg[2]})

  gg = mesh.compute_derivatives_c(phi)
  gg = mesh.compute_derivatives_c(phi3)


def PetscVecLoadFromFile(fname):
  import PetscBinaryIO as pio # This file has been copied from the petsc source tree

  io = pio.PetscBinaryIO() # Instantiate a petsc binary loader
  with open(fname) as fp:
    objecttype = io.readObjectType(fp)
    v = io.readVec(fp)
  return v


def PetscVecLoadFromFile_native_python(fname):
  fp = open(fname, "rb")
  h1 = np.fromfile(fp, dtype=np.dtype(">i4"), count=1)
  h2 = np.fromfile(fp, dtype=np.dtype(">i4"), count=1)
  print('vecid',h1, 'length',h2)
  v = np.fromfile(fp, dtype=np.dtype(">f8"), count=int(h2))
  print(v[0])
  fp.close()
  return v


def PetscVecLoadFromFile_custom_load_byte_swap(fname):
  import ctypes
  from numpy.ctypeslib import ndpointer


  fp = open(fname, "rb")
  
  # load the header as big endian "int"
  h1 = np.fromfile(fp, dtype=np.dtype(">i4"), count=1)
  h2 = np.fromfile(fp, dtype=np.dtype(">i4"), count=1)
  print('vecid',h1, 'length',h2)
  
  # Load the data as little endian "double"
  # This should be fast (but result will look wrong)
  v = np.fromfile(fp, dtype=np.dtype("<f8"), count=int(h2))
  
  print('big endian data',v[0])

  # Call the byte swapping c code for float64
  lib = ctypes.cdll.LoadLibrary("./byte_swap.so")

  c_method_byteswap64 = lib.byte_swap_float64
  c_method_byteswap64.restype = None
  c_method_byteswap64.argtypes = [
                            ctypes.c_size_t,
                            ndpointer(ctypes.c_double, flags="C_CONTIGUOUS")
                            ]

  c_method_byteswap64(v.size, v)

  print('little endian data',v[0])
  fp.close()
  return v


def FEMeshQ1VertexFieldLoadFromPetscVec(mesh, fname, bs=1):
  x = mesh.create_vertex_field(dof=bs)
  _x = PetscVecLoadFromFile(fname)
  if bs != 1:
    x[:,:] = np.reshape(_x, newshape=(mesh.nv, bs))
  else:
    x[:] = _x[:]
  return x


def FEMeshQ1CellFieldLoadFromPetscVec(mesh, fname, bs=1):
  x = mesh.create_cell_field(dof=bs)
  _x = PetscVecLoadFromFile(fname)
  if bs != 1:
    x[:,:] = np.reshape(_x, newshape=(mesh.ne, bs))
  else:
    x[:] = _x[:]
  return x


def FEMeshQ1CreateFromPetscVec(mx, my, mz, coor_fname=None):
  mt0 = perf_counter()
  mesh = FEMeshQ1(3, mx, my, mz)
  mesh.create_e2v_fast()

  if coor_fname is not None:
    mesh.create_coor()
    
    coor = PetscVecLoadFromFile(coor_fname)
    mesh.coor[:,:] = np.reshape(coor, newshape=(mesh.nv, 3))
    
    if mesh.is_vertex_field(mesh.coor) == False:
      print('Coordinate file load',coor_fname,'is not consistent with mesh size specified')
  mt1 = perf_counter()
  dt = mt1 - mt0
  print('->',inspect.currentframe().f_code.co_name + '()','execution time:',('%1.4e'%dt),'(sec)')

  return mesh


def ptatin_demo():
  import os
  import json
  
  dir = "/Users/dmay/codes/ptatin-dev/ptatin-ckp/EPSL_POLC_100-ckp/checkpoints/initial_condition"
  
  # Parse a JSON file and determine the mesh size for the Q2 mesh
  jfile = open(os.path.join(dir, "stokes_v_dmda.json"), "r")
  js = json.load(jfile)
  jfile.close()
  
  nvert_i = js["DMDA"]["directions"][0]["points"]
  nvert_j = js["DMDA"]["directions"][1]["points"]
  nvert_k = js["DMDA"]["directions"][2]["points"]
  
  mx = nvert_i - 1
  my = nvert_j - 1
  mz = nvert_k - 1
  
  # Load up the mesh coordinates for the Q2 mesh, load the velocity vector
  mesh = FEMeshQ1CreateFromPetscVec(mx, my, mz, os.path.join(dir,"stokes_v_dmda_coords.pbvec"))
  u = FEMeshQ1VertexFieldLoadFromPetscVec(mesh, os.path.join(dir, "ptatinstate_stokes_Xv.pbvec"), 3)
  
  # Create a Q1 mesh, load a temperature vector
  mesh_t = FEMeshQ1CreateFromPetscVec(int(mx/2), int(my/2), int(mz/2), None)
  t = FEMeshQ1VertexFieldLoadFromPetscVec(mesh_t, os.path.join(dir, "ptatinstate_energy_Xt.pbvec"))
  
  # Interpolate the temperature vector from the Q1 mesh onto the Q2 mesh
  tQ2 = mesh_t.interp_coarse_to_fine(t, mesh)
  
  # Compute grad(v), and then the symmetric gradient
  _grad = mesh.compute_derivatives(u)
  grad_v = {"gu":_grad[0], "gv":_grad[1], "gw":_grad[2]}
  
  sym_grad_v = { "exx":None, "exy":None, "exz":None, "eyy":None, "eyz":None, "ezz":None }
  sym_grad_v["exx"] = grad_v["gu"][:,0]
  sym_grad_v["eyy"] = grad_v["gv"][:,1]
  sym_grad_v["ezz"] = grad_v["gw"][:,2]
  
  sym_grad_v["exy"] = 0.5*(grad_v["gu"][:,1] + grad_v["gv"][:,0])
  sym_grad_v["exz"] = 0.5*(grad_v["gu"][:,2] + grad_v["gw"][:,0])
  sym_grad_v["eyz"] = 0.5*(grad_v["gv"][:,2] + grad_v["gw"][:,1])
  
  # Compute second invariant
  e2 = sym_grad_v["exx"]**2 + sym_grad_v["eyy"]**2 + sym_grad_v["ezz"]**2
  e2 += 2.0 * (sym_grad_v["exy"]**2 + sym_grad_v["exz"]**2 + sym_grad_v["eyz"]**2)
  e2 = np.sqrt(0.5 * e2)
  
  # Write a paraview file
  def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z
  z = merge_two_dicts(sym_grad_v, {"e2":e2})
  
  simple_vts_writer("grid.vts.gz", mesh, pointData={"vel": u, "t": tQ2}, cellData=z)
  
  # Slice and down-sample u and t
  """
  tcoor, tsize, _, slice_t = mesh.extract_subset([0,-1], [0,-1], [10,11], sample_rate=(1, 1, 1), pointData=tQ2)
  tcoor, tsize, _, slice_u = mesh.extract_subset([0,-1], [0,-1], [10,11], sample_rate=(1, 1, 1), pointData=u)
  tcoor, tsize, slice_e2, _ = mesh.extract_subset([0,-1], [0,-1], [10,11], sample_rate=(1, 1, 1), cellData=e2)
  simple_vts_writer("subset3.vts", {"coor": tcoor, "nodesize": tsize}, cellData={"e2": slice_e2}, pointData={"t": slice_t, "u": slice_u})
  """
  
  cellInput = {"e2": e2}
  pointInput = {"t": tQ2, "u": u }
  tcoor, tsize, cellSlice, pointSlice = mesh.extract_subset([0,-1], [0,-1], [10,11], sample_rate=(1, 1, 1), pointData=pointInput, cellData=cellInput)
  simple_vts_writer("subset3.vts", {"coor": tcoor, "nodesize": tsize}, cellData=cellSlice, pointData=pointSlice)



def ptatin_demo_p0field():
  import os
  import json


  dir = "/Users/dmay/codes/ptatin-dev/ptatin-ckp/EPSL_POLC_100-ckp-2/checkpoints/initial_condition"
  
  # Parse a JSON file and determine the mesh size for the Q2 mesh
  jfile = open(os.path.join(dir, "stokes_v_dmda.json"), "r")
  js = json.load(jfile)
  jfile.close()
  
  nvert_i = js["DMDA"]["directions"][0]["points"]
  nvert_j = js["DMDA"]["directions"][1]["points"]
  nvert_k = js["DMDA"]["directions"][2]["points"]
  
  mx = nvert_i - 1
  my = nvert_j - 1
  mz = nvert_k - 1
  
  # Load up the mesh coordinates for the Q2 mesh, load the velocity vector
  mesh = FEMeshQ1CreateFromPetscVec(mx, my, mz, os.path.join(dir,"stokes_v_dmda_coords.pbvec"))
  u = FEMeshQ1VertexFieldLoadFromPetscVec(mesh, os.path.join(dir, "ptatinstate_stokes_Xv.pbvec"), 3)
  
  # Compute grad(v), and then the symmetric gradient
  _grad = mesh.compute_derivatives(u)
  grad_v = {"gu":_grad[0], "gv":_grad[1], "gw":_grad[2]}
  
  sym_grad_v = { "exx":None, "exy":None, "exz":None, "eyy":None, "eyz":None, "ezz":None }
  sym_grad_v["exx"] = grad_v["gu"][:,0]
  sym_grad_v["eyy"] = grad_v["gv"][:,1]
  sym_grad_v["ezz"] = grad_v["gw"][:,2]
  
  sym_grad_v["exy"] = 0.5*(grad_v["gu"][:,1] + grad_v["gv"][:,0])
  sym_grad_v["exz"] = 0.5*(grad_v["gu"][:,2] + grad_v["gw"][:,0])
  sym_grad_v["eyz"] = 0.5*(grad_v["gv"][:,2] + grad_v["gw"][:,1])
  
  # Compute second invariant
  e2 = sym_grad_v["exx"]**2 + sym_grad_v["eyy"]**2 + sym_grad_v["ezz"]**2
  e2 += 2.0 * (sym_grad_v["exy"]**2 + sym_grad_v["exz"]**2 + sym_grad_v["eyz"]**2)
  e2 = np.sqrt(0.5 * e2)
  print('size.e2',e2.shape)

  tcoor, tsize, subsample_e2, _ = mesh.extract_subset([0,-1], [0,-1], [0,-1], sample_rate=(2, 2, 2), cellData=e2)
  print('size.e2[sub-sample]',subsample_e2.shape)

  # Create a Q1 mesh, load a temperature vector
  mx_q2 = int(mx/2)
  my_q2 = int(my/2)
  mz_q2 = int(mz/2)
  
  mesh_t = FEMeshQ1CreateFromPetscVec(mx_q2, my_q2, mz_q2, None)
  temperature = FEMeshQ1VertexFieldLoadFromPetscVec(mesh_t, os.path.join(dir, "ptatinstate_energy_Xt.pbvec"))

  # We did not load temperature coords, so insert t mesh coords as determine by the sub-sampling step
  mesh_t.coor = tcoor

  # region files unfortunately don't live in same location as the checkpoint data... lame
  dir = "/Users/dmay/codes/ptatin-dev/ptatin-ckp/EPSL_POLC_100-ckp-2"
  stepname = "step0"
  
  filename = os.path.join(dir, stepname, stepname + ".dmda-cell.region.vec")
  cell_region_f = FEMeshQ1CellFieldLoadFromPetscVec(mesh_t, filename, bs=1)
  cell_region = np.zeros(cell_region_f.shape, dtype=np.int32)
  cell_region = cell_region_f.astype(np.int32)

  filename = os.path.join(dir, stepname, stepname + ".dmda-cell.density.vec")
  cell_density = FEMeshQ1CellFieldLoadFromPetscVec(mesh_t, filename, bs=1)

  filename = os.path.join(dir, stepname, stepname + ".dmda-cell.viscosity.vec")
  cell_viscosity = FEMeshQ1CellFieldLoadFromPetscVec(mesh_t, filename, bs=1)

  simple_vts_writer("cellfields.vts.gz", mesh_t,
                    pointData={
                      "t": temperature
                    },
                    cellData = {
                      "region": cell_region,
                      "viscosity": cell_viscosity,
                      "density": cell_density,
                      "e2": subsample_e2,
                      }
                    )


  # Add any slices as before
  irange = [0, -1]
  jrange = [0, -1]
  krange = [10, 11]
  
  """
  slice_coor, slice_size, slice_temperature    = mesh_t.extract_subset(irange, jrange, krange, pointData=temperature)
  slice_coor, slice_size, slice_cell_region    = mesh_t.extract_subset(irange, jrange, krange, cellData=cell_region)
  slice_coor, slice_size, slice_cell_viscosity = mesh_t.extract_subset(irange, jrange, krange, cellData=cell_viscosity)
  slice_coor, slice_size, slice_cell_density   = mesh_t.extract_subset(irange, jrange, krange, cellData=cell_density)
  slice_coor, slice_size, slice_subsample_e2   = mesh_t.extract_subset(irange, jrange, krange, cellData=subsample_e2)
  
  simple_vts_writer("cellfields_slice.vts", {"coor":slice_coor, "nodesize":slice_size},
                    pointData={
                      "t": slice_temperature,
                    },
                    cellData={
                      "region": slice_cell_region,
                      "viscosity": slice_cell_viscosity,
                      "density": slice_cell_density,
                      "e2": slice_subsample_e2,
                    },
                    )
  """

  cellInput = {"region": cell_region, "viscosity": cell_viscosity, "density": cell_density, "e2": subsample_e2}
  
  pointInput = {"t": temperature }
  
  tcoor, tsize, cellSlice, pointSlice = mesh_t.extract_subset(irange, jrange, krange, pointData=pointInput, cellData=cellInput)
  
  simple_vts_writer("cellfields_slice.vts", {"coor": tcoor, "nodesize": tsize}, cellData=cellSlice, pointData=pointSlice)



def ptatin_demo_c_compute_derivs():
  import os
  import json
  
  
  dir = "/Users/dmay/codes/ptatin-dev/ptatin-ckp/EPSL_POLC_100-ckp/checkpoints/initial_condition"
  
  # Parse a JSON file and determine the mesh size for the Q2 mesh
  jfile = open(os.path.join(dir, "stokes_v_dmda.json"), "r")
  js = json.load(jfile)
  jfile.close()
  
  nvert_i = js["DMDA"]["directions"][0]["points"]
  nvert_j = js["DMDA"]["directions"][1]["points"]
  nvert_k = js["DMDA"]["directions"][2]["points"]
  
  mx = nvert_i - 1
  my = nvert_j - 1
  mz = nvert_k - 1
  
  # Load up the mesh coordinates for the Q2 mesh, load the velocity vector
  mesh = FEMeshQ1CreateFromPetscVec(mx, my, mz, os.path.join(dir,"stokes_v_dmda_coords.pbvec"))
  u = FEMeshQ1VertexFieldLoadFromPetscVec(mesh, os.path.join(dir, "ptatinstate_stokes_Xv.pbvec"), 3)
  
  # Create a Q1 mesh, load a temperature vector
  mesh_t = FEMeshQ1CreateFromPetscVec(int(mx/2), int(my/2), int(mz/2), None)
  t = FEMeshQ1VertexFieldLoadFromPetscVec(mesh_t, os.path.join(dir, "ptatinstate_energy_Xt.pbvec"))
  
  # Interpolate the temperature vector from the Q1 mesh onto the Q2 mesh
  tQ2 = mesh_t.interp_coarse_to_fine(t, mesh)
  
  # Compute grad(v)
  _grad = mesh.compute_derivatives(u)
  grad_v = {"gu":_grad[0], "gv":_grad[1], "gw":_grad[2]}
  
  _grad_v = dict()
  _grad_v["xx"] = grad_v["gu"][:,0]
  _grad_v["xy"] = grad_v["gu"][:,1]
  _grad_v["xz"] = grad_v["gu"][:,2]
  
  _grad_v["yx"] = grad_v["gv"][:,0]
  _grad_v["yy"] = grad_v["gv"][:,1]
  _grad_v["yz"] = grad_v["gv"][:,2]

  _grad_v["zx"] = grad_v["gw"][:,0]
  _grad_v["zy"] = grad_v["gw"][:,1]
  _grad_v["zz"] = grad_v["gw"][:,2]

  simple_vts_writer("grid.vts", mesh, cellData=_grad_v, pointData={"u": u})
  
  
  # Compute grad(v)
  del _grad
  _grad = mesh.compute_derivatives_c(u)
  grad_v = {"gu":_grad[0], "gv":_grad[1], "gw":_grad[2]}
  
  _grad_v = dict()
  _grad_v["xx"] = grad_v["gu"][:,0]
  _grad_v["xy"] = grad_v["gu"][:,1]
  _grad_v["xz"] = grad_v["gu"][:,2]
  
  _grad_v["yx"] = grad_v["gv"][:,0]
  _grad_v["yy"] = grad_v["gv"][:,1]
  _grad_v["yz"] = grad_v["gv"][:,2]
  
  _grad_v["zx"] = grad_v["gw"][:,0]
  _grad_v["zy"] = grad_v["gw"][:,1]
  _grad_v["zz"] = grad_v["gw"][:,2]
  simple_vts_writer("grid_c.vts", mesh, cellData=_grad_v)



if __name__ == '__main__':

  #test_1()
  #test_debug()
  #test_adj()
  #test_interp()
  #test_extract_topo()
  #test_vec()
  
  #ptatin_demo()
  #ptatin_demo_p0field()

  ptatin_demo_c_compute_derivs()
