
import numpy as np
from time import perf_counter

import pyviztools as pvt
from pyviztools import simple_vts_writer
from pyviztools.ptatin_utils import *
from pyviztools.utils import *

def get_data(dir):
  import os, json

  # Parse a JSON file and determine the mesh size for the Q2 mesh
  jfile = open(os.path.join(dir, "stokes_v_dmda.json"), "r")
  js = json.load(jfile)
  jfile.close()

  nvert_i = js["DMDA"]["directions"][0]["points"]
  nvert_j = js["DMDA"]["directions"][1]["points"]
  nvert_k = js["DMDA"]["directions"][2]["points"]

  mx = nvert_i - 1
  my = nvert_j - 1
  mz = nvert_k - 1

  # Load up the mesh coordinates for the Q2 mesh, load the velocity vector
  mesh = pvt.FEMeshQ1CreateFromPetscVec(mx, my, mz, os.path.join(dir,"stokes_v_dmda_coords.pbvec"))
  u = pvt.FEMeshQ1VertexFieldLoadFromPetscVec(mesh, os.path.join(dir, "ptatinstate_stokes_Xv.pbvec"), 3)

  return mesh, u

def build_mesh_rtz(mx, my, mz, start, end):
  mesh = pvt.FEMeshQ1(3, mx, mz, my)
  mesh.create_e2v()
  mesh.create_coor()
  femesh_set_uniform_coordinates(mesh, start, end)
  return mesh

def create_cylinderical_mesh(elements, r_range, angle_range, depth_range, depth_dir=None):
  mx, my, mz = elements[0], elements[1], elements[2]
  mesh = pvt.FEMeshQ1(3, mx, my, mz)
  mesh.create_e2v()
  mesh.create_coor()

  if depth_dir == "y": # r-theta plane is x,z
    mesh_c = pvt.FEMeshQ1(3, mx, mz, my)
    mesh_c.create_e2v()
    mesh_c.create_coor()

    dz = (depth_range[1] - depth_range[0])/float(my)
    dr = (r_range[1] - r_range[0])/float(mx)
    da = (angle_range[1] - angle_range[0])/float(mz)

    for di in range(my+1):
      for ai in range(mz+1):
        for ri in range(mx+1):
          r_ = r_range[0] + ri * dr
          a_ = angle_range[0] + ai * da
          d_ = depth_range[0] + di * dz
          x_ = r_ * np.cos(a_)
          z_ = r_ * np.sin(a_)
          y_ = d_
          idx = ri + di * (mx+1) + ai * (mx+1)*(my+1)
          mesh.coor[idx, :] = np.array([x_, y_, z_])

          idx = ri + ai * (mesh_c.m+1) + di * (mesh_c.m+1)*(mesh_c.n+1)
          mesh_c.coor[idx, :] = np.array([r_, a_, d_])


  elif depth_dir == "z": # r-theta plane is x,y
    raise RuntimeError('Unsupported for direction `z`')
  elif depth_dir == "x": # r-theta plane is y,z
    raise RuntimeError('Unsupported for direction `x`')
  else:
    raise RuntimeError('Unsupported direction. Must be `x`, `y` or `z`')

  return mesh, mesh_c

def point_location(mesh, points):
  cmethods = pvt.clib_ftable["utils"]

  npoints = points.shape[0]
  el_p = np.zeros(npoints, dtype=np.int64)
  xi_p = np.zeros((npoints, points.shape[1]), dtype=np.float32)

  msizes = np.array([mesh.m, mesh.n, mesh.p], dtype=np.int64)
  cmethods["structured_hex_mesh_point_location"](
    msizes,
    mesh.element,
    mesh.coor,
    points.shape[0],
    points,
    el_p,
    xi_p
  )
  return el_p, xi_p

def interpolate(mesh, field=None, points=None):
  if field is None or points is None:
    raise RuntimeError("Must provide values for both `field` and `points`.")

  el, xi = point_location(mesh, points)
  print('[interpolate] min el', np.min(el))
  print('[interpolate] max el', np.max(el), mesh.m*mesh.n*mesh.p)

  cmethods = pvt.clib_ftable["utils"]

  msize = np.int64(mesh.m * mesh.n * mesh.p)
  bs = np.int64(field.shape[1])
  npoints = points.shape[0]
  field_p = np.zeros((npoints, field.shape[1]), dtype=np.float32)

  cmethods["structured_hex_mesh_interpolate"](
    msize,
    mesh.element,
    bs,
    field,
    points.shape[0],
    el,
    xi,
    field_p
  )
  return field_p

# cartesian_to_cylinderical if reverse = False
def transform_v(coor, field, reverse=False):
  """
  vx -> vr
  vz -> vtheta
  vy -> vz
  """
  theta = np.arctan2(coor[:,2], coor[:,0])
  vR = np.zeros(field.shape, dtype=np.float32)
  vX_x = field[:, 0]
  vX_y = field[:, 1]
  vX_z = field[:, 2]

  # https://dynref.engr.illinois.edu/rvy.html
  basis = [[None, None, None],[None, None, None],[None, None, None]]
  if reverse == False: # cartesian -> cylinderical
    basis[0][0] = np.cos(theta)
    basis[0][1] = 0.0
    basis[0][2] = np.sin(theta)

    basis[1][0] = -np.sin(theta)
    basis[1][1] = 0.0
    basis[1][2] = np.cos(theta)

    basis[2][0] = 0.0
    basis[2][1] = 1.0
    basis[2][2] = 0.0
  else: # cylinderical -> cartesian
    basis[0][0] = np.cos(theta)
    basis[0][1] = -np.sin(theta)
    basis[0][2] = 0.0

    basis[1][0] = 0.0
    basis[1][1] = 0.0
    basis[1][2] = 1.0

    basis[2][0] = np.sin(theta)
    basis[2][1] = np.cos(theta)
    basis[2][2] = 0.0


  vR[:, 0] = basis[0][0] * vX_x + basis[0][1] * vX_y + basis[0][2] * vX_z
  vR[:, 1] = basis[1][0] * vX_x + basis[1][1] * vX_y + basis[1][2] * vX_z
  vR[:, 2] = basis[2][0] * vX_x + basis[2][1] * vX_y + basis[2][2] * vX_z

  return vR



def compute_strain_rate_rtz(mesh, field):
  index = {"r": 0, "theta": 1, "z": 2}

  centroid = femesh_get_cell_centroids(mesh)
  rr_c    = centroid[:, index["r"]]
  theta_c = centroid[:, index["theta"]]

  ur     = field[:, index["r"]]
  utheta = field[:, index["theta"]]
  uz     = field[:, index["z"]]

  grad_ur     = mesh.compute_derivatives(ur)
  grad_utheta = mesh.compute_derivatives(utheta)
  grad_uz     = mesh.compute_derivatives(uz)

  field_cell = mesh.interp2cell(field)
  ur_c = field_cell[:, index["r"]]
  utheta_c = field_cell[:, index["theta"]]

  sr = dict()
  sr["e_rr"] = grad_ur[0][:, index["r"]]
  sr["e_tt"] = (1.0/rr_c[:]) * grad_utheta[0][:, index["theta"]] + ur_c[:]/rr_c[:]
  sr["e_zz"] = grad_uz[0][:, index["z"]]

  sr["e_rt"] = 0.5 * ( (1.0/rr_c[:]) * grad_ur[0][:, index["theta"]]
                          + grad_utheta[0][:, index["r"]]
                          - utheta_c[:]/rr_c[:] )
  #sr["e_tr"] = sr["e_rt"]

  sr["e_tz"] = 0.5 * ( (1.0/rr_c[:])*grad_uz[0][:, index["theta"]] + grad_utheta[0][:, index["z"]] )
  #sr["e_zt"] = sr["e_tz"]

  sr["e_rz"] = 0.5 * ( grad_ur[0][:, index["z"]] + grad_uz[0][:, index["r"]] )
  #sr["e_zr"] = sr["e_rz"]


  return sr

def test():
  mesh = pvt.FEMeshQ1(3, 3, 4, 2)
  mesh.create_e2v()
  mesh.create_coor()
  femesh_set_uniform_coordinates(mesh, np.array([0.0, 0.0, 0.0],dtype=np.float32), np.array([1.0, 0.5, 1.0],dtype=np.float32))

  vx = mesh.coor[:,0]
  vz = mesh.coor[:,2]

  theta = np.arctan2(mesh.coor[:,2], mesh.coor[:,0])
  rr = np.sqrt(mesh.coor[:,0]**2 + mesh.coor[:,2]**2) + 1.0e-32

  vr = vz[:]*np.cos(theta) + vx*np.sin(theta)
  print(np.min(vr), np.max(vr))
  vt = -vz[:]*np.sin(theta) + vx*np.cos(theta)
  print(np.min(vt), np.max(vt))

  xx = rr * np.cos(theta)
  #print(mesh.coor[:,0])
  #print(xx)
  zz = rr * np.sin(theta)
  print(mesh.coor[:,2])
  print(zz)

  field = mesh.create_vertex_field(dof=3)
  vr[:] = 0.0
  vt[:] = 1.0
  field[:,0] = np.cos(theta) * vr[:] - np.sin(theta) * vt[:]
  field[:,2] = np.sin(theta) * vr[:] + np.cos(theta) * vt[:]


  simple_vts_writer("demo_rtz.vts", mesh, pointData={"vel": field})

  cyl_xyz, cyl_rtz = create_cylinderical_mesh([10, 10, 10], [0.01, 0.6], [0.0, np.pi/10.0], depth_range=[0, 0.3], depth_dir="y")

  simple_vts_writer("demo.vts", cyl_xyz)


# interpolate v (Vi[x]) to some r-t mesh (any)
# convert Vi[x] to Vi[r]
# interpolate Vi[r] onto an r/theta mesh
# compute strain-rate on r-theta mesh

def ptatin_cylinderical_viz_broken():

  mesh, velocity = get_data("/Users/dmay/codes/ptatin3d-pyviztools/step180")

  simple_vts_writer("grid.vts", mesh, pointData={"vel": velocity})

  cyl_xyz, cyl_rtz = create_cylinderical_mesh([64, 64, 64], [0.1, 0.3], [0.0, np.pi/2.0], depth_range=[0, 0.31], depth_dir="y")
  simple_vts_writer("cyl_grid.vts", cyl_xyz)
  simple_vts_writer("cyl_grid_rtz.vts", cyl_rtz)


  #el, xi = point_location(cyl_xyz, np.array([[0.0, 0.0, 0.0]], dtype=np.float32))
  el, xi = point_location(mesh, cyl_xyz.coor)
  print('min el', np.min(el))

  vel_p = interpolate(mesh, points=cyl_xyz.coor, field=velocity)
  simple_vts_writer("cyl_grid_v.vts", cyl_xyz, pointData={"vel": vel_p})

  vel_p_rtz = transform_v(cyl_xyz.coor, vel_p)
  print('vy/vz', np.max(vel_p[:,1]), np.max(vel_p_rtz[:,2]))
  #if 1 == 1: # test!
  #    vel_p_rtz_xyz = transform_v(cyl_xyz.coor, vel_p_rtz, reverse=True)
  #    simple_vts_writer("cyl_grid_v2.vts", cyl_xyz, pointData={"vel": vel_p, "vel2":vel_p_rtz_xyz})


  cyl_p_rtz = transform_v(cyl_xyz.coor, cyl_xyz.coor)
  print(cyl_p_rtz)

  #el, xi = point_location(cyl_rtz, cyl_p_rtz)
  #print('min/ax el', np.min(el), np.max(el))
  vel_p_ = interpolate(cyl_rtz, points=cyl_p_rtz, field=vel_p_rtz)
  print('vz_interp/vz_ref', np.max(vel_p_[:,2]), np.max(vel_p_rtz[:,2]))
  simple_vts_writer("cyl_grid_v_rtz.vts", cyl_rtz, pointData={"vel":vel_p_})

  idx = np.argsort(vel_p_rtz[:,2])
  print('coor_p_rtz', cyl_p_rtz[idx[-1], :])
  print('vel_p_rtz', vel_p_rtz[idx[-1], :])
  print('vel_p_rtz[inter]', interpolate(cyl_rtz, points=np.array(
    [ [ cyl_p_rtz[idx[-1], 0],cyl_p_rtz[idx[-1], 1],cyl_p_rtz[idx[-1], 2] ] ], dtype=np.float32), field=vel_p_rtz))


  sr_rtz = compute_strain_rate_rtz(cyl_rtz, vel_p_)
  #simple_vts_writer("cyl_grid_e_rtz.vts", cyl_rtz, pointData={"vel":vel_p_}, cellData=sr_rtz)
  simple_vts_writer("cyl_grid_e_rtz.vts", cyl_rtz, pointData={"v_r":vel_p_[:,0], "v_theta":vel_p_[:,1], "v_z":vel_p_[:,2]}, cellData=sr_rtz)

def ptatin_cylinderical_viz():

  mesh, velocity = get_data("/Users/dmay/codes/ptatin3d-pyviztools/step180")

  simple_vts_writer("grid.vts", mesh, pointData={"vel": velocity})

  mesh_rtz = build_mesh_rtz(
    64, 64, 64,
    np.array([0.01, 0.0, 0.0],dtype=np.float32),
    np.array([0.3, np.pi/2.0, 0.31],dtype=np.float32))

  #rtz_2_xyz = transform_v(mesh_rtz.coor, mesh_rtz.coor, reverse=True)
  rtz_2_xyz = np.zeros(mesh_rtz.coor.shape, dtype=np.float32)
  rtz_2_xyz[:, 1] = mesh_rtz.coor[:, 2]
  rtz_2_xyz[:, 0] = mesh_rtz.coor[:, 0] * np.cos(mesh_rtz.coor[:, 1])
  rtz_2_xyz[:, 2] = mesh_rtz.coor[:, 0] * np.sin(mesh_rtz.coor[:, 1])

  vel_xyz_p = interpolate(mesh, points=rtz_2_xyz, field=velocity)

  vel_rtz_p = transform_v(rtz_2_xyz, vel_xyz_p)

  vel_p_ = interpolate(mesh_rtz, points=mesh_rtz.coor, field=vel_rtz_p)
  print('vz_interp/vz_ref', np.max(vel_p_[:,2]), np.max(vel_rtz_p[:,2]))

  sr_rtz = compute_strain_rate_rtz(mesh_rtz, vel_p_)
  #simple_vts_writer("cyl_grid_e_rtz.vts", cyl_rtz, pointData={"vel":vel_p_}, cellData=sr_rtz)
  simple_vts_writer("cyl_grid_e_rtz.vts", mesh_rtz, pointData={"v_r":vel_p_[:,0], "v_theta":vel_p_[:,1], "v_z":vel_p_[:,2]}, cellData=sr_rtz)


if __name__ == '__main__':
  #test()
  #ptatin_cylinderical_viz_broken()
  ptatin_cylinderical_viz()
