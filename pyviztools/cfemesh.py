
import numpy as np
from time import perf_counter
import inspect

from . import clibvt, clib_ftable
from . import femesh
from .utils import *


class CFEMeshQ1(femesh.FEMeshQ1):
  def __init__(self, d, m, n, p):
    if clibvt is None:
      raise RuntimeError('No support for C methods provided. Likley .so is missing')
    
    femesh.FEMeshQ1.__init__(self, d, m, n, p)

  #def is_vertex_field(self, field):
  #def is_cell_field(self, field):
  #def create_coor(self):
  
  def create_e2v(self):
    avx = False
    
    mt0 = perf_counter()
    m = np.zeros(3, dtype=np.int64)
    m[0], m[1], m[2] = self.m, self.n, self.p
    self.element = np.zeros(shape=(self.ne,self.basis_per_el), dtype=np.int64)
    ee = self.element
    element_v = ee.view()
    element_v.shape = self.ne * self.basis_per_el

    clib_ftable["femesh"]["e2v_q1_3d"](m, element_v)

    mt1 = perf_counter()
    dt = mt1 - mt0
    print('->',inspect.currentframe().f_code.co_name + '(avx='+str(avx)+') execution time:',('%1.4e'%dt),'(sec)')


  #def get_elements(self):
  #def get_cell_volume(self, nvec=10000):
  #def eval_basis(self, xi):
  #def eval_basis_gradient_0(self):
  #def create_vertex_field(self, dof=1):
  #def create_cell_field(self, dof=1):
  
  # Supports Vec with any blocksize
  def interp2cell(self, field):
    avx = False
    
    mt0 = perf_counter()
    if not self.is_vertex_field(field):
      raise RuntimeError('Only valid for vertex field')

    dof = vec_get_blocksize(field)
    #print('field: dof',dof)

    #field_cell = np.zeros(shape=(self.ne, dof), dtype=np.float32)
    field_cell = self.create_cell_field(dof=vec_get_blocksize(field))

    ee = self.element
    element_v = ee.view()
    element_v.shape = self.ne * self.basis_per_el
      
    field_v = field.view()
    field_v.shape = self.nv * dof
    
    field_cell_v = field_cell.view()
    field_cell_v.shape = self.ne * dof
      
    clib_ftable["femesh"]["interp_to_cell"](
                            self.ne, dof,
                            element_v,
                            field_v,
                            field_cell_v)

    mt1 = perf_counter()
    dt = mt1 - mt0
    print('->',inspect.currentframe().f_code.co_name + '(avx='+str(avx)+') execution time:',('%1.4e'%dt),'(sec)')
    return field_cell


  def interpgrad2cell(self, _field):
    
    if _field.flags["C_CONTIGUOUS"]:
      field = _field
    else:
      print('-> Note: interpgrad2cell() had to flatten to ensure C_CONTIGUOUS is true')
      field = _field.flatten()

    mt0 = perf_counter()
    if not self.is_vertex_field(field):
      raise RuntimeError('Only valid for vertex field')
    
    dof = len(field.shape)
    #print('field: dof',dof)
    if dof != 1:
      print('Only valid for scalar field. Use .compute_derivatives() instead')
      return None

    avx = False

    ee = self.element
    element_v = ee.view()
    element_v.shape = self.ne * self.basis_per_el
      
    field_v = field.view()
    field_v.shape = self.nv
      
    grad_phi_cell = np.zeros(shape=(self.ne, self.dim), dtype=np.float32)
    grad_v = grad_phi_cell.view()
    grad_v.shape = self.ne * 3
      
    clib_ftable["femesh"]["compute_derivatives_scalar"](
                              self.nv, self.ne,
                              element_v, self.coor,
                              field_v,
                              grad_v)


    mt1 = perf_counter()
    dt = mt1 - mt0
    print('->',inspect.currentframe().f_code.co_name + '(avx='+str(avx)+') execution time:',('%1.4e'%dt),'(sec)')
    return grad_phi_cell


  # Supports Vec with any blocksize
  def compute_derivatives(self, field, **kwargs):
    
    mt0 = perf_counter()
    if not self.is_vertex_field(field):
      raise RuntimeError('Only valid for vertex field')
  
    dof = vec_get_blocksize(field)
    #print('field: dof',dof)
    if dof != 1 and dof != 3:
      raise RuntimeError('Only valid for block-size = 1 or 3. Found',str(dof))

    grad = list()
    for b in range(dof):
      grad_phi_cell = np.zeros(shape=(self.ne, self.dim), dtype=np.float32)
      grad.append(grad_phi_cell)

    avx = False

    if dof == 1:
      ee = self.element
      element_v = ee.view()
      element_v.shape = self.ne * self.basis_per_el
      
      field_v = field.view()
      field_v.shape = self.nv

      grad_v = grad[0].view()
      grad_v.shape = self.ne * 3

      clib_ftable["femesh"]["compute_derivatives_scalar"](
                               self.nv, self.ne,
                               element_v, self.coor,
                               field_v,
                               grad_v)

    if dof == 3:
      ee = self.element
      element_v = ee.view()
      element_v.shape = self.ne * self.basis_per_el

      field_v = field.view()
      field_v.shape = self.nv * 3

      gradu_v = grad[0].view()
      gradu_v.shape = self.ne * 3

      gradv_v = grad[1].view()
      gradv_v.shape = self.ne * 3

      gradw_v = grad[2].view()
      gradw_v.shape = self.ne * 3

      try:
        clib_ftable["femesh"]["compute_derivatives_vector_avx256"](
                                                             self.nv, self.ne,
                                                             element_v, self.coor,
                                                             field_v,
                                                             gradu_v, gradv_v, gradw_v)
        avx = True
      except:
        clib_ftable["femesh"]["compute_derivatives_vector"](
                                 self.nv, self.ne,
                                 element_v, self.coor,
                                 field_v,
                                 gradu_v, gradv_v, gradw_v)

    mt1 = perf_counter()
    dt = mt1 - mt0
    print('->',inspect.currentframe().f_code.co_name + '(avx='+str(avx)+') execution time:',('%1.4e'%dt),'(sec)')
    return grad

  #def interp_coarse_to_fine(self, field_c, to_mesh):
  #def extract_subset_face(self, faceid):
  #def extract_subset(self, _irange, _jrange, _krange, sample_rate=(1, 1, 1), cellData=None, pointData=None):
  #def cell_field_to_point_field(self, field_c, volume_scale=False, **kwargs):
