
import numpy as np
from time import perf_counter
import inspect

from .femesh import FEMeshQ1
from .utils import *


def CreateNumpyVtkTypes():
  typemap = {
    np.int8    : "Int8",
    np.int16   : "Int16",
    np.int32   : "Int32",
    np.int64   : "Int64",
    np.uint8   : "UInt8",
    np.uint16  : "UInt16",
    np.uint32  : "UInt32",
    np.uint64  : "UInt64",
    np.float32 : "Float32",
    np.float64 : "Float64", }
  return typemap


def simple_vts_writer(fname, mesh, cellData=None, pointData=None):
  """
  Simple VTS writer. Files are emmitted using the Appended (binary / raw) data format.
  Vectors with block size 1, 3 are written with components 1 or 3.
  Vectors with different block sizes are written out component wise.
  """

  mt0 = perf_counter()
  # Depending on the input, get the coordinate vector and set the node index range
  if issubclass(mesh.__class__, FEMeshQ1) == True:
    mesh_coor = mesh.coor
    mesh_m = mesh.m # #nodes = mesh.m+1, paraview wants a range [first, last], which is one minus the #nodes
    mesh_n = mesh.n
    mesh_p = mesh.p
  else:
    mesh_coor = mesh["coor"]
    mesh_m = mesh["nodesize"][0] - 1
    mesh_n = mesh["nodesize"][1] - 1
    mesh_p = mesh["nodesize"][2] - 1

  np2vtk_typemap = CreateNumpyVtkTypes()

  def vtkdatacollect(vd, x, name=''):
    L = x.nbytes
    vd.append([np.uint64(L), x, name])

  if fname[-3::] == '.gz':
    import gzip
    f = gzip.open(fname,"wb")
    print('Writing gzip file:',fname)
  else:
    f = open(fname,"wb")
    print('Writing file:',fname)

  f.write(b'<?xml version="1.0"?>\n')
  f.write(b'<VTKFile type="StructuredGrid" version="1.0" byte_order="LittleEndian" header_type="UInt64">\n')
  
  offset_type = np.uint64

  line = '''<StructuredGrid WholeExtent="0 %d 0 %d 0 %d">''' % (mesh_m, mesh_n, mesh_p) + "\n"
  f.write(line.encode('ascii'))

  line = '''<Piece Extent="0 %d 0 %d 0 %d">''' % (mesh_m, mesh_n, mesh_p) + "\n"
  f.write(line.encode('ascii'))

  offset_count = np.uint64(0)
  vtkdata = list()

  f.write('<Points>'.encode('ascii'))
  line = '''
  <DataArray Name="coordinates" NumberOfComponents="3" type="%s" format="appended" offset="%d"/>
''' % (np2vtk_typemap[mesh_coor.dtype.type], offset_count)
  f.write(line.encode('ascii'))
  f.write('</Points>\n'.encode('ascii'))
  vtkdatacollect(vtkdata, mesh_coor, name="coor")
  offset_count += mesh_coor.nbytes + offset_count.itemsize
  

  if cellData is not None:
    f.write('<CellData>\n'.encode('ascii'))
    for fieldName in cellData:
      field = cellData[fieldName]
      
      
      if len(field.shape) == 1:
        line = '''  <DataArray Name="%s" NumberOfComponents="1" type="%s" format="appended" offset="%d"/>\n''' % (fieldName, np2vtk_typemap[field.dtype.type], offset_count)
        f.write(line.encode('ascii'))
        vtkdatacollect(vtkdata, field, name=fieldName)
        offset_count += field.nbytes + offset_count.itemsize

      elif field.shape[1] == 3 or field.shape[1] == 9:
        line = '''  <DataArray Name="%s" NumberOfComponents="%d" type="%s" format="appended" offset="%d"/>\n''' % (fieldName, field.shape[1], np2vtk_typemap[field.dtype.type], offset_count)
        f.write(line.encode('ascii'))
        vtkdatacollect(vtkdata, field, name=fieldName)
        offset_count += field.nbytes + offset_count.itemsize
          
      else:
        for d in range(field.shape[1]):
          line = '''  <DataArray Name="%s-%d" NumberOfComponents="1" type="%s" format="appended" offset="%d"/>\n''' % (fieldName, d, np2vtk_typemap[field.dtype.type], offset_count)
          f.write(line.encode('ascii'))
          vtkdatacollect(vtkdata, field[:, d], name=fieldName+str(d))
          offset_count += field[:, d].nbytes + offset_count.itemsize

    f.write('</CellData>\n'.encode('ascii'))

  if pointData is not None:
    f.write('<PointData>\n'.encode('ascii'))
    for fieldName in pointData:
      field = pointData[fieldName]
    
      if len(field.shape) == 1:
        line = '''  <DataArray Name="%s" NumberOfComponents="1" type="%s" format="appended" offset="%d"/>\n''' % (fieldName, np2vtk_typemap[field.dtype.type], offset_count)
        f.write(line.encode('ascii'))
        vtkdatacollect(vtkdata, field, name=fieldName)
        offset_count += field.nbytes + offset_count.itemsize

      elif field.shape[1] == 3 or field.shape[1] == 9:
        line = '''  <DataArray Name="%s" NumberOfComponents="%d" type="%s" format="appended" offset="%d"/>\n''' % (fieldName, field.shape[1], np2vtk_typemap[field.dtype.type], offset_count)
        f.write(line.encode('ascii'))
        vtkdatacollect(vtkdata, field, name=fieldName)
        offset_count += field.nbytes + offset_count.itemsize
      
      else:
        for d in range(field.shape[1]):
          line = '''  <DataArray Name="%s-%d" NumberOfComponents="1" type="%s" format="appended" offset="%d"/>\n''' % (fieldName, d, np2vtk_typemap[field.dtype.type], offset_count)
          f.write(line.encode('ascii'))
          vtkdatacollect(vtkdata, field[:, d], name=fieldName+str(d))
          offset_count += field[:, d].nbytes + offset_count.itemsize
    

    f.write('</PointData>\n'.encode('ascii'))

  f.write('</Piece>\n'.encode('ascii'))
  f.write('</StructuredGrid>\n'.encode('ascii'))
  f.write('<AppendedData encoding="raw">\n'.encode('ascii'))

  f.write('_'.encode('ascii'))
  for k in range(len(vtkdata)):
    data_length = vtkdata[k][0]
    data        = vtkdata[k][1]
    f.write(data_length)
    print('  vtk field:',vtkdata[k][2],': shape',data.shape,': bytes',data_length, ': type',data.dtype, ': C_CONTIGUOUS', data.flags["C_CONTIGUOUS"])
    if data.flags["C_CONTIGUOUS"]:
      #print('  vtk field: C_CONTIGUOUS')
      f.write(data)
    else:
      #print('  vtk field: flatten before write')
      f.write(data.flatten())

  f.write('\n</AppendedData>\n'.encode('ascii'))
  f.write('</VTKFile>'.encode('ascii'))

  f.close()
  mt1 = perf_counter()
  dt = mt1 - mt0
  print('->',inspect.currentframe().f_code.co_name + '()','execution time:',('%1.4e'%dt),'(sec)')


def simple_vtr_writer(fname, mesh, cellData=None, pointData=None):
  """
  Simple VTR writer. Files are emmitted using the Appended (binary / raw) data format.
  Vectors with block size 1, 3 are written with components 1 or 3.
  Vectors with different block sizes are written out component wise.
  """

  mt0 = perf_counter()
  if isinstance(mesh, dict) == False:
    raise RuntimeError('simple_vtr_writer() type(`mesh`) must be a dict')
    
  mesh_coor_x = mesh["coor"][0]
  mesh_coor_y = mesh["coor"][1]
  mesh_coor_z = mesh["coor"][2]
  mesh_m = mesh["nodesize"][0] - 1
  mesh_n = mesh["nodesize"][1] - 1
  mesh_p = mesh["nodesize"][2] - 1

  np2vtk_typemap = CreateNumpyVtkTypes()

  def vtkdatacollect(vd, x, name=''):
    L = x.nbytes
    vd.append([np.uint64(L), x, name])

  if fname[-3::] == '.gz':
    import gzip
    f = gzip.open(fname,"wb")
    print('Writing gzip file:',fname)
  else:
    f = open(fname,"wb")
    print('Writing file:',fname)

  f.write(b'<?xml version="1.0"?>\n')
  f.write(b'<VTKFile type="RectilinearGrid" version="1.0" byte_order="LittleEndian" header_type="UInt64">\n')
  
  offset_type = np.uint64

  line = '''<RectilinearGrid WholeExtent="0 %d 0 %d 0 %d">''' % (mesh_m, mesh_n, mesh_p) + "\n"
  f.write(line.encode('ascii'))

  line = '''<Piece Extent="0 %d 0 %d 0 %d">''' % (mesh_m, mesh_n, mesh_p) + "\n"
  f.write(line.encode('ascii'))

  offset_count = np.uint64(0)
  vtkdata = list()

  f.write('<Coordinates>\n'.encode('ascii'))

  mdata = mesh_coor_x
  line = '''  <DataArray Name="coor_x" type="%s" format="appended" offset="%d"/>
''' % (np2vtk_typemap[mdata.dtype.type], offset_count)
  f.write(line.encode('ascii'))
  vtkdatacollect(vtkdata, mdata, name="coor_x")
  offset_count += mdata.nbytes + offset_count.itemsize

  mdata = mesh_coor_y
  line = '''  <DataArray Name="coor_y" type="%s" format="appended" offset="%d"/>
''' % (np2vtk_typemap[mdata.dtype.type], offset_count)
  f.write(line.encode('ascii'))
  vtkdatacollect(vtkdata, mdata, name="coor_y")
  offset_count += mdata.nbytes + offset_count.itemsize

  mdata = mesh_coor_z
  line = '''  <DataArray Name="coor_z" type="%s" format="appended" offset="%d"/>
''' % (np2vtk_typemap[mdata.dtype.type], offset_count)
  f.write(line.encode('ascii'))
  vtkdatacollect(vtkdata, mdata, name="coor_z")
  offset_count += mdata.nbytes + offset_count.itemsize

  f.write('</Coordinates>\n'.encode('ascii'))


  if cellData is not None:
    f.write('<CellData>\n'.encode('ascii'))
    for fieldName in cellData:
      field = cellData[fieldName]
      
      
      if len(field.shape) == 1:
        line = '''  <DataArray Name="%s" NumberOfComponents="1" type="%s" format="appended" offset="%d"/>\n''' % (fieldName, np2vtk_typemap[field.dtype.type], offset_count)
        f.write(line.encode('ascii'))
        vtkdatacollect(vtkdata, field, name=fieldName)
        offset_count += field.nbytes + offset_count.itemsize

      elif field.shape[1] == 3 or field.shape[1] == 9:
        line = '''  <DataArray Name="%s" NumberOfComponents="%d" type="%s" format="appended" offset="%d"/>\n''' % (fieldName, field.shape[1], np2vtk_typemap[field.dtype.type], offset_count)
        f.write(line.encode('ascii'))
        vtkdatacollect(vtkdata, field, name=fieldName)
        offset_count += field.nbytes + offset_count.itemsize
          
      else:
        for d in range(field.shape[1]):
          line = '''  <DataArray Name="%s-%d" NumberOfComponents="1" type="%s" format="appended" offset="%d"/>\n''' % (fieldName, d, np2vtk_typemap[field.dtype.type], offset_count)
          f.write(line.encode('ascii'))
          vtkdatacollect(vtkdata, field[:, d], name=fieldName+str(d))
          offset_count += field[:, d].nbytes + offset_count.itemsize

    f.write('</CellData>\n'.encode('ascii'))

  if pointData is not None:
    f.write('<PointData>\n'.encode('ascii'))
    for fieldName in pointData:
      field = pointData[fieldName]
    
      if len(field.shape) == 1:
        line = '''  <DataArray Name="%s" NumberOfComponents="1" type="%s" format="appended" offset="%d"/>\n''' % (fieldName, np2vtk_typemap[field.dtype.type], offset_count)
        f.write(line.encode('ascii'))
        vtkdatacollect(vtkdata, field, name=fieldName)
        offset_count += field.nbytes + offset_count.itemsize

      elif field.shape[1] == 3 or field.shape[1] == 9:
        line = '''  <DataArray Name="%s" NumberOfComponents="%d" type="%s" format="appended" offset="%d"/>\n''' % (fieldName, field.shape[1], np2vtk_typemap[field.dtype.type], offset_count)
        f.write(line.encode('ascii'))
        vtkdatacollect(vtkdata, field, name=fieldName)
        offset_count += field.nbytes + offset_count.itemsize
      
      else:
        for d in range(field.shape[1]):
          line = '''  <DataArray Name="%s-%d" NumberOfComponents="1" type="%s" format="appended" offset="%d"/>\n''' % (fieldName, d, np2vtk_typemap[field.dtype.type], offset_count)
          f.write(line.encode('ascii'))
          vtkdatacollect(vtkdata, field[:, d], name=fieldName+str(d))
          offset_count += field[:, d].nbytes + offset_count.itemsize
    

    f.write('</PointData>\n'.encode('ascii'))

  f.write('</Piece>\n'.encode('ascii'))
  f.write('</RectilinearGrid>\n'.encode('ascii'))
  f.write('<AppendedData encoding="raw">\n'.encode('ascii'))

  f.write('_'.encode('ascii'))
  for k in range(len(vtkdata)):
    data_length = vtkdata[k][0]
    data        = vtkdata[k][1]
    f.write(data_length)
    print('  vtk field:',vtkdata[k][2],': shape',data.shape,': bytes',data_length, ': type',data.dtype, ': C_CONTIGUOUS', data.flags["C_CONTIGUOUS"])
    if data.flags["C_CONTIGUOUS"]:
      #print('  vtk field: C_CONTIGUOUS')
      f.write(data)
    else:
      #print('  vtk field: flatten before write')
      f.write(data.flatten())

  f.write('\n</AppendedData>\n'.encode('ascii'))
  f.write('</VTKFile>'.encode('ascii'))

  f.close()
  mt1 = perf_counter()
  dt = mt1 - mt0
  print('->',inspect.currentframe().f_code.co_name + '()','execution time:',('%1.4e'%dt),'(sec)')

