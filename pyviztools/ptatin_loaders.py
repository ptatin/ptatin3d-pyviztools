
import numpy as np
from time import perf_counter
import inspect

from . import femesh
from . import PetscBinaryIO



def PetscVecLoadFromFile(fname, **kwags):
  io = PetscBinaryIO.PetscBinaryIO(**kwags) # Instantiate a petsc binary loader
  with open(fname) as fp:
    objecttype = io.readObjectType(fp)
    v = io.readVec(fp)
  return v


def PetscVecLoadFromFile_native_python(fname):
  fp = open(fname, "rb")
  h1 = np.fromfile(fp, dtype=np.dtype(">i4"), count=1)
  h2 = np.fromfile(fp, dtype=np.dtype(">i4"), count=1)
  #print('vecid',h1, 'length',h2)
  v = np.fromfile(fp, dtype=np.dtype(">f8"), count=int(h2))
  #print(v[0])
  fp.close()
  return v


def FEMeshQ1VertexFieldLoadFromPetscVec(mesh, fname, bs=1, **kwargs):
  x = mesh.create_vertex_field(dof=bs)
  _x = PetscVecLoadFromFile(fname, **kwargs)
  if bs != 1:
    x[:,:] = np.reshape(_x, newshape=(mesh.nv, bs))
  else:
    x[:] = _x[:]
  return x


def FEMeshQ1CellFieldLoadFromPetscVec(mesh, fname, bs=1, **kwargs):
  x = mesh.create_cell_field(dof=bs)
  _x = PetscVecLoadFromFile(fname, **kwargs)
  if bs != 1:
    x[:,:] = np.reshape(_x, newshape=(mesh.ne, bs))
  else:
    x[:] = _x[:]
  return x


def FEMeshQ1CreateFromPetscVec(mx, my, mz, coor_fname=None, mtype=femesh.FEMeshQ1, **kwargs):
  mt0 = perf_counter()
  #mesh = femesh.FEMeshQ1(3, mx, my, mz)
  mesh = mtype(3, mx, my, mz)
  mesh.create_e2v()

  if coor_fname is not None:
    mesh.create_coor()
    
    coor = PetscVecLoadFromFile(coor_fname, **kwargs)
    mesh.coor[:,:] = np.reshape(coor, newshape=(mesh.nv, 3))
    
    if mesh.is_vertex_field(mesh.coor) == False:
      print('Coordinate file load',coor_fname,'is not consistent with mesh size specified')
  mt1 = perf_counter()
  dt = mt1 - mt0
  print('->',inspect.currentframe().f_code.co_name + '()','execution time:',('%1.4e'%dt),'(sec)')

  return mesh


