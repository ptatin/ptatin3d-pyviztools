
import numpy as np
import pyviztools as pvt


## Inspired by this post
## https://stackoverflow.com/questions/21828202/fast-inverse-and-transpose-matrix-in-python
def stacked_inverse_3x3(A):
  AI = np.empty_like(A)
  for i in range(3):
    AI[...,:,i] = np.cross(A[...,i-2,:], A[...,i-1,:])
  x1x2 = AI[...,:,0]
  x0 = A[...,0,:]
  mydet = np.einsum('ij,ij->i',x1x2, x0)
  #print(mydet,np.linalg.det(A[0]))
  return  AI / mydet[..., None, None]


def stacked_detJ_3x3(A):
  AI = np.empty_like(A)
  for i in range(3):
    AI[...,:,i] = np.cross(A[...,i-2,:], A[...,i-1,:])
  x1x2 = AI[...,:,0]
  x0 = A[...,0,:]
  mydet = np.einsum('ij,ij->i',x1x2, x0)
  return mydet


def blockshaped(arr, nrows, ncols):
  """
  Return an array of shape (n, nrows, ncols) where
  n * nrows * ncols = arr.size

  If arr is a 2D array, the returned array should look like n subblocks with
  each subblock preserving the "physical" layout of arr.
  """
  h, w = arr.shape
  assert h % nrows == 0, "{} rows is not evenly divisble by {}".format(h, nrows)
  assert w % ncols == 0, "{} cols is not evenly divisble by {}".format(w, ncols)
  return (arr.reshape(h//nrows, nrows, -1, ncols)
          .swapaxes(1,2)
          .reshape(-1, nrows, ncols))


def vec_get_blocksize(vec):
  if len(vec.shape) == 1: return 1
  else:                   return vec.shape[1]


def femesh_point_location(mesh, points):
  m = np.asarray([mesh.m, mesh.n, mesh.p], dtype=np.int64)
  
  element_v = mesh.element.view()
  element_v.shape = mesh.ne * mesh.basis_per_el
  
  if points.ndim == 1:
    npoints = 1
    ndim = points.shape[0]
  elif points.ndim == 2:
    npoints = points.shape[0]
    ndim = points.shape[1]
  else:
    pass

  points_v = points.view()
  points_v.shape = npoints * ndim

  wil = np.zeros(npoints, dtype=np.int64)
  
  if npoints > 1:
    xi = np.zeros((npoints, ndim), dtype=np.float32)
    xi_v = xi.view()
    xi_v.shape = npoints * ndim
  else:
    xi = np.zeros(ndim, dtype=np.float32)
    xi_v = xi

    pvt.clib_ftable["utils"]["structured_hex_mesh_point_location"](m,element_v,p_mesh.coor,npoints,points_v,wil,xi_v)

  return wil, xi


def femesh_set_uniform_coordinates(mesh, start, end):
  dir = np.asarray([mesh.m+1, mesh.n+1, mesh.p+1], dtype=np.int64)

  mesh.create_coor()
  coor_v = mesh.coor.view()
  coor_v.shape = mesh.nv * mesh.dim

  if mesh.dim == 2:
    pvt.clib_ftable["utils"]["set_uniform_coordinates_2"](dir, start, end, coor_v)
  if mesh.dim == 3:
    pvt.clib_ftable["utils"]["set_uniform_coordinates_3"](dir, start, end, coor_v)


def femesh_get_cell_centroids(mesh):
  c_cell = mesh.create_cell_field(dof=mesh.dim)

  elements = mesh.get_elements()
  if mesh.dim == 2:
    for n in range(mesh.basis_per_el):
      _v_index = elements[:, n]
      c_cell[:, 0] += mesh.coor[_v_index, 0]
      c_cell[:, 1] += mesh.coor[_v_index, 1]
  if mesh.dim == 3:
    for n in range(mesh.basis_per_el):
      _v_index = elements[:, n]
      c_cell[:, 0] += mesh.coor[_v_index, 0]
      c_cell[:, 1] += mesh.coor[_v_index, 1]
      c_cell[:, 2] += mesh.coor[_v_index, 2]
  c_cell *= 1.0/np.float32(mesh.basis_per_el)
  return c_cell
