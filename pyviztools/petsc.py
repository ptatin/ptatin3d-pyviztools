
import os
import numpy as np

# Defaults appear first in the tuples below
_valid_conf_scalar   = ("real", "complex")
_valid_conf_float_dt = ("double", "float", "__float128", "__fp16")
_vaid_conf_int_dt    = ("int", "long int")

_conf_float_map = {32: "float", 64: "double", 128: "__float128", 16: "__fp16"}
_conf_int_map   = {32: "int", 64: "long int"}

_inv_conf_float_map = {"float": 32, "double": 64, "__float128": 128, "__fp16": 16}
_inv_conf_int_map   = {"int": 32, "long int": 64}

# valid for versions 3.7 - 3.13
def _parse_petsc_conf(petsc_build):
  conf_file = os.path.join(petsc_build, 'lib/petsc/conf/petscvariables')
  print('Parsing', conf_file)

  f_dict = dict()
  fp = open(conf_file, "r")
  for line in fp:
    chop = line.split('=') # Split line into a tuple.
    key = chop[0].lstrip()
    key = key.rstrip()
    value = chop[1].lstrip()
    value = value.rstrip()
    f_dict[key] = value
  fp.close()

  conf = dict()
  conf['PETSC_SCALAR']      = f_dict['PETSC_SCALAR']
  conf['PETSC_SCALAR_SIZE'] = int(f_dict['PETSC_SCALAR_SIZE'])
  conf['PETSC_INDEX_SIZE']  = int(f_dict['PETSC_INDEX_SIZE'])

  return conf


class PETSc:
  """
  Helper class to correctly load PETSc binary objects.
  """

  version = [0, 0, 0] # Currently not used or set

  scalar_type    = "real"
  float_datatype = "double"
  int_datatype   = "int"
  path_to_petsc_build = None


  def configure_from_package(self, fullpath=None, dir=None, arch=None):
    """
    Determine PETSc configure information from the build itself.
    Either specify the fullpath to your petsc build, or provide values for
    both PETSC_DIR and PETSC_ARCH.
    If no inputs are provided, the environment variables
    PETSC_DIR and PETSC_ARCH will be used.
    """

    self.path_to_petsc_build = None # reset value

    _path_to_petsc = None
    # Query env variables if dir, arch not specified
    if fullpath == None and dir == None and arch == None:
      petsc_dir = os.environ.get('PETSC_DIR')
      petsc_arch = os.environ.get('PETSC_ARCH')
      _path_to_petsc = os.path.join(petsc_dir, petsc_arch)
      print('[from env]', _path_to_petsc)

    else:
      if fullpath is not None:
        _path_to_petsc = fullpath
        print('[from fullpath]', _path_to_petsc)
      else:
        # Check everything is not None
        if dir is None or arch is None:
          raise RuntimeError("A non-None value for PETSC_DIR and PETSC_ARCH must be provided. Found " + str(dir) + ", " + str(arch))
        _path_to_petsc = os.path.join(dir, arch)
        print('[from dir+arch]', _path_to_petsc)

    # Check path actually exists
    if os.path.exists(_path_to_petsc) != True:
      raise RuntimeError("Path to PETSc " + _path_to_petsc + " does not exist")
    else:
      print("Found PETSc build: " + _path_to_petsc)

    self.path_to_petsc_build = _path_to_petsc

    self.conf = _parse_petsc_conf(self.path_to_petsc_build)

    self.scalar_type    = self.conf['PETSC_SCALAR']
    self.float_datatype = _conf_float_map[ self.conf['PETSC_SCALAR_SIZE'] ]
    self.int_datatype   = _conf_int_map[ self.conf['PETSC_INDEX_SIZE'] ]


  def __str__(self):
    if self.scalar_type != "real":
      s = 'Scalar type (PetscScalar) [--with-scalar-type]: ' + repr(self.scalar_type)
    else:
      s = 'Scalar type (PetscScalar): ' + repr(self.scalar_type)

    if self.float_datatype != "double":
      s += '\n' + 'Number type (PetscReal) [ --with-precision]: ' + repr(self.float_datatype)
    else:
      s += '\n' + 'Number type (PetscReal): ' + repr(self.float_datatype)

    if self.int_datatype != "int":
      s += '\n' + 'Integer type (PetscInt) [--with-64-bit-indices]: ' + repr(self.int_datatype)
    else:
      s += '\n' + 'Integer type (PetscInt): ' + repr(self.int_datatype)

    if self.path_to_petsc_build is not None:
      s += '\n' + 'Path to PETSc build: ' + repr(self.path_to_petsc_build)
    return s


  def configure(self, scalar, float_dtype, int_dtype):
    """
    Manually provide PETSc configure information.
    """

    if scalar not in _valid_conf_scalar:
      raise RuntimeError("Valid scalar types are: " + str(_valid_conf_scalar) + ". You provided " + repr(scalar))
    if float_dtype not in _valid_conf_float_dt:
      raise RuntimeError("Valid number types are: " + str(_valid_conf_float_dt) + ". You provided " + repr(float_dtype))
    if int_dtype not in _vaid_conf_int_dt:
      raise RuntimeError("Valid integer types are: " + str(_vaid_conf_int_dt) + ". You provided " + repr(int_dtype))

    self.scalar_type = scalar
    self.float_datatype = float_dtype
    self.int_datatype = int_dtype

    self.conf = dict()
    self.conf['PETSC_SCALAR'] = self.scalar_type
    self.conf['PETSC_SCALAR_SIZE'] = _inv_conf_float_map[self.float_datatype]
    self.conf['PETSC_INDEX_SIZE'] = _inv_conf_int_map[self.int_datatype]


  def __repr__(self):
    r = "petsc = pyptviz.petsc.PETSc(); petsc.configure("
    args = ",".join([repr(self.scalar_type), repr(self.float_datatype), repr(self.int_datatype)])
    return r + args + ")"


  def emit_conf(self):
    """
    Create a petsc_conf.py file.
    """
    prec = self.float_datatype
    iscmpl = 'False'
    if self.scalar_type == "complex":
      iscmpl = 'True'
    text = "def get_conf():" + '\n'\
    + "  precision = '" + str(prec) + "'" + '\n'\
    + "  indices = '" + str(self.conf['PETSC_INDEX_SIZE']) + "bit'" + '\n'\
    + "  complexscalars = " + str(iscmpl) + '\n'\
    + "" + '\n'\
    + "  return precision, indices, complexscalars" + '\n'

    fp = open("petsc_conf.py", "w")
    fp.write(text)
    fp.close()


  def read_binary_Vec(self, fname, skip_header=False):
    """
    Load a PETSc Vec object associated with the file `fname` into a flat numpy ndarray.
    The configure information in `PETSc` will be used to conduct the appropriate binary read.
    """

    _c_to_numpy = {
          "__fp16": 'f2', "float": 'f4', "double": 'f8', "__float128": None,
          "short": 'i2', "int": 'i4', "long int": 'i8', "long long int": 'i16',
          "unsigned short": 'u2', "unsigned int": 'u4', "unsigned long int": 'u8', "unsigned long long int": 'u16',
          "bool": '?' }

    if self.scalar_type == "complex":
      _c_to_numpy["__fp16"] = 'c4'
      _c_to_numpy["float"] = 'c8'
      _c_to_numpy["double"] = 'c16'
      _c_to_numpy["__float128"] = None

    # Use big endian for PETSc binary files, i.e. '>'
    petsc_scalar_to_numpy = ">" + _c_to_numpy[ self.float_datatype ]
    petsc_int_to_numpy = ">" + _c_to_numpy[ self.int_datatype ]

    v = None
    with open(fname, 'rb') as fp:
      # Load 2 PetscInt's.
      if skip_header == False:
        h1 = np.fromfile(fp, dtype=np.dtype( petsc_int_to_numpy ), count=1)
        h2 = np.fromfile(fp, dtype=np.dtype( petsc_int_to_numpy ), count=1)
        print('vecid:', h1, 'length:', h2)

      print('numpy scalar type:', petsc_scalar_to_numpy)
      if skip_header == False:
        # Specify the number of entries to read.
        v = np.fromfile(fp, dtype=np.dtype( petsc_scalar_to_numpy ), count=int(h2))
      else:
        # Read all entries.
        v = np.fromfile(fp, dtype=np.dtype( petsc_scalar_to_numpy ))

    return v


  def read_binary_DM(self, fname):
    raise NotImplementedError("todo")
