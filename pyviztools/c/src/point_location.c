

#include <stdio.h>
#include <viztools.h>

#define NSD_2D 2
#define NSD_3D 3

#define ftype_      float
#define ftype_fabs  fabsf
#define ftype_sqrt  sqrtf

//#define PTAT3D_DBG_PointLocation


void ElementHelper_matrix_inverse_2x2(ftype_ A[2][2],ftype_ B[2][2])
{
  ftype_ det;
  
  det = A[0][0]*A[1][1] - A[0][1]*A[1][0];
  det = 1.0/det;
  
  B[0][0] = det * A[1][1];
  B[0][1] = -det * A[0][1];
  B[1][0] = -det * A[1][0];
  B[1][1] = det * A[0][0];
}

void ElementHelper_matrix_inverse_3x3(ftype_ A[3][3],ftype_ B[3][3])
{
  ftype_ t4, t6, t8, t10, t12, t14, t17;
  
  t4 = A[2][0] * A[0][1];
  t6 = A[2][0] * A[0][2];
  t8 = A[1][0] * A[0][1];
  t10 = A[1][0] * A[0][2];
  t12 = A[0][0] * A[1][1];
  t14 = A[0][0] * A[1][2];
  t17 = 0.1e1 / (t4 * A[1][2] - t6 * A[1][1] - t8 * A[2][2] + t10 * A[2][1] + t12 * A[2][2] - t14 * A[2][1]);
  
  B[0][0] = (A[1][1] * A[2][2] - A[1][2] * A[2][1]) * t17;
  B[0][1] = -(A[0][1] * A[2][2] - A[0][2] * A[2][1]) * t17;
  B[0][2] = (A[0][1] * A[1][2] - A[0][2] * A[1][1]) * t17;
  B[1][0] = -(-A[2][0] * A[1][2] + A[1][0] * A[2][2]) * t17;
  B[1][1] = (-t6 + A[0][0] * A[2][2]) * t17;
  B[1][2] = -(-t10 + t14) * t17;
  B[2][0] = (-A[2][0] * A[1][1] + A[1][0] * A[2][1]) * t17;
  B[2][1] = -(-t4 + A[0][0] * A[2][1]) * t17;
  B[2][2] = (-t8 + t12) * t17;
}

void P3D_ConstructNi_Q1_2D(ftype_ _xi[],ftype_ Ni[])
{
  ftype_ xi  = _xi[0];
  ftype_ eta = _xi[1];
  
  Ni[0] = 0.25 * ( 1.0 - xi ) * ( 1.0 - eta );
  Ni[1] = 0.25 * ( 1.0 + xi ) * ( 1.0 - eta );
  Ni[2] = 0.25 * ( 1.0 - xi ) * ( 1.0 + eta );
  Ni[3] = 0.25 * ( 1.0 + xi ) * ( 1.0 + eta );
}

void P3D_ConstructGNi_Q1_2D(ftype_ _xi[],ftype_ GNix[],ftype_ GNiy[])
{
  ftype_ xi  = _xi[0];
  ftype_ eta = _xi[1];
  
  GNix[0] = - 0.25 * ( 1.0 - eta );
  GNix[1] =   0.25 * ( 1.0 - eta );
  GNix[2] = - 0.25 * ( 1.0 + eta );
  GNix[3] =   0.25 * ( 1.0 + eta );
  //
  GNiy[0] = - 0.25 * ( 1.0 - xi );
  GNiy[1] = - 0.25 * ( 1.0 + xi );
  GNiy[2] =   0.25 * ( 1.0 - xi );
  GNiy[3] =   0.25 * ( 1.0 + xi );
}

void P3D_ConstructNi_Q1_3D(ftype_ _xi[],ftype_ Ni[])
{
  ftype_ xi   = _xi[0];
  ftype_ eta  = _xi[1];
  ftype_ zeta = _xi[2];
  
  Ni[0] = 0.125 * ( 1.0 - xi ) * ( 1.0 - eta ) * ( 1.0 - zeta );
  Ni[1] = 0.125 * ( 1.0 + xi ) * ( 1.0 - eta ) * ( 1.0 - zeta );
  Ni[2] = 0.125 * ( 1.0 - xi ) * ( 1.0 + eta ) * ( 1.0 - zeta );
  Ni[3] = 0.125 * ( 1.0 + xi ) * ( 1.0 + eta ) * ( 1.0 - zeta );
  
  Ni[4] = 0.125 * ( 1.0 - xi ) * ( 1.0 - eta ) * ( 1.0 + zeta );
  Ni[5] = 0.125 * ( 1.0 + xi ) * ( 1.0 - eta ) * ( 1.0 + zeta );
  Ni[6] = 0.125 * ( 1.0 - xi ) * ( 1.0 + eta ) * ( 1.0 + zeta );
  Ni[7] = 0.125 * ( 1.0 + xi ) * ( 1.0 + eta ) * ( 1.0 + zeta );
}

void P3D_ConstructGNi_Q1_3D(ftype_ _xi[],ftype_ GNi[3][8])
{
  ftype_ xi   = _xi[0];
  ftype_ eta  = _xi[1];
  ftype_ zeta = _xi[2];
  
  GNi[0][0] = - 0.125 * ( 1.0 - eta ) * ( 1.0 - zeta );
  GNi[0][1] =   0.125 * ( 1.0 - eta ) * ( 1.0 - zeta );
  GNi[0][2] = - 0.125 * ( 1.0 + eta ) * ( 1.0 - zeta );
  GNi[0][3] =   0.125 * ( 1.0 + eta ) * ( 1.0 - zeta );
  
  GNi[0][4] = - 0.125 * ( 1.0 - eta ) * ( 1.0 + zeta );
  GNi[0][5] =   0.125 * ( 1.0 - eta ) * ( 1.0 + zeta );
  GNi[0][6] = - 0.125 * ( 1.0 + eta ) * ( 1.0 + zeta );
  GNi[0][7] =   0.125 * ( 1.0 + eta ) * ( 1.0 + zeta );
  //
  GNi[1][0] = - 0.125 * ( 1.0 - xi ) * ( 1.0 - zeta );
  GNi[1][1] = - 0.125 * ( 1.0 + xi ) * ( 1.0 - zeta );
  GNi[1][2] =   0.125 * ( 1.0 - xi ) * ( 1.0 - zeta );
  GNi[1][3] =   0.125 * ( 1.0 + xi ) * ( 1.0 - zeta );
  
  GNi[1][4] = - 0.125 * ( 1.0 - xi ) * ( 1.0 + zeta );
  GNi[1][5] = - 0.125 * ( 1.0 + xi ) * ( 1.0 + zeta );
  GNi[1][6] =   0.125 * ( 1.0 - xi ) * ( 1.0 + zeta );
  GNi[1][7] =   0.125 * ( 1.0 + xi ) * ( 1.0 + zeta );
  //
  GNi[2][0] = -0.125 * ( 1.0 - xi ) * ( 1.0 - eta );
  GNi[2][1] = -0.125 * ( 1.0 + xi ) * ( 1.0 - eta );
  GNi[2][2] = -0.125 * ( 1.0 - xi ) * ( 1.0 + eta );
  GNi[2][3] = -0.125 * ( 1.0 + xi ) * ( 1.0 + eta );
  
  GNi[2][4] = 0.125 * ( 1.0 - xi ) * ( 1.0 - eta );
  GNi[2][5] = 0.125 * ( 1.0 + xi ) * ( 1.0 - eta );
  GNi[2][6] = 0.125 * ( 1.0 - xi ) * ( 1.0 + eta );
  GNi[2][7] = 0.125 * ( 1.0 + xi ) * ( 1.0 + eta );
}


/* 2d implementation */
static void _compute_deltaX_2d(ftype_ J[NSD_2D][NSD_2D],ftype_ f[],ftype_ h[])
{
  int    i;
  ftype_ invJ[NSD_2D][NSD_2D];
  
  
  ElementHelper_matrix_inverse_2x2(J,invJ);
  
  h[0] = h[1] = 0.0;
  for (i=0; i<NSD_2D; i++) {
    h[0] = h[0] + invJ[0][i] * f[i];
    h[1] = h[1] + invJ[1][i] * f[i];
  }
}

static void _compute_J_2dQ1(ftype_ xi[],ftype_ vertex[],ftype_ J[2][2])
{
  int    i;
  ftype_ GNi_xi[NPE_2D_Q1],GNi_eta[NPE_2D_Q1];
  
  
  J[0][0] = J[0][1] = 0.0;
  J[1][0] = J[1][1] = 0.0;
  
  P3D_ConstructGNi_Q1_2D(xi,GNi_xi,GNi_eta);
  for (i=0; i<NPE_2D_Q1; i++) {
    int  i2 = i * NSD_2D;
    ftype_ x = vertex[i2];
    ftype_ y = vertex[i2+1];
    
    J[0][0] += x * GNi_xi[i];
    J[0][1] += x * GNi_eta[i];
    
    J[1][0] += y * GNi_xi[i];
    J[1][1] += y * GNi_eta[i];
  }
}

static void _compute_F_2dQ1(ftype_ xi[],ftype_ vertex[],ftype_ pos[],ftype_ f[])
{
  int    i;
  ftype_ Ni[NPE_2D_Q1];
  
  
  /* Update F for the next iteration */
  f[0] = f[1] = 0.0;
  
  P3D_ConstructNi_Q1_2D(xi,Ni);
  for (i=0; i<NPE_2D_Q1; i++) {
    int i2   = i * NSD_2D;
    int i2p1 = i2+1;
    
    f[0] += vertex[i2]   * Ni[i];
    f[1] += vertex[i2p1] * Ni[i];
  }
  f[0] = - f[0] + pos[0];
  f[1] = - f[1] + pos[1];
}

typedef struct {
  ftype_   coor[2];
  ftype_   xi[2];
  long int wil;
} MPntStd2d;

static void InverseMappingDomain_2dQ1(ftype_ tolerance,
                               int max_its,
                               int monitor,
                               const ftype_ coords[],
                               long int mx,long int my,
                               const long int element[],
                               long int np,
                               const ftype_ coor_p[],
                               long int el_p[],
                               ftype_ xi_p[])
{
  ftype_    h[NSD_2D];
  ftype_    Jacobian[NSD_2D][NSD_2D];
  ftype_    f[NSD_2D];
  int       d,its,k;
  ftype_    residual2,tolerance2,F2;
  long int  p;
  ftype_    cxip[NSD_2D],Lxip[NSD_2D],Gxip[NSD_2D];
  ftype_    dxi,deta,xi0,eta0;
  long int  nI,nJ,wil_cart;
  ftype_    vertex[NSD_2D * NPE_2D_Q1];
  int       point_found;
  
  
  tolerance2 = tolerance * tolerance; /* Eliminates the need to do a sqrt in the convergence test */
  
#ifdef PTAT3D_DBG_PointLocation
  printf("Domain: ncells = %ld x %ld = %ld\n",mx,my,mx*my);
#endif
  
  /* map domain to [-1,1]x[-1,1] domain */
  dxi  = 2.0/((ftype_)mx);
  deta = 2.0/((ftype_)my);
#ifdef PTAT3D_DBG_PointLocation
  printf("Domain: (dxi,eta) = (%1.8e , %1.8e)\n",dxi,deta);
#endif
  
  for (p=0; p<np; p++) {
    MPntStd2d marker_p;
    
    marker_p.coor[0] = coor_p[2*p+0];
    marker_p.coor[1] = coor_p[2*p+1];
    marker_p.wil = -1;
    marker_p.xi[0] = 0.0;
    marker_p.xi[1] = 0.0;
    
    cxip[0] = marker_p.xi[0];
    cxip[1] = marker_p.xi[1];
    
    Gxip[0] = 0.0;
    Gxip[1] = 0.0;
    
    if (monitor) {
      printf("point[%ld]: pos = (%+1.8e , %+1.8e) : xi = (%+1.8e , %+1.8e)\n",p,marker_p.coor[0],marker_p.coor[1],marker_p.xi[0],marker_p.xi[1]);
    }
    
    point_found = 0;
    
    its = 0;
    do {
#ifdef PTAT3D_DBG_PointLocation
      printf("iteration: %d\n",its);
#endif
      /* convert Gxi to nInJ */
      nI = (Gxip[0] + 1.0)/dxi;
      nJ = (Gxip[1] + 1.0)/deta;
      
      if (nI == mx) { nI--; }
      if (nJ == my) { nJ--; }
      
      if ((nI < 0) || (nJ < 0)) {
#ifdef PTAT3D_DBG_PointLocation
        printf("  nI(%ld), nJ(%ld) negative Gxip (%+1.8e , %+1.8e)\n",nI,nJ,Gxip[0],Gxip[1]);
#endif
        break;
      }
      if (nI >= mx) {
#ifdef PTAT3D_DBG_PointLocation
        printf("  nI too large\n");
#endif
        break;
      }
      if (nJ >= my) {
#ifdef PTAT3D_DBG_PointLocation
        printf("  nJ too large\n");
#endif
        break;
      }
      
      /* Get coords of cell nInJ */
      wil_cart = nI + nJ * mx;
      for (k=0; k<NPE_2D_Q1; k++) {
        long int nid = element[wil_cart*NPE_2D_Q1+k];
        
        vertex[NSD_2D*k+0] = coords[NSD_2D*nid+0];
        vertex[NSD_2D*k+1] = coords[NSD_2D*nid+1];
      }
      
#ifdef PTAT3D_DBG_PointLocation
      printf("  nI / nJ = %ld / %ld : wil_cart %ld : nid = ",nI,nJ,wil_cart);
      for (k=0; k<NPE_2D_Q1; k++) {
        long int nid = element[wil_cart*NPE_2D_Q1+k];
        printf("%ld ",nid);
      }
      printf("\n");
        
      printf("  [vertex] ");
      for (k=0; k<NPE_2D_Q1; k++) {
        printf("(%+1.8e , %+1.8e) ",vertex[NSD_2D*k+0],vertex[NSD_2D*k+1]);
      }
      printf("\n");
#endif
      
      /* convert global (domain) xi TO local (element) xi  */
      xi0   = -1.0 + nI*dxi;
      eta0  = -1.0 + nJ*deta;
      
      /*  Gxi-(-1)/2 = (xp - x0)/dx */
      //      Lxip[0] = 0.5*(Gxip[0]+1.0)*dxi  + xi0;
      //      Lxip[1] = 0.5*(Gxip[1]+1.0)*deta + eta0;
      // x*-x*0/dx = (x+1)/2
      Lxip[0] = 2.0*(Gxip[0] - xi0  )/dxi   - 1.0;
      Lxip[1] = 2.0*(Gxip[1] - eta0 )/deta  - 1.0;
      
#ifdef PTAT3D_DBG_PointLocation
      printf("  Lxi,Lxeta = (%+1.8e , %+1.8e) : nI,nJ (%ld , %ld)\n",Lxip[0],Lxip[1],nI,nJ);
#endif
      
      _compute_F_2dQ1(Lxip,vertex,marker_p.coor,f);
      if (monitor) {
        printf("%4D InverseMapping : F = (%+1.8e , %+1.8e) : xi = (%+1.8e , %+1.8e)\n",its,f[0],f[1],Lxip[0],Lxip[1]);
      }
      
      /* Check for convergence */
      F2 = f[0]*f[0] + f[1]*f[1];
      if (F2 < tolerance2) {
        if (monitor) printf("%4d InverseMapping : converged : Norm of F %1.8e\n",its,ftype_sqrt(F2));
        point_found = 1;
        break;
      }

      /* compute update */
      _compute_J_2dQ1(Lxip,vertex,Jacobian);
      _compute_deltaX_2d(Jacobian,f,h);
      Lxip[0] += 10.0e-1 * h[0];
      Lxip[1] += 10.0e-1 * h[1];
#ifdef PTAT3D_DBG_PointLocation
      printf("  [delta] = (%+1.8e , %+1.8e)\n",h[0],h[1]);
      printf("  [corrected] Lxi,Lxeta = (%+1.8e , %+1.8e)\n",Lxip[0],Lxip[1]);
#endif
      
      residual2 = h[0]*h[0] + h[1]*h[1];
      if (residual2 < tolerance2) {
        if (monitor) printf("%4d InverseMapping : converged : Norm of correction %1.8e\n",its,ftype_sqrt(residual2));
        point_found = 1;
        break;
      }
      
      /* convert Lxip => Gxip */
      //      xi0  = -1.0 + I*dxi;
      //      eta0 = -1.0 + J*deta;
      
      //      Gxip[0] = 2.0 * (Lxip[0]-xi0)/dxi   - 1.0;
      //      Gxip[1] = 2.0 * (Lxip[1]-eta0)/deta - 1.0;
      // x*-x*0/dx = (x+1)/2
      Gxip[0] = dxi   * (Lxip[0] + 1.0)/2.0 + xi0;
      Gxip[1] = deta  * (Lxip[1] + 1.0)/2.0 + eta0;
#ifdef PTAT3D_DBG_PointLocation
      printf("  [Gxi] = (%+1.8e , %+1.8e)\n",Gxip[0],Gxip[1]);
#endif
      
      for (d=0; d<NSD_2D; d++) {
        if (Gxip[d] < -1.0) {
          Gxip[d] = -1.0;
#ifdef PTAT3D_DBG_PointLocation
          printf("  correction outside box: correcting\n");
#endif
        }
        
        if (Gxip[d] > 1.0) {
          Gxip[d] = 1.0;
#ifdef PTAT3D_DBG_PointLocation
          printf("  correction outside box: correcting\n");
#endif
        }
      }
      
      its++;
    } while (its < max_its);
    
    if (monitor && (point_found == 0)){
      if (its >= max_its) {
        printf("%4d %s : Reached maximum iterations (%d) without converging.\n",its,__FUNCTION__,max_its);
      } else {
        printf("%4d %s : Newton broke down, diverged or stagnated after (%d) iterations without converging.\n", its,__FUNCTION__,its);
      }
    }
    
    /* if at the end of the solve, it still looks like the point is outside the mapped domain, mark point as not being found */
    if (ftype_fabs(Gxip[0]) > 1.0) { point_found = 0; }
    if (ftype_fabs(Gxip[1]) > 1.0) { point_found = 0; }
    
    /* update local variables */
    if (point_found == 0) {
      Lxip[0] = NAN;
      Lxip[1] = NAN;
      wil_cart = -1;
    } else {
      /* convert Gxi to nInJ */
      nI = (Gxip[0] + 1.0)/dxi;
      nJ = (Gxip[1] + 1.0)/deta;
      if (nI == mx){ nI--; }
      if (nJ == my){ nJ--; }
      
      if (nI >= mx) {
#ifdef PTAT3D_DBG_PointLocation
        printf("  nI too large -> point location failed\n");
#endif
        for (d=0; d<NSD_2D; d++) { xi_p[NSD_2D*p+d] = NAN; }
        el_p[p] = -1;
        continue;
      }
      if (nJ >= my) {
#ifdef PTAT3D_DBG_PointLocation
        printf("  nJ too large -> point location failed\n");
#endif
        for (d=0; d<NSD_2D; d++) { xi_p[NSD_2D*p+d] = NAN; }
        el_p[p] = -1;
        continue;
      }
      
      /* convert global (domain) xi TO local (element) xi  */
      /*  Gxi-(-1)/2 = (xp - x0)/dx */
      xi0  = -1.0 + nI*dxi;
      eta0 = -1.0 + nJ*deta;
      
      // x*-x*0/dx = (x+1)/2
      Lxip[0] = 2.0*(Gxip[0] - xi0)  /dxi   - 1.0;
      Lxip[1] = 2.0*(Gxip[1] - eta0) /deta  - 1.0;
      
      wil_cart = nI + nJ * mx;
    }
    
    /* set into vector */
    for (d=0; d<NSD_2D; d++) {
      marker_p.xi[d] = Lxip[d];
    }
    marker_p.wil = wil_cart;
    
#ifdef PTAT3D_DBG_PointLocation
    printf("  <<final>> xi,eta = (%+1.8e , %+1.8e) [element = %ld]\n",Lxip[0],Lxip[1],wil_cart);
#endif
    
    for (d=0; d<NSD_2D; d++) {
      xi_p[NSD_2D*p+d] = marker_p.xi[d];
    }
    el_p[p] = marker_p.wil;
  }
}

/* computes h = inv(J) f */
static void _compute_deltaX_3d(ftype_ A[NSD_3D][NSD_3D],ftype_ f[],ftype_ h[])
{
  ftype_ B[NSD_3D][NSD_3D];

  ElementHelper_matrix_inverse_3x3(A,B);
  
  h[0] = B[0][0]*f[0] + B[0][1]*f[1] + B[0][2]*f[2];
  h[1] = B[1][0]*f[0] + B[1][1]*f[1] + B[1][2]*f[2];
  h[2] = B[2][0]*f[0] + B[2][1]*f[1] + B[2][2]*f[2];
}

static void _compute_J_3dQ1(ftype_ xi[],ftype_ vertex[],ftype_ J[NSD_3D][NSD_3D])
{
  int    i,j;
  ftype_ GNi[NSD_3D][NPE_3D_Q1];
  
  
  for (i=0; i<NSD_3D; i++) {
    for (j=0; j<NSD_3D; j++) {
      J[i][j] = 0.0;
    }
  }
  
  P3D_ConstructGNi_Q1_3D(xi,GNi);
  for (i=0; i<NPE_3D_Q1; i++) {
    int i3 = i * NSD_3D;
    ftype_ x = vertex[i3];
    ftype_ y = vertex[i3+1];
    ftype_ z = vertex[i3+2];
    
    J[0][0] += x * GNi[0][i];
    J[0][1] += x * GNi[1][i];
    J[0][2] += x * GNi[2][i];
    
    J[1][0] += y * GNi[0][i];
    J[1][1] += y * GNi[1][i];
    J[1][2] += y * GNi[2][i];
    
    J[2][0] += z * GNi[0][i];
    J[2][1] += z * GNi[1][i];
    J[2][2] += z * GNi[2][i];
  }
}

static void _compute_F_3dQ1(ftype_ xi[],ftype_ vertex[],ftype_ pos[],ftype_ f[])
{
  int    i;
  ftype_ Ni[NPE_3D_Q1];
  
  
  /* Update F for the next iteration */
  f[0] = f[1] = f[2] = 0.0;
  
  P3D_ConstructNi_Q1_3D(xi,Ni);
  for (i=0; i<NPE_3D_Q1; i++) {
    int i3   = i * NSD_3D;
    int i3p1 = i3+1;
    int i3p2 = i3+2;
    
    f[0] += vertex[i3  ] * Ni[i];
    f[1] += vertex[i3p1] * Ni[i];
    f[2] += vertex[i3p2] * Ni[i];
  }
  f[0] = - f[0] + pos[0];
  f[1] = - f[1] + pos[1];
  f[2] = - f[2] + pos[2];
}

typedef struct {
  ftype_   coor[3];
  ftype_   xi[3];
  long int wil;
} MPntStd3d;

static void InverseMappingDomain_3dQ1(ftype_ tolerance,
                               int max_its,
                               int monitor,
                               const ftype_ coords[],
                               long int mx,long int my,long int mz,
                               const long int element[],
                               long int np,
                               const ftype_ coor_p[],
                               long int el_p[],
                               ftype_ xi_p[])
{
  ftype_   h[NSD_3D];
  ftype_   Jacobian[NSD_3D][NSD_3D];
  ftype_   f[NSD_3D];
  long int p;
  int      its,k,d;
  ftype_   residual2,tolerance2,F2;
  ftype_   cxip[NSD_3D],Lxip[NSD_3D],Gxip[NSD_3D];
  ftype_   dxi,deta,dzeta,xi0,eta0,zeta0;
  long int nI,nJ,nK,wil_cart;
  ftype_   vertex[NSD_3D * NPE_3D_Q1];
  int      point_found;
  
  tolerance2 = tolerance * tolerance; /* Eliminates the need to do a sqrt in the convergence test */
  
#ifdef PTAT3D_DBG_PointLocation
  printf("Domain: ncells = %ld x %ld x %ld = %ld\n",mx,my,mz,mx*my*mz);
#endif
  
  /* map domain to [-1,1]x[-1,1]x[-1,1] domain */
  dxi   = 2.0/((ftype_)mx);
  deta  = 2.0/((ftype_)my);
  dzeta = 2.0/((ftype_)mz);
#ifdef PTAT3D_DBG_PointLocation
  printf("Domain: (dxi,eta,zeta) = (%1.8e , %1.8e , %1.8e)\n",dxi,deta,dzeta);
#endif
  
  for (p=0; p<np; p++) {
    MPntStd3d marker_p;
    
#ifdef PTAT3D_DBG_PointLocation
    printf("POINT[%ld]\n",p);
#endif
    
    /* copy these values */
    for (d=0; d<NSD_3D; d++) {
      marker_p.coor[d] = coor_p[NSD_3D*p+d];
      marker_p.xi[d] = 0.0;
    }
    marker_p.wil = -1;
    
    for (d=0; d<NSD_3D; d++) {
      cxip[d] = marker_p.xi[d];
      Gxip[d] = 0.0;
    }

    if (monitor) printf("point[%ld]: pos = (%+1.8e , %+1.8e , %+1.8e) : xi = (%+1.8e , %+1.8e , %+1.8e)\n",p, marker_p.coor[0],marker_p.coor[1],marker_p.coor[2], marker_p.xi[0],marker_p.xi[1],marker_p.xi[2] );
    
    point_found = 0;
    
    its = 0;
    do {
#ifdef PTAT3D_DBG_PointLocation
      printf("iteration: %d\n",its);
#endif
      /* convert Gxi to nInJ */
      nI = (Gxip[0] + 1.0)/dxi;
      nJ = (Gxip[1] + 1.0)/deta;
      nK = (Gxip[2] + 1.0)/dzeta;
      
      if (nI == mx) { nI--; }
      if (nJ == my) { nJ--; }
      if (nK == mz) { nK--; }
      
      if ((nI < 0) || (nJ < 0) || (nK < 0)) {
#ifdef PTAT3D_DBG_PointLocation
        printf("  nI(%ld), nJ(%ld), nK(%ld) negative Gxip (%+1.8e , %+1.8e , %+1.8e)\n",nI,nJ,nK,Gxip[0],Gxip[1],Gxip[2]);
#endif
        break;
      }
      if (nI >= mx) {
#ifdef PTAT3D_DBG_PointLocation
        printf("  nI too large\n");
#endif
        break;
      }
      if (nJ >= my) {
#ifdef PTAT3D_DBG_PointLocation
        printf("  nJ too large\n");
#endif
        break;
      }
      if (nK >= mz) {
#ifdef PTAT3D_DBG_PointLocation
        printf("  nK too large\n");
#endif
        break;
      }
      
      /* Get coords of cell nInJ */
      wil_cart = nI + nJ * mx + nK * mx * my;
      for (k=0; k<NPE_3D_Q1; k++) {
        long int nid = element[wil_cart*NPE_3D_Q1+k];
        
        vertex[NSD_3D*k+0] = coords[NSD_3D*nid+0];
        vertex[NSD_3D*k+1] = coords[NSD_3D*nid+1];
        vertex[NSD_3D*k+2] = coords[NSD_3D*nid+2];
      }
      
#ifdef PTAT3D_DBG_PointLocation
      printf("  nI / nJ / nK = %ld / %ld / %ld : wil_cart %ld : nid = ",nI,nJ,nK,wil_cart);
      for (k=0; k<NPE_3D_Q1; k++) {
        long int nid = element[wil_cart*NPE_3D_Q1+k];
        printf("%ld ",nid);
      }
      printf("\n");
      
      printf("  [vertex] ");
      for (k=0; k<NPE_3D_Q1; k++) {
        printf("(%+1.8e , %+1.8e , %+1.8e) ",vertex[3*k+0],vertex[3*k+1],vertex[3*k+2] );
      }
      printf("\n");
#endif

      /* convert global (domain) xi TO local (element) xi  */
      xi0   = -1.0 + nI*dxi;
      eta0  = -1.0 + nJ*deta;
      zeta0 = -1.0 + nK*dzeta;
      
      /*  Gxi-(-1)/2 = (xp - x0)/dx */
      //      Lxip[0] = 0.5*(Gxip[0]+1.0)*dxi  + xi0;
      //      Lxip[1] = 0.5*(Gxip[1]+1.0)*deta + eta0;
      // x*-x*0/dx = (x+1)/2
      Lxip[0] = 2.0*(Gxip[0] - xi0  )/dxi   - 1.0;
      Lxip[1] = 2.0*(Gxip[1] - eta0 )/deta  - 1.0;
      Lxip[2] = 2.0*(Gxip[2] - zeta0)/dzeta - 1.0;
      
#ifdef PTAT3D_DBG_PointLocation
      printf("  Lxi,Lxeta,Lzeta = (%+1.8e , %+1.8e , %+1.8e) : nI,nJ,Nk (%ld, %ld, %ld)\n",Lxip[0],Lxip[1],Lxip[2],nI,nJ,nK);
#endif
      _compute_F_3dQ1(Lxip,vertex,marker_p.coor,f);
#ifdef PTAT3D_DBG_PointLocation
      if (monitor) printf("%4d InverseMapping : F = (%+1.8e , %+1.8e , %+1.8e) : xi = (%+1.8e , %+1.8e , %+1.8e)\n",its,f[0],f[1],f[2],Lxip[0],Lxip[1],Lxip[2]);
#endif
      /* Check for convergence */
      F2 = f[0]*f[0] + f[1]*f[1] + f[2]*f[2];
      if (F2 < tolerance2) {
        if (monitor) printf("%4d InverseMapping : converged : Norm of F %1.8e\n",its,ftype_sqrt(F2));
        point_found = 1;
        break;
      }
      
      /* compute update */
      _compute_J_3dQ1(Lxip,vertex,Jacobian);
      _compute_deltaX_3d(Jacobian,f,h);
      Lxip[0] += 10.0e-1 * h[0];
      Lxip[1] += 10.0e-1 * h[1];
      Lxip[2] += 10.0e-1 * h[2];
#ifdef PTAT3D_DBG_PointLocation
      printf("  [delta] = (%+1.8e , %+1.8e , %+1.8e)\n",h[0],h[1],h[2]);
      printf("  [corrected] Lxi,Leta,Lzeta = (%+1.8e , %+1.8e , %+1.8e)\n",Lxip[0],Lxip[1],Lxip[2]);
#endif
      residual2 = h[0]*h[0] + h[1]*h[1] + h[2]*h[2];
      if (residual2 < tolerance2) {
        if (monitor) printf("%4d InverseMapping : converged : Norm of correction %1.8e\n",its,ftype_sqrt(residual2));
        point_found = 1;
        break;
      }
      
      /* convert Lxip => Gxip */
      //      xi0  = -1.0 + nI*dxi;
      //      eta0 = -1.0 + nJ*deta;
      
      //      Gxip[0] = 2.0 * (Lxip[0]-xi0)/dxi   - 1.0;
      //      Gxip[1] = 2.0 * (Lxip[1]-eta0)/deta - 1.0;
      // x*-x*0/dx = (x+1)/2
      Gxip[0] = dxi   * (Lxip[0] + 1.0)/2.0 + xi0;
      Gxip[1] = deta  * (Lxip[1] + 1.0)/2.0 + eta0;
      Gxip[2] = dzeta * (Lxip[2] + 1.0)/2.0 + zeta0;
#ifdef PTAT3D_DBG_PointLocation
      printf("  [Gxi] = (%+1.8e , %+1.8e , %+1.8e)\n",Gxip[0],Gxip[1],Gxip[2]);
#endif
      
      for (d=0; d<NSD_3D; d++) {
        if (Gxip[d] < -1.0) {
          Gxip[d] = -1.0;
#ifdef PTAT3D_DBG_PointLocation
          printf("  correction outside box: correcting\n");
#endif
        }
        
        if (Gxip[d] > 1.0) {
          Gxip[d] = 1.0;
#ifdef PTAT3D_DBG_PointLocation
          printf("  correction outside box: correcting\n");
#endif
        }
      }
      
      its++;
    } while (its < max_its);
    
    if (monitor && (point_found == 0)) {
      if (its >= max_its) {
        printf("%4d %s : Reached maximum iterations (%d) without converging.\n",its,__FUNCTION__,max_its);
      } else {
        printf("%4d %s : Newton broke down, diverged or stagnated after (%d) iterations without converging.\n",its,__FUNCTION__,its);
      }
    }
    
    /* if at the end of the solve, it still looks like the point is outside the mapped domain, mark point as not being found */
    if (ftype_fabs(Gxip[0]) > 1.0) { point_found = 0; }
    if (ftype_fabs(Gxip[1]) > 1.0) { point_found = 0; }
    if (ftype_fabs(Gxip[2]) > 1.0) { point_found = 0; }
    
    /* update local variables */
    if (point_found == 0) {
      Lxip[0] = NAN;
      Lxip[1] = NAN;
      Lxip[2] = NAN;
      wil_cart = -1;
    } else {
      /* convert Gxi to nInJ */
      nI = (Gxip[0] + 1.0)/dxi;
      nJ = (Gxip[1] + 1.0)/deta;
      nK = (Gxip[2] + 1.0)/dzeta;
      if (nI == mx) { nI--; }
      if (nJ == my) { nJ--; }
      if (nK == mz) { nK--; }
      
      if (nI >= mx) {
#ifdef PTAT3D_DBG_PointLocation
        printf("  nI too large -> point location failed\n");
#endif
        for (d=0; d<NSD_3D; d++) { xi_p[NSD_3D*p+d] = NAN; }
        el_p[p] = -1;
        continue;
      }
      if (nJ >= my) {
#ifdef PTAT3D_DBG_PointLocation
        printf("  nJ too large -> point location failed\n");
#endif
        for (d=0; d<NSD_3D; d++) { xi_p[NSD_3D*p+d] = NAN; }
        el_p[p] = -1;
        continue;
      }
      if (nK >= mz) {
#ifdef PTAT3D_DBG_PointLocation
        printf("  nK too large -> point location failed\n");
#endif
        for (d=0; d<NSD_3D; d++) { xi_p[NSD_3D*p+d] = NAN; }
        el_p[p] = -1;
        continue;
      }
      
      /* convert global (domain) xi TO local (element) xi  */
      /*  Gxi-(-1)/2 = (xp - x0)/dx */
      xi0   = -1.0 + nI*dxi;
      eta0  = -1.0 + nJ*deta;
      zeta0 = -1.0 + nK*dzeta;
      
      // x*-x*0/dx = (x+1)/2
      Lxip[0] = 2.0*(Gxip[0] - xi0  )/dxi   - 1.0;
      Lxip[1] = 2.0*(Gxip[1] - eta0 )/deta  - 1.0;
      Lxip[2] = 2.0*(Gxip[2] - zeta0)/dzeta - 1.0;
      
      wil_cart = nI + nJ * mx + nK * mx * my;
    }
    
    /* set into vector */
    for (d=0; d<NSD_3D; d++) {
      marker_p.xi[d] = Lxip[d];
    }
    marker_p.wil = wil_cart;
    
#ifdef PTAT3D_DBG_PointLocation
    printf("  <<final>> xi,eta,zeta = (%+1.8e , %+1.8e , %+1.8e) [element = %ld]\n",Lxip[0],Lxip[1],Lxip[2],wil_cart);
#endif
    
    for (d=0; d<NSD_3D; d++) {
      xi_p[NSD_3D*p+d] = marker_p.xi[d];
    }
    el_p[p] = marker_p.wil;
  }
}

/*
 m[] : Number of elements in each direction (i,j).
 elements[] : Table defining the element->vertex map.
 coor[] : Vertex coordinates.
 npoints : Number of target points to locate.
 coor_p[] : Coordinates of target points {x0,y0, x1,y1, ...}.
 el_p[] : Element containing target points. If not found, assign a value of NAN.
 xi_p[] : Local coordinates of target points.
*/
void vt_structured_mesh_point_location_2(
      long int m[],long int elements[],float coor[],
      long int npoints, float coor_p[], long int el_p[], float xi_p[])
{
  ftype_ tolerance = 1.0e-6;
  int max_its = 20;
  int monitor = 0;
  
  InverseMappingDomain_2dQ1(tolerance,max_its,monitor,
                            coor,m[0],m[1],(const long int*)elements,
                            npoints,(const float*)coor_p,el_p,xi_p);
}

/*
 m[] : Number of elements in each direction (i,j,k).
 elements[] : Table defining the element->vertex map.
 coor[] : Vertex coordinates.
 npoints : Number of target points to locate.
 coor_p[] : Coordinates of target points {x0,y0,z0, x1,y1,z1, ...}.
 el_p[] : Element containing target points. If not found, assign a value of NAN.
 xi_p[] : Local coordinates of target points.
*/
void vt_structured_mesh_point_location_3(
      long int m[],long int elements[],float coor[],
      long int npoints, float coor_p[], long int el_p[], float xi_p[])
{
  ftype_ tolerance = 1.0e-6;
  int max_its = 20;
  int monitor = 0;
  
  InverseMappingDomain_3dQ1(tolerance,max_its,monitor,
                                 coor,m[0],m[1],m[2],(const long int*)elements,
                                 npoints,(const float*)coor_p,el_p,xi_p);
}


