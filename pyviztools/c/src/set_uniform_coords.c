
#include <math.h>

#include "vt_constants.h"


/*
  dir[]   - number of vertices in each direction (i,j)
  start[] - [x0, y0], len( 2 )
  end[]   - [x1, y1], len( 2 )
  coor[]  - len( 2 * nvert ), nvert = dir[0] * dir[1]
*/
void vt_set_uniform_coordinates_2(long int dir[],float start[],float end[],float coor[])
{
  int      d;
  long int i,j;
  float    dx[NSD_2D],xv[NSD_2D];
  
  for (d=0; d<NSD_2D; d++) {
    dx[d] = (end[d] - start[d])/(float)(dir[d]-1);
  }
  for (j=0; j<dir[1]; j++) {
    for (i=0; i<dir[0]; i++) {
      long int nid = i + j * dir[0];
      xv[0] = start[0] + i * dx[0];
      xv[1] = start[1] + j * dx[1];
      for (d=0; d<NSD_2D; d++) {
        coor[NSD_2D*nid+d] = xv[d];
      }
    }
  }
}

/*
  dir[]   - number of vertices in each direction (i,j,k)
  start[] - [x0, y0, z0], len( 3 )
  end[]   - [x1, y1, z1], len( 3 )
  coor[]  - len( 3 * nvert ), nvert = dir[0] * dir[1] * dir[2]
*/
void vt_set_uniform_coordinates_3(long int dir[],float start[],float end[],float coor[])
{
  int      d;
  long int i,j,k;
  float    dx[NSD_3D],xv[NSD_3D];
  
  for (d=0; d<NSD_3D; d++) {
    dx[d] = (end[d] - start[d])/(float)(dir[d]-1);
  }
  for (k=0; k<dir[2]; k++) {
    for (j=0; j<dir[1]; j++) {
      for (i=0; i<dir[0]; i++) {
        long int nid = i + j * dir[0] + k * dir[0] * dir[1];
        xv[0] = start[0] + i * dx[0];
        xv[1] = start[1] + j * dx[1];
        xv[2] = start[2] + k * dx[2];
        for (d=0; d<NSD_3D; d++) {
          coor[NSD_3D*nid+d] = xv[d];
        }
      }
    }
  }
}

