

#ifndef __vt_constants_h__
#define __vt_constants_h__

#define NSD_3D    3  /* number of spatial dimensions */
#define NPE_3D_Q1 8  /* nodes per element */

#define NSD_2D    2  /* number of spatial dimensions */
#define NPE_2D_Q1 4  /* nodes per element */

#endif
