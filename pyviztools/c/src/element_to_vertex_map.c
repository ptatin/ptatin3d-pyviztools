
#include <math.h>

#include "vt_constants.h"

/*
  m[]   - number of elements in each direction (i,j)
  e2v[] - len( 4 * ne ), ne = m[0] * m[1]
*/
void vt_e2v_q1_2d(long int m[],long int e2v[])
{
  int      v;
  long int i,j;
  long int ni,nj,eid;
  
  ni = m[0] + 1;
  nj = m[1] + 1;
  
  for (j=0; j<m[1]; j++) {
    for (i=0; i<m[0]; i++) {
      long int _e2v[NPE_2D_Q1];
      
      eid = i + j * m[0];
      
      _e2v[0] = i + j * ni;
      _e2v[1] = _e2v[0] + 1;
      _e2v[2] = _e2v[0] + ni;
      _e2v[3] = _e2v[0] + ni + 1;
      
      for (v=0; v<NPE_2D_Q1; v++) {
        e2v[NPE_2D_Q1 * eid + v] = _e2v[v];
      }
    }
  }
}

/*
  m[]   - number of elements in each direction (i,j,k)
  e2v[] - len( 8 * ne ), ne = m[0] * m[1] * m[2]
*/
void vt_e2v_q1_3d(long int m[],long int e2v[])
{
  int      v;
  long int i,j,k;
  long int ni,nj,nk,eid;
  
  ni = m[0] + 1;
  nj = m[1] + 1;
  nk = m[2] + 1;
  
  for (k=0; k<m[2]; k++) {
    for (j=0; j<m[1]; j++) {
      for (i=0; i<m[0]; i++) {
        long int _e2v[NPE_3D_Q1];
        
        eid = i + j * m[0] + k * m[0] * m[1];
        
        _e2v[0] = i + j * ni + k * ni * nj;
        _e2v[1] = _e2v[0] + 1;
        _e2v[2] = _e2v[0] + ni;
        _e2v[3] = _e2v[0] + ni + 1;
        
        _e2v[4+0] = i + j * ni + (k+1) * ni * nj;
        _e2v[4+1] = _e2v[4] + 1;
        _e2v[4+2] = _e2v[4] + ni;
        _e2v[4+3] = _e2v[4] + ni + 1;
        
        for (v=0; v<NPE_3D_Q1; v++) {
          e2v[NPE_3D_Q1 * eid + v] = _e2v[v];
        }
      }
    }
  }
}
