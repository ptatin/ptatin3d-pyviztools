
#include <math.h>

void vt_gauss_smooth(
                   long int msize[],
                   float ds[],
                   float *_avd_region,
                   float *_avd_region_gs)
{
  long int nx,ny,nz,i,j,k,ii,jj,kk,ck,cj,ci,d;
  float    gs_h[3],gs_sigma;
  long int ispan[3],jspan[3],kspan[3];
  
  nx = msize[0] + 1;
  ny = msize[1] + 1;
  nz = msize[2] + 1;
  
  gs_h[0] = ds[0];
  gs_h[1] = ds[1];
  gs_h[2] = ds[2];
  
  gs_sigma = ds[0];
  if (ds[1] > gs_sigma) { gs_sigma = ds[1]; }
  if (ds[2] > gs_sigma) { gs_sigma = ds[2]; }
  gs_sigma = 2.0 * gs_sigma;

  
  for (k=0; k<msize[2]; k++) {
    
    ck = 0;
    for (d=k-1; d<=k+1; d++) {
      if (d < 0) continue;
      if (d >= msize[2]) continue;
      kspan[ck] = d;
      ck++;
    }
    
    
    for (j=0; j<msize[1]; j++) {
      
      cj = 0;
      for (d=j-1; d<=j+1; d++) {
        if (d < 0) continue;
        if (d >= msize[1]) continue;
        jspan[cj] = d;
        cj++;
      }

      
      for (i=0; i<msize[0]; i++) {
        float kernel_sum,field_gs = 0.0;
        
        ci = 0;
        for (d=i-1; d<=i+1; d++) {
          if (d < 0) continue;
          if (d >= msize[0]) continue;
          ispan[ci] = d;
          ci++;
        }

        
        kernel_sum = 0.0;
        field_gs = 0.0;
        
        for (kk=0; kk<ck; kk++) {
          for (jj=0; jj<cj; jj++) {
            for (ii=0; ii<ci; ii++) {
              float    delta_i,delta_j,delta_k;
              float    arg = 0.0;
              long int idx;
              float    kernel;
            
              delta_i = (float)(i - ispan[ii]);
              delta_j = (float)(j - jspan[jj]);
              delta_k = (float)(k - kspan[kk]);
              
              arg  = (gs_h[0]*gs_h[0]) * ( delta_i * delta_i );
              arg += (gs_h[1]*gs_h[1]) * ( delta_j * delta_j );
              arg += (gs_h[2]*gs_h[2]) * ( delta_k * delta_k );

              arg = 0.5 * arg / (gs_sigma*gs_sigma);
              
              kernel = exp( -arg );
              
              kernel_sum += kernel;
              
              idx = ispan[ii] + jspan[jj] * msize[0] + kspan[kk] * msize[0] * msize[1];
              
              field_gs += _avd_region[idx] * kernel;
              
            }
          }
        }

        field_gs = field_gs / kernel_sum;
        
        _avd_region_gs[i + j * msize[0] + k * msize[0] * msize[1]] = field_gs;
        
      }
    }
  }
  
}

/*
def c_gauss_smooth(mesh, dx, dy, dz, cfield):
  _msize = np.zeros(3, dtype=np.int64)
  _msize[0] = mesh.m
  _msize[1] = mesh.n
  _msize[2] = mesh.p

  _ds = np.zeros(3, dtype=np.float32)
  _ds[0] = dx
  _ds[1] = dy
  _ds[2] = dz


  # If the input cfield is not float32, make a copy
  if cfield.dtype != np.float32:
    _avd_region = np.asarray(cfield, np.float32)
  else:
    _avd_region = cfield

  _avd_region_gs = np.zeros(cfield.shape, dtype=np.float32)

  #vt_gauss_smooth(
  #                 long int msize[],
  #                 float ds[],
  #                 float *_avd_region,
  #                 float *_avd_region_gs)

  return _avd_region_gs
*/
