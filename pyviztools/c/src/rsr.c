
#include <viztools.h>
#include <complex.h>

#define SQR(x)      ((x)*(x))                        // x^2
#define SQR_ABS(x)  (SQR(creal(x)) + SQR(cimag(x)))  // |x|^2

#define FLOAT64_EQUALITY(a, b) ( (fabs((a)-(b)) < 1.0e-20) ? 1 : 0 )

static int zheevj3(double complex A[3][3], double complex Q[3][3], double w[3]);

static void compute_eigV(long int n_points, double E[], double eig_vec[], double eig_val[]);


// ----------------------------------------------------------------------------
static int zheevj3(double complex A[3][3], double complex Q[3][3], double w[3])
// ----------------------------------------------------------------------------
// Numerical diagonalization of 3x3 matrcies
// Copyright (C) 2006  Joachim Kopp
// ----------------------------------------------------------------------------
// Calculates the eigenvalues and normalized eigenvectors of a hermitian 3x3
// matrix A using the Jacobi algorithm.
// The upper triangular part of A is destroyed during the calculation,
// the diagonal elements are read but not destroyed, and the lower
// triangular elements are not referenced at all.
// ----------------------------------------------------------------------------
// Parameters:
//   A: The hermitian input matrix
//   Q: Storage buffer for eigenvectors
//   w: Storage buffer for eigenvalues
// ----------------------------------------------------------------------------
// Return value:
//   0: Success
//  -1: Error (no convergence)
// ----------------------------------------------------------------------------
{
  const int n = 3;
  double sd, so;                  // Sums of diagonal resp. off-diagonal elements
  double complex s, t;            // sin(phi), tan(phi) and temporary storage
  double c;                       // cos(phi)
  double g, h, z;                 // Temporary storage
  double thresh;
  int i,nIter,p,q,r;
  
  // Initialize Q to the identitity matrix
#ifndef EVALS_ONLY
  for (i=0; i < n; i++) {
    Q[i][i] = 1.0;
    for (int j=0; j < i; j++)
    Q[i][j] = Q[j][i] = 0.0;
  }
#endif
  
  // Initialize w to diag(A)
  for (i=0; i < n; i++)
  w[i] = creal(A[i][i]);
  
  // Calculate SQR(tr(A))
  sd = 0.0;
  for (i=0; i < n; i++)
  sd += fabs(w[i]);
  sd = SQR(sd);
  
  // Main iteration loop
  for (nIter=0; nIter < 50; nIter++) {
    // Test for convergence
    so = 0.0;
    for (p=0; p < n; p++)
    for (q=p+1; q < n; q++)
    so += fabs(creal(A[p][q])) + fabs(cimag(A[p][q]));
    if (so == 0.0)
    return(0);
    
    if (nIter < 4)
    thresh = 0.2 * so / SQR(n);
    else
    thresh = 0.0;
    
    // Do sweep
    for (p=0; p < n; p++)
    for (q=p+1; q < n; q++) {
      g = 100.0 * (fabs(creal(A[p][q])) + fabs(cimag(A[p][q])));
      if (nIter > 4  &&  fabs(w[p]) + g == fabs(w[p])
          &&  fabs(w[q]) + g == fabs(w[q])) {
        A[p][q] = 0.0;
      }
      else if (fabs(creal(A[p][q])) + fabs(cimag(A[p][q])) > thresh) {
        // Calculate Jacobi transformation
        h = w[q] - w[p];
        if (fabs(h) + g == fabs(h))
        t = A[p][q] / h;
        else {
          if (h < 0.0)
          t = -2.0 * A[p][q] / (sqrt(SQR(h) + 4.0*SQR_ABS(A[p][q])) - h);
          else if (h == 0.0)
          t = A[p][q] * (1.0 / cabs(A[p][q]));  // A[p][q]/fabs(A[p][q]) could cause overflows
          else
          t = 2.0 * A[p][q] / (sqrt(SQR(h) + 4.0*SQR_ABS(A[p][q])) + h);
        }
        c = 1.0/sqrt(1.0 + SQR_ABS(t));
        s = t * c;
        z = creal(t * conj(A[p][q]));
        
        // Apply Jacobi transformation
        A[p][q] = 0.0;
        w[p] -= z;
        w[q] += z;
        for (r=0; r < p; r++) {
          t = A[r][p];
          A[r][p] = c*t - conj(s)*A[r][q];
          A[r][q] = s*t + c*A[r][q];
        }
        for (r=p+1; r < q; r++) {
          t = A[p][r];
          A[p][r] = c*t - s*conj(A[r][q]);
          A[r][q] = s*conj(t) + c*A[r][q];
        }
        for (r=q+1; r < n; r++) {
          t = A[p][r];
          A[p][r] = c*t - s*A[q][r];
          A[q][r] = conj(s)*t + c*A[q][r];
        }
        
        // Update eigenvectors
#ifndef EVALS_ONLY
        for (r=0; r < n; r++) {
          t = Q[r][p];
          Q[r][p] = c*t - conj(s)*Q[r][q];
          Q[r][q] = s*t + c*Q[r][q];
        }
#endif
      }
    }
  }
  
  return(-1);
}

static void compute_eigV(long int n_points, double E[], double eig_vec[], double eig_val[])
{
  long int       n;
  int            i,j,cnt,ierr;
  double complex A[3][3],Q[3][3];
  double         w[3];
  
  for (n=0; n<n_points; n++){
    /* Construct the 3x3 tensor from 1D array */
    cnt = 0;
    for (i=0; i<3; i++) {
      for (j=0; j<3; j++) {
        A[i][j] = E[9*n + cnt];
        cnt++;
      }
    }
    
    /* Compute eigen values and vectors */
    ierr = zheevj3(A, Q, w);
    
    cnt = 0;
    for (i=0; i<3; i++) {
      for (j=0; j<3; j++) {
        eig_vec[9*n + cnt] = (double)Q[i][j];
        cnt++;
      }
    }
    
    cnt = 0;
    for (i=0; i<3; i++){
      eig_val[3*n + cnt] = w[i];
      cnt++;
    }
  }
}


/* Function to compute the regime stress ratio (RSR)
 E is the strain rate tensor (should also work with the stress tensor)
 E should be of the form of a flattened array.
*/
void vt_compute_rsr_all(long int n_points, double E[], float RSR[])
{
  long int n;
  double *eig_vec,*eig_val;
  
  /* Allocate memory for arrays */
  eig_vec = (double *)malloc(9*n_points * sizeof(double));
  eig_val = (double *)malloc(3*n_points * sizeof(double));
  
  /* Compute eigen vectors and values */
  compute_eigV(n_points,E,eig_vec,eig_val);
  
  for (n=0; n<n_points; n++){
    int    p;
    double v0,v1,v2,w0,w1,w2;
    double vertical,S_V;
    double sigma1,sigma2,sigma3,R;
    
    /* v0,v1,v2 is the dot product between each eigen vectors and the vertical unit vector [0, 1, 0]
     or simply the value of the y coordinate of each eigen vector */
    v0 = eig_vec[9*n + 1];
    v1 = eig_vec[9*n + 4];
    v2 = eig_vec[9*n + 7];
    /* Unpack eigen values for current point*/
    /* Scale by -1.0 to match the physical convention */
    w0 = -eig_val[3*n + 0];
    w1 = -eig_val[3*n + 1];
    w2 = -eig_val[3*n + 2];
    
    /* Determine which vector is the closest to the vertical vector */
    if (v0 > v1 && v0 > v2) {
      vertical = v0;
    } else if (v1 > v0 && v1 > v2) {
      vertical = v1;
    } else {
      vertical = v2;
    }
    
    /* Determine sigma vertical */
    if (vertical == v0) {
      S_V = w0;
    } else if (vertical == v1) {
      S_V = w1;
    } else {
      S_V = w2;
    }
    
    /* Determine sigma1, sigma2 and sigma3 */
    if ((w0 > w1) && (w0 > w2)) {
      sigma1 = w0;
      if (w1 > w2) {
        sigma2 = w1;
        sigma3 = w2;
      } else {
        sigma2 = w2;
        sigma3 = w1;
      }
    } else if ((w1 > w0) && (w1 > w2)) {
      sigma1 = w1;
      if (w0 > w2) {
        sigma2 = w0;
        sigma3 = w2;
      } else {
        sigma2 = w2;
        sigma3 = w0;
      }
    } else {
      sigma1 = w2;
      if (w0 > w1) {
        sigma2 = w0;
        sigma3 = w1;
      } else {
        sigma2 = w1;
        sigma3 = w0;
      }
    }
    
    if (sigma1 == S_V) {
      p = 0;
    } else if (sigma3 == S_V) {
      p = 2;
    } else {
      p = 1;
    }
    
    R = (sigma2 - sigma3)/(sigma1 - sigma3);
    RSR[n] = (p + 0.5) + pow(-1.0,p) * (R - 0.5);
  }
  
  free(eig_vec);
  free(eig_val);
}

void vt_compute_rsr(long int n_points, double E[], float RSR[])
{
  long int n;
  double   eig_vec[9],eig_val[3];
  
  for (n=0; n<n_points; n++){
    int    p,ierr;
    double v0,v1,v2,w0,w1,w2;
    double vertical,S_V;
    double sigma1,sigma2,sigma3,R;

    /* Compute eigen values and vectors */
    {
      int            i,j,cnt;
      double complex A[3][3],Q[3][3];
      
      /* Construct the 3x3 tensor from 1D array */
      cnt = 0;
      for (i=0; i<3; i++) {
        for (j=0; j<3; j++) {
          A[i][j] = E[9*n + cnt];
          cnt++;
        }
      }
      
      ierr = zheevj3(A, Q, eig_val);
        
      cnt = 0;
      for (i=0; i<3; i++) {
        for (j=0; j<3; j++) {
          eig_vec[cnt] = Q[i][j];
          cnt++;
        }
      }
    }
    /* Factorization failed, set NAN */
    if (ierr != 0) {
      RSR[n] = NAN;
      continue;
    }
    
    /* v0,v1,v2 is the dot product between each eigen vectors and the vertical unit vector [0, 1, 0]
     or simply the value of the y coordinate of each eigen vector */
    v0 = eig_vec[1];
    v1 = eig_vec[4];
    v2 = eig_vec[7];
    /* Unpack eigen values for current point */
    /* Scale by -1.0 to match the physical convention */
    w0 = -eig_val[0];
    w1 = -eig_val[1];
    w2 = -eig_val[2];
    
    /* Determine which vector is the closest to the vertical vector */
    if ((v0 > v1) && (v0 > v2)) {
      vertical = v0;
    } else if ((v1 > v0) && (v1 > v2)) {
      vertical = v1;
    } else {
      vertical = v2;
    }
    
    /* Determine sigma vertical */
    //if (vertical == v0) {
    if (FLOAT64_EQUALITY(vertical, v0)) {
      S_V = w0;
    //} else if (vertical == v1) {
    } else if (FLOAT64_EQUALITY(vertical, v1)) {
      S_V = w1;
    } else {
      S_V = w2;
    }
    
    /* Determine sigma1, sigma2 and sigma3 */
    if ((w0 > w1) && (w0 > w2)) {
      sigma1 = w0;
      if (w1 > w2) {
        sigma2 = w1;
        sigma3 = w2;
      } else {
        sigma2 = w2;
        sigma3 = w1;
      }
    } else if ((w1 > w0) && (w1 > w2)) {
      sigma1 = w1;
      if (w0 > w2) {
        sigma2 = w0;
        sigma3 = w2;
      } else {
        sigma2 = w2;
        sigma3 = w0;
      }
    } else {
      sigma1 = w2;
      if (w0 > w1) {
        sigma2 = w0;
        sigma3 = w1;
      } else {
        sigma2 = w1;
        sigma3 = w0;
      }
    }
    
    //if (sigma1 == S_V) {
    if (FLOAT64_EQUALITY(sigma1, S_V)) {
      p = 0;
    //} else if (sigma3 == S_V) {
    } else if (FLOAT64_EQUALITY(sigma3, S_V)) {
      p = 2;
    } else {
      p = 1;
    }
    
    R = (sigma2 - sigma3)/(sigma1 - sigma3);
    RSR[n] = (p + 0.5) + pow(-1.0,p) * (R - 0.5);
  }
}
