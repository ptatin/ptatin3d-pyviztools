
#include <viztools.h>

#define ftype_      float

static void P3D_ConstructNi_Q1_3D(ftype_ _xi[],ftype_ Ni[])
{
  ftype_ xi   = _xi[0];
  ftype_ eta  = _xi[1];
  ftype_ zeta = _xi[2];

  Ni[0] = 0.125 * ( 1.0 - xi ) * ( 1.0 - eta ) * ( 1.0 - zeta );
  Ni[1] = 0.125 * ( 1.0 + xi ) * ( 1.0 - eta ) * ( 1.0 - zeta );
  Ni[2] = 0.125 * ( 1.0 - xi ) * ( 1.0 + eta ) * ( 1.0 - zeta );
  Ni[3] = 0.125 * ( 1.0 + xi ) * ( 1.0 + eta ) * ( 1.0 - zeta );

  Ni[4] = 0.125 * ( 1.0 - xi ) * ( 1.0 - eta ) * ( 1.0 + zeta );
  Ni[5] = 0.125 * ( 1.0 + xi ) * ( 1.0 - eta ) * ( 1.0 + zeta );
  Ni[6] = 0.125 * ( 1.0 - xi ) * ( 1.0 + eta ) * ( 1.0 + zeta );
  Ni[7] = 0.125 * ( 1.0 + xi ) * ( 1.0 + eta ) * ( 1.0 + zeta );
}

/*
 nelements : Number of elements..
 elements[] : Table defining the element->vertex map.
 bs: Block size of the field.
 field[] : Field to interpolate.
 npoints : Number of target points to interpolate to.
 coor_p[] : Coordinates of target points to interpolate to {x0,y0,z0, x1,y1,z1, ...}.
 el_p[] : Element containing target points.
 xi_p[] : Local coordinates of target points.
 field_p[] : Interpolated field at target points.
*/
void vt_structured_mesh_interpolate_3(
      long int nelements, long int elements[], long int bs, float field[],
      long int npoints, long int el_p[], float xi_p[],
      float field_p[])
{
  long int k,b,i;
  ftype_ val_i,Ni[NPE_3D_Q1];
  const long int *vidx;

  for (k=0; k<npoints; k++) {

    if (el_p[k] < 0 || el_p[k] >= nelements) {
      for (b=0; b<bs; b++) {
        field_p[bs*k + b] = NAN;
      }
      continue;
    }

    P3D_ConstructNi_Q1_3D(&xi_p[3*k],Ni);
    vidx = (const long int*)&elements[ NPE_3D_Q1*el_p[k] ];

    for (b=0; b<bs; b++) {
      val_i = 0.0;
      for (i=0; i<NPE_3D_Q1; i++) {
        val_i += Ni[i] * field[bs*vidx[i] + b];
      }
      field_p[bs*k + b] = val_i;
    }
  }
}
