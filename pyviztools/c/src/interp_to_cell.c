
#include "viztools.h"


void vt_interp_to_cell(
  long int ne,
  long int bs,
  long int elements[],
  float field[],
  float field_c[]
)
{
  long int e;
  int      i,b;
  
  for (e=0; e<ne; e++) {
    long int *element = &elements[NPE_3D_Q1*e];
    
    for (b=0; b<bs; b++) {
      float sum = 0.0;
      for (i=0; i<NPE_3D_Q1; i++) {
        long int vid = element[i];
        sum += field[bs * vid + b];
      }
      field_c[bs * e + b] = sum;
    }
  }
}
