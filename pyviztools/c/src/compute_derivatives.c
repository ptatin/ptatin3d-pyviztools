
#include "viztools.h"


static void _eval_basis_gradient_xi0(float DNi[NSD_3D][NPE_3D_Q1])
{
  float _xi[] = {0, 0, 0};

  DNi[0][0] = 0.125 * (0.0 - 1.0) * (1.0 - _xi[1]) * (1.0 - _xi[2]);
  DNi[0][1] = 0.125 * (0.0 + 1.0) * (1.0 - _xi[1]) * (1.0 - _xi[2]);
  DNi[0][2] = 0.125 * (0.0 - 1.0) * (1.0 + _xi[1]) * (1.0 - _xi[2]);
  DNi[0][3] = 0.125 * (0.0 + 1.0) * (1.0 + _xi[1]) * (1.0 - _xi[2]);
  DNi[0][4] = 0.125 * (0.0 - 1.0) * (1.0 - _xi[1]) * (1.0 + _xi[2]);
  DNi[0][5] = 0.125 * (0.0 + 1.0) * (1.0 - _xi[1]) * (1.0 + _xi[2]);
  DNi[0][6] = 0.125 * (0.0 - 1.0) * (1.0 + _xi[1]) * (1.0 + _xi[2]);
  DNi[0][7] = 0.125 * (0.0 + 1.0) * (1.0 + _xi[1]) * (1.0 + _xi[2]);


  DNi[1][0] = 0.125 * (1.0 - _xi[0]) * (0.0 - 1.0) * (1.0 - _xi[2]);
  DNi[1][1] = 0.125 * (1.0 + _xi[0]) * (0.0 - 1.0) * (1.0 - _xi[2]);
  DNi[1][2] = 0.125 * (1.0 - _xi[0]) * (0.0 + 1.0) * (1.0 - _xi[2]);
  DNi[1][3] = 0.125 * (1.0 + _xi[0]) * (0.0 + 1.0) * (1.0 - _xi[2]);
  DNi[1][4] = 0.125 * (1.0 - _xi[0]) * (0.0 - 1.0) * (1.0 + _xi[2]);
  DNi[1][5] = 0.125 * (1.0 + _xi[0]) * (0.0 - 1.0) * (1.0 + _xi[2]);
  DNi[1][6] = 0.125 * (1.0 - _xi[0]) * (0.0 + 1.0) * (1.0 + _xi[2]);
  DNi[1][7] = 0.125 * (1.0 + _xi[0]) * (0.0 + 1.0) * (1.0 + _xi[2]);


  DNi[2][0] = 0.125 * (1.0 - _xi[0]) * (1.0 - _xi[1]) * (0.0 - 1.0);
  DNi[2][1] = 0.125 * (1.0 + _xi[0]) * (1.0 - _xi[1]) * (0.0 - 1.0);
  DNi[2][2] = 0.125 * (1.0 - _xi[0]) * (1.0 + _xi[1]) * (0.0 - 1.0);
  DNi[2][3] = 0.125 * (1.0 + _xi[0]) * (1.0 + _xi[1]) * (0.0 - 1.0);
  DNi[2][4] = 0.125 * (1.0 - _xi[0]) * (1.0 - _xi[1]) * (0.0 + 1.0);
  DNi[2][5] = 0.125 * (1.0 + _xi[0]) * (1.0 - _xi[1]) * (0.0 + 1.0);
  DNi[2][6] = 0.125 * (1.0 - _xi[0]) * (1.0 + _xi[1]) * (0.0 + 1.0);
  DNi[2][7] = 0.125 * (1.0 + _xi[0]) * (1.0 + _xi[1]) * (0.0 + 1.0);
}


static void _p3d_evaluate_geometry_single_point(
                                         float GNI[NSD_3D][NPE_3D_Q1],
                                         float el_coords[NSD_3D*NPE_3D_Q1],
                                         float dNudx[NPE_3D_Q1],
                                         float dNudy[NPE_3D_Q1],
                                         float dNudz[NPE_3D_Q1])
{
  int   k;
  float t4,t6,t8,t10,t12,t14,t17;
  float J[3][3],iJ[3][3],detJp;
  
  J[0][0] = J[0][1] = J[0][2] = 0.0;
  J[1][0] = J[1][1] = J[1][2] = 0.0;
  J[2][0] = J[2][1] = J[2][2] = 0.0;

  for (k=0; k<NPE_3D_Q1; k++) {
    float xc = el_coords[NSD_3D*k+0];
    float yc = el_coords[NSD_3D*k+1];
    float zc = el_coords[NSD_3D*k+2];
    
    J[0][0] += GNI[0][k] * xc ;
    J[0][1] += GNI[0][k] * yc ;
    J[0][2] += GNI[0][k] * zc ;
    
    J[1][0] += GNI[1][k] * xc ;
    J[1][1] += GNI[1][k] * yc ;
    J[1][2] += GNI[1][k] * zc ;
    
    J[2][0] += GNI[2][k] * xc ;
    J[2][1] += GNI[2][k] * yc ;
    J[2][2] += GNI[2][k] * zc ;
  }
  
  detJp = J[0][0]*(J[1][1]*J[2][2] - J[1][2]*J[2][1])
        - J[0][1]*(J[1][0]*J[2][2] - J[1][2]*J[2][0])
        + J[0][2]*(J[1][0]*J[2][1] - J[1][1]*J[2][0]);
  
  t4  = J[2][0] * J[0][1];
  t6  = J[2][0] * J[0][2];
  t8  = J[1][0] * J[0][1];
  t10 = J[1][0] * J[0][2];
  t12 = J[0][0] * J[1][1];
  t14 = J[0][0] * J[1][2];
  t17 = 0.1e1 / (t4 * J[1][2] - t6 * J[1][1] - t8 * J[2][2] + t10 * J[2][1] + t12 * J[2][2] - t14 * J[2][1]);
  
  iJ[0][0] = (J[1][1] * J[2][2] - J[1][2] * J[2][1]) * t17;  // 4
  iJ[0][1] = -(J[0][1] * J[2][2] - J[0][2] * J[2][1]) * t17; // 5
  iJ[0][2] = (J[0][1] * J[1][2] - J[0][2] * J[1][1]) * t17;  // 4
  iJ[1][0] = -(-J[2][0] * J[1][2] + J[1][0] * J[2][2]) * t17;// 6
  iJ[1][1] = (-t6 + J[0][0] * J[2][2]) * t17;                // 4
  iJ[1][2] = -(-t10 + t14) * t17;                            // 4
  iJ[2][0] = (-J[2][0] * J[1][1] + J[1][0] * J[2][1]) * t17; // 5
  iJ[2][1] = -(-t4 + J[0][0] * J[2][1]) * t17;               // 5
  iJ[2][2] = (-t8 + t12) * t17;                              // 3
  
  /* shape function derivatives */
  for (k=0; k<NPE_3D_Q1; k++) {
    dNudx[k] = iJ[0][0]*GNI[0][k] + iJ[0][1]*GNI[1][k] + iJ[0][2]*GNI[2][k];
    
    dNudy[k] = iJ[1][0]*GNI[0][k] + iJ[1][1]*GNI[1][k] + iJ[1][2]*GNI[2][k];
    
    dNudz[k] = iJ[2][0]*GNI[0][k] + iJ[2][1]*GNI[1][k] + iJ[2][2]*GNI[2][k];
  }
}


void vt_compute_derivatives_scalar(
  long int nv,
  long int ne,
  long int elements[],
  float coor[],
  float field[],
  float grad[]
)
{
  float    elfield[NPE_3D_Q1],elcoor[NSD_3D*NPE_3D_Q1],elgrad[NSD_3D];
  long int e;
  int      i,d;
  float    gradNi[NSD_3D][NPE_3D_Q1];
  float    gradx[NPE_3D_Q1],grady[NPE_3D_Q1],gradz[NPE_3D_Q1];
  
  _eval_basis_gradient_xi0(gradNi);
  
  for (e=0; e<ne; e++) {
    long int *element = &elements[NPE_3D_Q1*e];
    
    for (i=0; i<NPE_3D_Q1; i++) {
      long int vid = element[i];
      
      elfield[i] = field[vid];
      for (d=0; d<NSD_3D; d++) {
        elcoor[NSD_3D*i+d] = coor[NSD_3D*vid+d];
      }
    }
    
    _p3d_evaluate_geometry_single_point(gradNi,elcoor,gradx,grady,gradz);

    memset(elgrad,0,sizeof(float)*NSD_3D);
    
    for (i=0; i<NPE_3D_Q1; i++) {
      elgrad[0] += gradx[i] * elfield[i];
      elgrad[1] += grady[i] * elfield[i];
      elgrad[2] += gradz[i] * elfield[i];
    }
    
    for (d=0; d<NSD_3D; d++) {
      grad[NSD_3D*e+d] = elgrad[d];
    }
  }
}


void vt_compute_derivatives_vector(
                                         long int nv,
                                         long int ne,
                                         long int elements[],
                                         float coor[],
                                         float field[],
                                         float gradu[],float gradv[],float gradw[]
                                         )
{
  float    elfield[NSD_3D][NPE_3D_Q1],elcoor[NSD_3D*NPE_3D_Q1],elgrad[NSD_3D][NSD_3D];
  long int e;
  int      i,j,k,d;
  float    gradNi[NSD_3D][NPE_3D_Q1];
  float    gradx[NPE_3D_Q1],grady[NPE_3D_Q1],gradz[NPE_3D_Q1];
  
  _eval_basis_gradient_xi0(gradNi);
  
  for (e=0; e<ne; e++) {
    long int *element = &elements[NPE_3D_Q1*e];
    
    for (k=0; k<NPE_3D_Q1; k++) {
      long int vid = element[k];
      
      for (d=0; d<NSD_3D; d++) {
        elfield[d][k] = field[NSD_3D*vid+d];
      }
      
      for (d=0; d<NSD_3D; d++) {
        elcoor[NSD_3D*k+d] = coor[NSD_3D*vid+d];
      }
    }
    
    _p3d_evaluate_geometry_single_point(gradNi,elcoor,gradx,grady,gradz);
    
    for (i=0; i<NSD_3D; i++) for (j=0; j<NSD_3D; j++) elgrad[i][j] = 0;
      
    /* elgrad[i][j] = \partial u_i / \partial x_j */
    for (d=0; d<NSD_3D; d++) {
      for (k=0; k<NPE_3D_Q1; k++) {
        elgrad[0][d] += gradx[k] * elfield[d][k];
        elgrad[1][d] += grady[k] * elfield[d][k];
        elgrad[2][d] += gradz[k] * elfield[d][k];
      }
    }
    
    for (d=0; d<NSD_3D; d++) {
      gradu[NSD_3D*e+d] = elgrad[d][0]; /* du/dx, du/dy, du/dz */
      gradv[NSD_3D*e+d] = elgrad[d][1];
      gradw[NSD_3D*e+d] = elgrad[d][2];
    }
  }
}


static inline void JacobianInvert__NEV_256S(float dx[3][3][NQP][NEV_256S],float dxdet[NEV_256S])
{
  int i,j,k,e;
  
  for (i=0; i<NQP; i++) {
    float a[3][3][NEV_256S] ALIGN32;
    for (e=0; e<NEV_256S; e++) {
      float b0,b3,b6,det,idet;
      for (j=0; j<3; j++) {
        for (k=0; k<3; k++) {
          a[j][k][e] = dx[j][k][i][e];
        }
      }
      b0 =  (a[1][1][e]*a[2][2][e] - a[2][1][e]*a[1][2][e]);
      b3 = -(a[1][0][e]*a[2][2][e] - a[2][0][e]*a[1][2][e]);
      b6 =  (a[1][0][e]*a[2][1][e] - a[2][0][e]*a[1][1][e]);
      det = a[0][0][e]*b0 + a[0][1][e]*b3 + a[0][2][e]*b6;
      idet = 1.0 / det;
      dx[0][0][i][e] =  idet*b0;
      dx[0][1][i][e] = -idet*(a[0][1][e]*a[2][2][e] - a[2][1][e]*a[0][2][e]);
      dx[0][2][i][e] =  idet*(a[0][1][e]*a[1][2][e] - a[1][1][e]*a[0][2][e]);
      dx[1][0][i][e] =  idet*b3;
      dx[1][1][i][e] =  idet*(a[0][0][e]*a[2][2][e] - a[2][0][e]*a[0][2][e]);
      dx[1][2][i][e] = -idet*(a[0][0][e]*a[1][2][e] - a[1][0][e]*a[0][2][e]);
      dx[2][0][i][e] =  idet*b6;
      dx[2][1][i][e] = -idet*(a[0][0][e]*a[2][1][e] - a[2][0][e]*a[0][1][e]);
      dx[2][2][i][e] =  idet*(a[0][0][e]*a[1][1][e] - a[1][0][e]*a[0][1][e]);
      dxdet[e] = fabsf(det);
    }
  }
}

static inline void contract(float grad[3][NQP][NPE_3D_Q1],
                               float elx[][NPE_3D_Q1][NEV_256S],
                               float dx[][3][NQP][NEV_256S])
{
  int q,i,u_i,ee;
  
  for (q=0; q<NQP; q++) {
    for (i=0; i<NPE_3D_Q1; i++) {
      for (u_i=0; u_i<3; u_i++) {
        for (ee=0; ee<NEV_256S; ee++) {
          dx[u_i][0][q][ee] += grad[u_i][q][i] * elx[0][i][ee];
          dx[u_i][1][q][ee] += grad[u_i][q][i] * elx[1][i][ee];
          dx[u_i][2][q][ee] += grad[u_i][q][i] * elx[2][i][ee];
        }
      }
    }
  }
}


void vt_compute_derivatives_vector_tensor(
                                         long int nv,
                                         long int nel,
                                         long int elements[],
                                         float coor[],
                                         float field[],
                                         float gradu[],float gradv[],float gradw[]
                                         )
{
  int      ee,i,l,k,q,d;
  long int e;
  float    gradNi[NSD_3D][NPE_3D_Q1];
  float    gradN[3][NQP][NPE_3D_Q1];
  
  _eval_basis_gradient_xi0(gradNi);
  for (q=0; q<NQP; q++) {
    for (i=0; i<NPE_3D_Q1; i++) {
      gradN[0][q][i] = gradNi[0][i];
      gradN[1][q][i] = gradNi[1][i];
      gradN[2][q][i] = gradNi[2][i];
    }
  }

  for (e=0; e<nel; e+=NEV_256S) {
    float elu[3][NPE_3D_Q1][NEV_256S]={};
    float elx[3][NPE_3D_Q1][NEV_256S]={};
    float dx[3][3][NQP][NEV_256S];
    float dxdet[NEV_256S];
    float du[3][3][NQP][NEV_256S];
    float dux[3][3][NEV_256S];
    const int single = 0;
    
    for (i=0; i<NPE_3D_Q1; i++) {
      for (ee=0; ee<NEV_256S; ee++) {
        long int E = elements[NPE_3D_Q1*VTMIN(e+ee,nel-1)+i]; // Pad up to length NEV by duplicating last element
        for (l=0; l<3; l++) {
          elx[l][i][ee] = coor[3*E+l];
          elu[l][i][ee] = field[3*E+l];
        }
      }
    }

    memset(dx, 0, sizeof dx);
    // dx[u_i][0][q][ee] += gradN[u_i][q][i] * elx[0][i][ee];
    contract(gradN,elx,dx);

    JacobianInvert__NEV_256S(dx,dxdet);
   
    memset(du, 0, sizeof du);
    contract(gradN,elu,du);

    // quadrature action
    for (ee=0; ee<NEV_256S; ee++) {
      for (l=0; l<3; l++) { // fields
        for (k=0; k<3; k++) { // directions
          dux[k][l][ee] = du[0][l][single][ee] * dx[k][0][single][ee] + du[1][l][single][ee] * dx[k][1][single][ee] + du[2][l][single][ee] * dx[k][2][single][ee];
        }
      }
    }
    
    for (ee=0; ee<NEV_256S; ee++) {
      long int E = VTMIN(e+ee,nel-1); // Pad up to length NEV by duplicating last
      
      for (d=0; d<NSD_3D; d++) {
        gradu[NSD_3D*E+d] = dux[d][0][ee]; /* du/dx, du/dy, du/dz */
        gradv[NSD_3D*E+d] = dux[d][1][ee];
        gradw[NSD_3D*E+d] = dux[d][2][ee];
      }
    }
    
  }
}



#if defined(__AVX__)

void contract_AVX256S_v2(float grad[3][NQP][NPE_3D_Q1], //nqp
                                   float elx[][NPE_3D_Q1][NEV_256S], //3
                                   float dx[3][3][NQP][NEV_256S]) //3
{
  int q,i,u_i,d_i;
  
  u_i = 0;
  for (q=0; q<NQP; q++) {
    __m256 grad_e[] = { _mm256_setzero_ps(), _mm256_setzero_ps(), _mm256_setzero_ps() };
    for (i=0; i<NPE_3D_Q1; i++) {
      __m256 _grad  = _mm256_set1_ps(grad[u_i][q][i]);
      for (d_i=0; d_i<3; d_i++) {
        __m256 _elx = _mm256_load_ps(elx[d_i][i]);
        grad_e[d_i] = _mm256_fmadd_ps(_grad, _elx, grad_e[d_i]);
      }
    }
    _mm256_store_ps(dx[u_i][0][q],grad_e[0]);
    _mm256_store_ps(dx[u_i][1][q],grad_e[1]);
    _mm256_store_ps(dx[u_i][2][q],grad_e[2]);
  }
  
  
  u_i = 1;
  for (q=0; q<NQP; q++) {
    __m256 grad_e[] = { _mm256_setzero_ps(), _mm256_setzero_ps(), _mm256_setzero_ps() };
    for (i=0; i<NPE_3D_Q1; i++) {
      __m256 _grad  = _mm256_set1_ps(grad[u_i][q][i]);
      for (d_i=0; d_i<3; d_i++) {
        __m256 _elx = _mm256_load_ps(elx[d_i][i]);
        grad_e[d_i] = _mm256_fmadd_ps(_grad, _elx, grad_e[d_i]);
      }
    }
    _mm256_store_ps(dx[u_i][0][q],grad_e[0]);
    _mm256_store_ps(dx[u_i][1][q],grad_e[1]);
    _mm256_store_ps(dx[u_i][2][q],grad_e[2]);
  }
  
  
  u_i = 2;
  for (q=0; q<NQP; q++) {
    __m256 grad_e[] = { _mm256_setzero_ps(), _mm256_setzero_ps(), _mm256_setzero_ps() };
    for (i=0; i<NPE_3D_Q1; i++) {
      __m256 _grad  = _mm256_set1_ps(grad[u_i][q][i]);
      for (d_i=0; d_i<3; d_i++) {
        __m256 _elx = _mm256_load_ps(elx[d_i][i]);
        grad_e[d_i] = _mm256_fmadd_ps(_grad, _elx, grad_e[d_i]);
      }
    }
    _mm256_store_ps(dx[u_i][0][q],grad_e[0]);
    _mm256_store_ps(dx[u_i][1][q],grad_e[1]);
    _mm256_store_ps(dx[u_i][2][q],grad_e[2]);
  }
}

void vt_compute_derivatives_vector_AVX256(
                                                long int nv,
                                                long int nel,
                                                long int elements[],
                                                float coor[],
                                                float field[],
                                                float gradu[],float gradv[],float gradw[]
                                                )
{
  int      ee,i,l,k,q,d;
  long int e;
  float    gradNi[NSD_3D][NPE_3D_Q1] ALIGN32;
  float    gradN[3][NQP][NPE_3D_Q1]  ALIGN32;
  
  _eval_basis_gradient_xi0(gradNi);
  for (q=0; q<NQP; q++) {
    for (i=0; i<NPE_3D_Q1; i++) {
      gradN[0][q][i] = gradNi[0][i];
      gradN[1][q][i] = gradNi[1][i];
      gradN[2][q][i] = gradNi[2][i];
    }
  }
  
  for (e=0; e<nel; e+=NEV_256S) {
    float elu[3][NPE_3D_Q1][NEV_256S] ALIGN32;
    float elx[3][NPE_3D_Q1][NEV_256S] ALIGN32;
    float dx[3][3][NQP][NEV_256S]     ALIGN32;
    float dxdet[NEV_256S]             ALIGN32;
    float du[3][3][NQP][NEV_256S]     ALIGN32;
    float dux[3][3][NEV_256S]         ALIGN32;
    const int single = 0;
    
    for (i=0; i<NPE_3D_Q1; i++) {
      for (ee=0; ee<NEV_256S; ee++) {
        long int E = elements[NPE_3D_Q1*VTMIN(e+ee,nel-1)+i]; // Pad up to length NEV by duplicating last element
        for (l=0; l<3; l++) {
          elx[l][i][ee] = coor[3*E+l];
          elu[l][i][ee] = field[3*E+l];
        }
      }
    }
    
    memset(dx, 0, sizeof dx);
    // dx[u_i][0][q][ee] += gradN[u_i][q][i] * elx[0][i][ee];
    contract_AVX256S_v2(gradN,elx,dx);

    JacobianInvert__NEV_256S(dx,dxdet);

    memset(du, 0, sizeof du);
    contract_AVX256S_v2(gradN,elu,du);
    
    
    // quadrature action
    for (ee=0; ee<NEV_256S; ee++) {
      for (l=0; l<3; l++) { // fields
        for (k=0; k<3; k++) { // directions
          dux[k][l][ee] = du[0][l][single][ee] * dx[k][0][single][ee] + du[1][l][single][ee] * dx[k][1][single][ee] + du[2][l][single][ee] * dx[k][2][single][ee];
        }
      }
    }
    
    for (ee=0; ee<NEV_256S; ee++) {
      long int E = VTMIN(e+ee,nel-1); // Pad up to length NEV by duplicating last
      
      for (d=0; d<NSD_3D; d++) {
        gradu[NSD_3D*E+d] = dux[d][0][ee]; /* du/dx, du/dy, du/dz */
        gradv[NSD_3D*E+d] = dux[d][1][ee];
        gradw[NSD_3D*E+d] = dux[d][2][ee];
      }
    }
    
  }
}

#endif
