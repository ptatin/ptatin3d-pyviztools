
#ifndef __viztools_h__
#define __viztools_h__

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "vt_constants.h"

#define NQP 1

#define VTMIN(a,b) (((a) < (b)) ? (a) : (b))

#define ALIGN32 __attribute__((aligned(32))) /* AVX 256 packed instructions need 32-byte alignment */
#define NEV_256S 8 /* single precision / float */
#define NEV_256D 4 /* double precision / double */

#if defined(__AVX__)

  #include <immintrin.h>

  #ifndef __FMA__
    #define _mm256_fmadd_pd(a,b,c) _mm256_add_pd(_mm256_mul_pd(a,b),c)
    #define _mm256_fmadd_ps(a,b,c) _mm256_add_ps(_mm256_mul_ps(a,b),c)
  #endif

#endif


#endif
