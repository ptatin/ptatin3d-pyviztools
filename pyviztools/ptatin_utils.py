
import numpy as np


def extract_topography(mesh):
  coor, size = mesh.extract_subset_face("jmax")
  
  height = coor[:, 1].flatten()
  tcoor = np.copy(coor)
  tcoor[:, 1] = 0.0
  return tcoor, size, height
