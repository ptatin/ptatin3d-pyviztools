
import os
import ctypes
import numpy as np
from numpy.ctypeslib import ndpointer

def load_clib():

  vt_module_path = os.path.dirname(os.path.abspath(__file__))
  c_library_path = os.path.join(vt_module_path, "c", "lib")
  c_library_name = os.path.join(c_library_path, "viztools.so")

  clibvt = None
  libpath = os.path.dirname(c_library_name)
  libname = os.path.basename(c_library_name)
  #print('[library path]',libpath)
  #print('[library name]',libname)

  # Append current working directry to LD_LIBRARY_PATH
  try:
    #print('[ld_library_path][orig]',os.environ["LD_LIBRARY_PATH"])
    os.environ["LD_LIBRARY_PATH"] += os.pathsep + libpath
    #print('[ld_library_path][mod]',os.environ["LD_LIBRARY_PATH"])
  except:
    os.environ["LD_LIBRARY_PATH"] = libpath
    #print('[ld_library_path][new]',os.environ["LD_LIBRARY_PATH"])

  try:
    clibvt = ctypes.cdll.LoadLibrary(os.path.join(libpath, libname))
    print('[load_clib]','Successfully loaded shared object:', libname)
  except:
    print('[load_clib]','Failed to load shared object:', libname)

  if clibvt is None:
    clibvt_ftable = None
    return clibvt, clibvt_ftable


  clibvt_ftable = dict()

  clibvt_ftable["femesh"] = femesh_load_methods(clibvt)

  clibvt_ftable["utils"] = utils_load_methods(clibvt)

  return clibvt, clibvt_ftable




def femesh_load_methods(clib):

  function = dict()

  # ===========================================================================================
  c_method = clib.vt_e2v_q1_3d
  c_method.restype = None
  c_method.argtypes = [
                        ndpointer(ctypes.c_long,  flags="C_CONTIGUOUS"), # m[]
                        ndpointer(ctypes.c_long,  flags="C_CONTIGUOUS"), # e2v[]
                      ]
  function["e2v_q1_3d"] = c_method


  # ===========================================================================================
  c_method = clib.vt_interp_to_cell
  c_method.restype = None
  c_method.argtypes = [
                         ctypes.c_long, # ne
                         ctypes.c_long, # bs
                         ndpointer(ctypes.c_long,  flags="C_CONTIGUOUS"), # elements[]
                         ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # field[]
                         ndpointer(ctypes.c_float, flags="C_CONTIGUOUS")  # field_cell[]
                       ]
  function["interp_to_cell"] = c_method

  # ===========================================================================================
  c_method = clib.vt_compute_derivatives_scalar
  c_method.restype = None
  c_method.argtypes = [
                        ctypes.c_long, # nv
                        ctypes.c_long, # ne
                        ndpointer(ctypes.c_long,  flags="C_CONTIGUOUS"), # elements[]
                        ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # coor[]
                        ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # field[]
                        ndpointer(ctypes.c_float, flags="C_CONTIGUOUS")  # grad[]
                      ]
  function["compute_derivatives_scalar"] = c_method

  # ===========================================================================================
  #c_method = clib.vt_compute_derivatives_vector
  c_method = clib.vt_compute_derivatives_vector_tensor
  c_method.restype = None
  c_method.argtypes = [
                        ctypes.c_long, # nv
                        ctypes.c_long, # ne
                        ndpointer(ctypes.c_long,  flags="C_CONTIGUOUS"), # elements[]
                        ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # coor[]
                        ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # field[]
                        ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # gradu[]
                        ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # gradv[]
                        ndpointer(ctypes.c_float, flags="C_CONTIGUOUS")  # gradw[]
                       ]
  function["compute_derivatives_vector"] = c_method

  # ===========================================================================================
  try:
    c_method = clib.vt_compute_derivatives_vector_AVX256
    print("+ found AVX256 implmentation for compute_derivatives_vector()")
    c_method.restype = None
    c_method.argtypes = [
                          ctypes.c_long, # nv
                          ctypes.c_long, # ne
                          ndpointer(ctypes.c_long,  flags="C_CONTIGUOUS"), # elements[]
                          ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # coor[]
                          ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # field[]
                          ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # gradu[]
                          ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # gradv[]
                          ndpointer(ctypes.c_float, flags="C_CONTIGUOUS")  # gradw[]
                        ]
    function["compute_derivatives_vector_avx256"] = c_method
  except:
    pass

  return function


def utils_load_methods(clib):

  function = dict()
  # ===========================================================================================
  c_method = clib.vt_gauss_smooth
  c_method.restype = None
  c_method.argtypes = [
                       ndpointer(ctypes.c_long,  flags="C_CONTIGUOUS"), # msize[]
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # ds[]
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # field[]
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS")  # field_gs[]
                       ]
  function["gauss_smooth"] = c_method

  # ===========================================================================================
  c_method = clib.vt_compute_rsr
  c_method.restype = None
  c_method.argtypes = [
                       ctypes.c_long,                                    # n_points
                       ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"), # E[]
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS")   # RSR[]
                       ]
  function["compute_rsr"] = c_method

  # ===========================================================================================
  c_method = clib.vt_set_uniform_coordinates_2
  c_method.restype = None
  c_method.argtypes = [
                       ndpointer(ctypes.c_long,  flags="C_CONTIGUOUS"), # dir[]
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # start[]
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # end[]
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS")  # coor[]
                       ]
  function["set_uniform_coordinates_2"] = c_method

  c_method = clib.vt_set_uniform_coordinates_3
  c_method.restype = None
  c_method.argtypes = [
                      ndpointer(ctypes.c_long,  flags="C_CONTIGUOUS"), # dir[]
                      ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # start[]
                      ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # end[]
                      ndpointer(ctypes.c_float, flags="C_CONTIGUOUS")  # coor[]
                      ]
  function["set_uniform_coordinates_3"] = c_method

  # ===========================================================================================
  c_method = clib.vt_structured_mesh_point_location_2
  c_method.restype = None
  c_method.argtypes = [
                       ndpointer(ctypes.c_long,  flags="C_CONTIGUOUS"), # msize[]
                       ndpointer(ctypes.c_long, flags="C_CONTIGUOUS"),  # elements[]
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # coor[]
                       ctypes.c_long,                                   # n_points
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # coor_p[]
                       ndpointer(ctypes.c_long, flags="C_CONTIGUOUS"),  # el_p[]
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS")  # xi_p[]
                       ]
  function["structured_quad_mesh_point_location"] = c_method

  # ===========================================================================================
  c_method = clib.vt_structured_mesh_point_location_3
  c_method.restype = None
  c_method.argtypes = [
                       ndpointer(ctypes.c_long,  flags="C_CONTIGUOUS"), # msize[]
                       ndpointer(ctypes.c_long, flags="C_CONTIGUOUS"),  # elements[]
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # coor[]
                       ctypes.c_long,                                   # n_points
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # coor_p[]
                       ndpointer(ctypes.c_long, flags="C_CONTIGUOUS"),  # el_p[]
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS")  # xi_p[]
                       ]
  function["structured_hex_mesh_point_location"] = c_method

  # ===========================================================================================
  c_method = clib.vt_structured_mesh_interpolate_3
  c_method.restype = None
  c_method.argtypes = [
                       ctypes.c_long,                                   # n_elements
                       ndpointer(ctypes.c_long, flags="C_CONTIGUOUS"),  # elements[]
                       ctypes.c_long,                                   # block size
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # field[]
                       ctypes.c_long,                                   # n_points
                       ndpointer(ctypes.c_long, flags="C_CONTIGUOUS"),  # el_p[]
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # xi_p[]
                       ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"), # field_p[]
                       ]
  function["structured_hex_mesh_interpolate"] = c_method


  return function
