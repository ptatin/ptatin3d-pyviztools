
import numpy as np
from time import perf_counter
import inspect

from .utils import *
from . import femesh


class FEMeshQ1Vec(femesh.FEMeshQ1):
  def __init__(self, d, m, n, p):
    femesh.FEMeshQ1.__init__(self, d, m, n, p)
    self.nvec = 1000

  #def is_vertex_field(self, field):
  #def is_cell_field(self, field):
  #def create_coor(self):
  #def create_e2v(self):
  #def create_e2v_fast(self): --> create_e2v
  
  def __create_e2v_fast(self):
    mt0 = perf_counter()
    ni, nj, nk = self.m+1, self.n+1, self.p+1
    n0 = np.linspace(0, ni*nj*nk-1, ni*nj*nk, dtype=np.int64)
    #print(n0,ni*nj*nk,len(n0))
    #_n0 = np.reshape(n0, newshape=(ni, nj, nk))
    _n0 = np.reshape(n0, newshape=(nk, nj, ni))
    #print(_n0.shape)
    #print(_n0[0,0,0],_n0[0,0,2])

    #n0 = _n0[0:(ni-1), 0:(nj-1), 0:(nk-1)]
    n0 = _n0[0:(nk-1), 0:(nj-1), 0:(ni-1)]
    #print(n0.shape)
    #print(n0,self.ne,n0.shape)
    
    element = np.zeros(shape=(self.basis_per_el, self.ne), dtype=np.int64)
    
    n0 = n0.flatten()
    element[0, :] = n0[:]
    element[1, :] = n0[:] + 1
    element[2, :] = n0[:] + ni
    element[3, :] = n0[:] + ni + 1
    
    element[4, :] = element[0, :] + ni * nj
    element[5, :] = element[1, :] + ni * nj
    element[6, :] = element[2, :] + ni * nj
    element[7, :] = element[3, :] + ni * nj

    #self.element = element.T
    self.element = np.ascontiguousarray(element.T)

    mt1 = perf_counter()
    dt = mt1 - mt0
    print('->',inspect.currentframe().f_code.co_name + '()','execution time:',('%1.4e'%dt),'(sec)')
  
  def create_e2v(self):
    return FEMeshQ1Vec.__create_e2v_fast(self)


  #def get_elements(self):
  #def get_cell_volume(self, nvec=10000):
  #def eval_basis(self, xi):
  #def eval_basis_gradient_0(self):
  #def create_vertex_field(self, dof=1):
  #def create_cell_field(self, dof=1):

  #def interp2cell_naive(self, field):
  #def interpgrad2cell_naive(self, field):

  # Supports Vec with any blocksize
  def __interp2cell_vec(self, field, nvec=1000):

    mt0 = perf_counter()
    if not self.is_vertex_field(field):
      raise RuntimeError('Only valid for vertex field')

    bs = vec_get_blocksize(field)
    #print('field: dof',bs)
    if bs > 1:
      phi_cell = np.zeros(shape=(self.ne, bs), dtype=np.float32)
    else:
      phi_cell = np.zeros(self.ne, dtype=np.float32)

    ecnt = 0
    while ecnt != self.ne:
      start = ecnt
      end = ecnt + nvec
      if end > self.ne:
        end = self.ne

      e_block = np.linspace(start, end-1, (end-start), dtype=np.int32)

      el_block = self.element[e_block]
      
      phi_e_block = field[el_block]
      #print(phi_e_block.shape) # nvec x npe x dof

      phi_cell_block = np.average(phi_e_block, axis=1)
      phi_cell[e_block] = phi_cell_block[:]
      
      ecnt += (end - start)
    mt1 = perf_counter()
    dt = mt1 - mt0
    print('->',inspect.currentframe().f_code.co_name + '()','execution time:',('%1.4e'%dt),'(sec)')
    return phi_cell


  def interp2cell(self, field):
    return FEMeshQ1Vec.__interp2cell_vec(self, field, self.nvec)


  def __interpgrad2cell_vec(self, field, nvec=1000):
    
    if not self.is_vertex_field(field):
      raise RuntimeError('Only valid for vertex field')

    dof = len(field.shape)
    #print('field: dof',dof)
    if dof != 1:
      print('Only valid for scalar field. Use .compute_derivatives() instead')
      return None
    
    grad_phi_cell = np.zeros(shape=(self.dim, self.ne), dtype=np.float32)
    
    gn = self.eval_basis_gradient_0()
    

    ecnt = 0
    while ecnt != self.ne:
      start = ecnt
      end = ecnt + nvec
      if end > self.ne:
        end = self.ne
    
      e_block = np.linspace(start, end-1, (end-start), dtype=np.int32)
      
      el_block = self.element[e_block]

      x_el_block = self.coor[el_block,0]
      y_el_block = self.coor[el_block,1]
      z_el_block = self.coor[el_block,2]
      f_el_block = field[el_block]
      #print('f_el_block',f_el_block.shape)

      #dphi_xi = np.matmul(f_el_block, gn[0])
      #dphi_eta = np.matmul(f_el_block, gn[1])
      #dphi_zeta = np.matmul(f_el_block, gn[2])

      dphi_xi = np.matmul(f_el_block, gn.T)
      #print('dphi_xi',dphi_xi.shape)

      #for s in range((end - start)):
      #  J[:,0] = np.matmul(gn, x_el[s])
      #  J[:,1] = np.matmul(gn, y_el[s])
      #  J[:,2] = np.matmul(gn, z_el[s])

      J0 = np.matmul(x_el_block, gn.T)
      J1 = np.matmul(y_el_block, gn.T)
      J2 = np.matmul(z_el_block, gn.T)

      J = np.zeros(shape=(self.dim,self.dim))
      for s in range((end - start)):
        J[:,0] = J0[s,:]
        J[:,1] = J1[s,:]
        J[:,2] = J2[s,:]
        #print(ecnt+s,'J',J)

        dphi_x = np.linalg.solve(J, dphi_xi[s,:])
        #print(ecnt+s,dphi_x)
        grad_phi_cell[:,s+start] = dphi_x[:]

      ecnt += (end - start)
    return grad_phi_cell.T


  def __interpgrad2cell_vec2(self, field, nvec=1000):
    
    mt0 = perf_counter()
    if not self.is_vertex_field(field):
      raise RuntimeError('Only valid for vertex field')
    
    dof = vec_get_blocksize(field)
    #print('field: dof',dof)
    if dof != 1:
      print('Only valid for scalar field. Use .compute_derivatives() instead')
      return None
  
    grad_phi_cell = np.zeros(shape=(self.dim, self.ne), dtype=np.float32)
    
    gn = self.eval_basis_gradient_0()
    
    ecnt = 0
    while ecnt != self.ne:
      start = ecnt
      end = ecnt + nvec
      if end > self.ne:
        end = self.ne
    
      bs = end - start
      #print('bs',bs)

      e_block = np.linspace(start, end-1, bs, dtype=np.int32)
      
      el_block = self.element[e_block]
      
      x_el_block = self.coor[el_block,0]
      y_el_block = self.coor[el_block,1]
      z_el_block = self.coor[el_block,2]
      f_el_block = field[el_block]
      
      dphi_xi = np.matmul(f_el_block, gn.T)
      
      J0 = np.matmul(x_el_block, gn.T)
      J1 = np.matmul(y_el_block, gn.T)
      J2 = np.matmul(z_el_block, gn.T)

      #J = np.zeros(shape=(bs, self.dim, self.dim))
      #for s in range(bs):
      #  J[s,:,0] = J0[s,:]
      #  J[s,:,1] = J1[s,:]
      #  J[s,:,2] = J2[s,:]

      #JJ = np.zeros(shape=(bs * self.dim, self.dim))
      #JJ[0:bs*3:3, :] = J0
      #JJ[1:bs*3:3, :] = J1
      #JJ[2:bs*3:3, :] = J2
      #J = np.reshape(JJ, newshape=(bs, self.dim, self.dim))
      """
        cell 63 [[ 0.16394137 -0.03419763  0.00555047]
        [-0.00451333  0.16666667 -0.05863373]
        [ 0.00233099  0.          0.16666667]]
      """

      JJ = np.zeros(shape=(self.dim, bs * self.dim))
      JJ[:, 0:bs*3:3] = J0.T
      JJ[:, 1:bs*3:3] = J1.T
      JJ[:, 2:bs*3:3] = J2.T
      #print('jj',JJ)
      J = blockshaped(JJ, 3, 3)
      #print('j ecnt',ecnt, J)

      #for s in range(bs):
      #  print('cell',ecnt+s,'J',J[s])

      iJ = stacked_inverse_3x3(J)
      
      # C_a,i = A_a,i,j * B a,j
      
      #for s in range(bs):
      #  dphi_x = np.matmul(iJ[s], dphi_xi[s,:])
      #  print(ecnt+s,'dphi_x',dphi_x)
      #  #grad_phi_cell[:,s+start] = dphi_x[:]
      
      # Compute C_ik = Ai(jk) * Ci(k)
      # C[s]i = A[s]ij * B[s]j => C[i]_j = A[i]_jk * C[i]_k
      # note the result is transposed (-> ki) to give use dim x evec
      #C = np.einsum('ijk,ik->ki', iJ, dphi_xi)
      C = np.einsum('ijk, ik -> ji', iJ, dphi_xi)
      #print('C.shape',C.shape)
      #print(ecnt,C)
      
      grad_phi_cell[:,e_block] = C
      
      ecnt += bs
    
    mt1 = perf_counter()
    dt = mt1 - mt0
    print('->',inspect.currentframe().f_code.co_name + '()','execution time:',('%1.4e'%dt),'(sec)')
    return grad_phi_cell.T


  def interpgrad2cell(self, field):
    #return FEMeshQ1Vec.__interpgrad2cell_vec(self, field, self.nvec)
    return FEMeshQ1Vec.__interpgrad2cell_vec2(self, field, self.nvec)


  # Supports Vec with any blocksize
  def compute_derivatives(self, field, **kwargs):
    """
    Equivalent to ParaView filter "Compute Derivatives".
    """
    
    nvec = self.nvec
    keys = list(kwargs.keys())
    if "nvec" in keys:
      nvec = kwargs["nvec"]
    
    return femesh.FEMeshQ1._FEMeshQ1__compute_derivatives(self, field, nvec)


  #def interp_coarse_to_fine(self, field_c, to_mesh):
  #def extract_subset_face(self, faceid):
  #def extract_subset(self, _irange, _jrange, _krange, sample_rate=(1, 1, 1), cellData=None, pointData=None):
  #def cell_field_to_point_field(self, field_c, volume_scale=False, **kwargs):


