
import numpy as np
from time import perf_counter
import inspect

from .utils import *


class FEMeshQ1:
  def __init__(self, d, m, n, p):
    self.dim = d
    self.m = m # nelements, i-direction
    self.n = n
    self.p = p
    self.nv = (m+1) * (n+1) * (p+1)
    self.ne = m * n * p

    map_npe = { 1: 2, 2: 4, 3: 8 } # dim -> nodes-per-element
    self.basis_per_el = map_npe[self.dim]
    
    self.coor = np.zeros(shape=(1,self.dim), dtype=np.float32)
    self.element = None


  def is_vertex_field(self, field):
    L = field.shape[0]
    if L == (self.m+1) * (self.n+1) * (self.p+1):
      return True
    else:
      return False


  def is_cell_field(self, field):
    L = field.shape[0]
    if L == self.m * self.n * self.p:
      return True
    else:
      return False


  def create_coor(self):
    self.coor = np.zeros(shape=(self.nv,self.dim), dtype=np.float32)


  def create_e2v(self):
    self.element = np.zeros(shape=(self.ne,self.basis_per_el), dtype=np.int64)
    e = 0
    ni = self.m + 1
    nj = self.n + 1
    nk = self.p + 1
    for k in range(self.p):
      for j in range(self.n):
        for i in range(self.m):

          n0 = i + j * ni + k * ni * nj
          n1 = i + j * ni + (k+1) * ni * nj

          self.element[e][:] = np.asarray([n0, n0+1, n0+ni, n0+ni+1, n1, n1+1, n1+ni, n1+ni+1])

          e += 1


  def get_elements(self):
    if self.element is None:
      self.create_e2v()
    return self.element


  def get_cell_volume(self, nvec=10000):
    cell_volume = self.create_cell_field(dof=1)
  
    gn = self.eval_basis_gradient_0()
    
    ecnt = 0
    while ecnt != self.ne:
      start = ecnt
      end = ecnt + nvec
      if end > self.ne:
        end = self.ne
    
      bs = end - start
      
      e_block = np.linspace(start, end-1, bs, dtype=np.int32)
      
      el_block = self.element[e_block]
      
      x_el_block = self.coor[el_block,0]
      y_el_block = self.coor[el_block,1]
      z_el_block = self.coor[el_block,2]
      
      J0 = np.matmul(x_el_block, gn.T)
      J1 = np.matmul(y_el_block, gn.T)
      J2 = np.matmul(z_el_block, gn.T)
      
      JJ = np.zeros(shape=(self.dim, bs * self.dim))
      JJ[:, 0:bs*3:3] = J0.T
      JJ[:, 1:bs*3:3] = J1.T
      JJ[:, 2:bs*3:3] = J2.T
      J = blockshaped(JJ, 3, 3)
      
      dJ = stacked_detJ_3x3(J)

      cell_volume[e_block] = 8.0 * dJ

      ecnt += bs

    v = np.sum(self.cell_volume)
    print('mesh(vol)',v)
    return cell_volume


  def eval_basis(self, xi):
    npoints = len(xi)
    Ni = np.zeros(shape=(npoints, self.basis_per_el))
    for p in range(npoints):
      _Ni = Ni[p][:]
      _xi = xi[p][:]
      _Ni[0] = 0.125 * (1.0 - _xi[0]) * (1.0 - _xi[1]) * (1.0 - _xi[2])
      _Ni[1] = 0.125 * (1.0 + _xi[0]) * (1.0 - _xi[1]) * (1.0 - _xi[2])
      _Ni[2] = 0.125 * (1.0 - _xi[0]) * (1.0 + _xi[1]) * (1.0 - _xi[2])
      _Ni[3] = 0.125 * (1.0 + _xi[0]) * (1.0 + _xi[1]) * (1.0 - _xi[2])
      
      _Ni[4] = 0.125 * (1.0 - _xi[0]) * (1.0 - _xi[1]) * (1.0 + _xi[2])
      _Ni[5] = 0.125 * (1.0 + _xi[0]) * (1.0 - _xi[1]) * (1.0 + _xi[2])
      _Ni[6] = 0.125 * (1.0 - _xi[0]) * (1.0 + _xi[1]) * (1.0 + _xi[2])
      _Ni[7] = 0.125 * (1.0 + _xi[0]) * (1.0 + _xi[1]) * (1.0 + _xi[2])
      Ni[p][:] = _Ni[:]
    return Ni


  def eval_basis_gradient_0(self):
    _xi = np.zeros(3)
    DNi = np.zeros(shape=(3, self.basis_per_el))
    
    DNi[0][0] = 0.125 * (0.0 - 1) * (1.0 - _xi[1]) * (1.0 - _xi[2])
    DNi[0][1] = 0.125 * (0.0 + 1) * (1.0 - _xi[1]) * (1.0 - _xi[2])
    DNi[0][2] = 0.125 * (0.0 - 1) * (1.0 + _xi[1]) * (1.0 - _xi[2])
    DNi[0][3] = 0.125 * (0.0 + 1) * (1.0 + _xi[1]) * (1.0 - _xi[2])
    DNi[0][4] = 0.125 * (0.0 - 1) * (1.0 - _xi[1]) * (1.0 + _xi[2])
    DNi[0][5] = 0.125 * (0.0 + 1) * (1.0 - _xi[1]) * (1.0 + _xi[2])
    DNi[0][6] = 0.125 * (0.0 - 1) * (1.0 + _xi[1]) * (1.0 + _xi[2])
    DNi[0][7] = 0.125 * (0.0 + 1) * (1.0 + _xi[1]) * (1.0 + _xi[2])
    
  
    DNi[1][0] = 0.125 * (1.0 - _xi[0]) * (0.0 - 1) * (1.0 - _xi[2])
    DNi[1][1] = 0.125 * (1.0 + _xi[0]) * (0.0 - 1) * (1.0 - _xi[2])
    DNi[1][2] = 0.125 * (1.0 - _xi[0]) * (0.0 + 1) * (1.0 - _xi[2])
    DNi[1][3] = 0.125 * (1.0 + _xi[0]) * (0.0 + 1) * (1.0 - _xi[2])
    DNi[1][4] = 0.125 * (1.0 - _xi[0]) * (0.0 - 1) * (1.0 + _xi[2])
    DNi[1][5] = 0.125 * (1.0 + _xi[0]) * (0.0 - 1) * (1.0 + _xi[2])
    DNi[1][6] = 0.125 * (1.0 - _xi[0]) * (0.0 + 1) * (1.0 + _xi[2])
    DNi[1][7] = 0.125 * (1.0 + _xi[0]) * (0.0 + 1) * (1.0 + _xi[2])


    DNi[2][0] = 0.125 * (1.0 - _xi[0]) * (1.0 - _xi[1]) * (0.0 - 1)
    DNi[2][1] = 0.125 * (1.0 + _xi[0]) * (1.0 - _xi[1]) * (0.0 - 1)
    DNi[2][2] = 0.125 * (1.0 - _xi[0]) * (1.0 + _xi[1]) * (0.0 - 1)
    DNi[2][3] = 0.125 * (1.0 + _xi[0]) * (1.0 + _xi[1]) * (0.0 - 1)
    DNi[2][4] = 0.125 * (1.0 - _xi[0]) * (1.0 - _xi[1]) * (0.0 + 1)
    DNi[2][5] = 0.125 * (1.0 + _xi[0]) * (1.0 - _xi[1]) * (0.0 + 1)
    DNi[2][6] = 0.125 * (1.0 - _xi[0]) * (1.0 + _xi[1]) * (0.0 + 1)
    DNi[2][7] = 0.125 * (1.0 + _xi[0]) * (1.0 + _xi[1]) * (0.0 + 1)

    return DNi


  def create_vertex_field(self, dof=1):
    if dof == 1:
      f = np.zeros(self.nv, dtype=np.float32)
    else:
      f = np.zeros(shape=(self.nv, dof), dtype=np.float32)
    return f


  def create_cell_field(self, dof=1):
    if dof == 1:
      f = np.zeros(self.ne, dtype=np.float32)
    else:
      f = np.zeros(shape=(self.ne, dof), dtype=np.float32)
    return f


  def __interp2cell_naive(self, field):
    
    if not self.is_vertex_field(field):
      raise RuntimeError('Only valid for vertex field')
    
    phi_cell = self.create_cell_field(dof=vec_get_blocksize(field))
    for e in range(self.ne):
      el = self.element[e]
      phi_e = field[el]
      phi_cell[e] = np.average(phi_e)
    return phi_cell


  def interp2cell(self, field):
    return FEMeshQ1.__interp2cell_naive(self, field)


  def __interpgrad2cell_naive(self, field):
    
    if not self.is_vertex_field(field):
      raise RuntimeError('Only valid for vertex field')
    
    dof = len(field.shape)
    #print('field: dof',dof)
    if dof != 1:
      print('Only valid for scalar field. Use .compute_derivatives() instead')
      return None

    #grad_phi_cell = np.zeros(shape=(self.ne, self.dim), dtype=np.float32)
    grad_phi_cell = np.zeros(shape=(self.dim, self.ne), dtype=np.float32)
    
    gn = self.eval_basis_gradient_0()

    for e in range(self.ne):
      el = self.element[e]

      x_el = self.coor[el,0]
      y_el = self.coor[el,1]
      z_el = self.coor[el,2]
      f_el = field[el]

      #dphi_xi = np.zeros(self.dim)
      #dphi_xi[0] = np.dot(gn[0], f_el)
      #dphi_xi[1] = np.dot(gn[1], f_el)
      #dphi_xi[2] = np.dot(gn[2], f_el)
      dphi_xi = np.matmul(gn, f_el)

      """
      J = np.zeros(shape=(self.dim,self.dim))
      J[0][0] =  np.dot(gn[0], x_el)
      J[0][1] =  np.dot(gn[0], y_el)
      J[0][2] =  np.dot(gn[0], z_el)

      J[1][0] =  np.dot(gn[1], x_el)
      J[1][1] =  np.dot(gn[1], y_el)
      J[1][2] =  np.dot(gn[1], z_el)

      J[2][0] =  np.dot(gn[2], x_el)
      J[2][1] =  np.dot(gn[2], y_el)
      J[2][2] =  np.dot(gn[2], z_el)
      """
      J = np.zeros(shape=(self.dim,self.dim))
      J[:,0] = np.matmul(gn, x_el)
      J[:,1] = np.matmul(gn, y_el)
      J[:,2] = np.matmul(gn, z_el)
      #print('cell',e,J)

      dphi_x = np.linalg.solve(J, dphi_xi)
      
      #grad_phi_cell[e,:] = dphi_x[:]
      grad_phi_cell[:,e] = dphi_x[:]

    return grad_phi_cell.T


  def interpgrad2cell(self, field):
    return FEMeshQ1.__interpgrad2cell_naive(self, field)


  def __compute_derivatives(self, field, nvec=1000):
    
    mt0 = perf_counter()
    if not self.is_vertex_field(field):
      raise RuntimeError('Only valid for vertex field')
    
    dof = vec_get_blocksize(field)
    #print('field: dof',dof)
  
    if dof == 1:
      field = np.atleast_2d(field).T
  
    grad = list()
    for b in range(dof):
      grad_phi_cell = np.zeros(shape=(self.dim, self.ne), dtype=np.float32)
      grad.append(grad_phi_cell)
    
    gn = self.eval_basis_gradient_0()
    
    ecnt = 0
    while ecnt != self.ne:
      start = ecnt
      end = ecnt + nvec
      if end > self.ne:
        end = self.ne
    
      bs = end - start
      
      e_block = np.linspace(start, end-1, bs, dtype=np.int32)
      
      el_block = self.element[e_block]
      
      x_el_block = self.coor[el_block,0]
      y_el_block = self.coor[el_block,1]
      z_el_block = self.coor[el_block,2]
      
      J0 = np.matmul(x_el_block, gn.T)
      J1 = np.matmul(y_el_block, gn.T)
      J2 = np.matmul(z_el_block, gn.T)
      
      
      JJ = np.zeros(shape=(self.dim, bs * self.dim))
      JJ[:, 0:bs*3:3] = J0.T
      JJ[:, 1:bs*3:3] = J1.T
      JJ[:, 2:bs*3:3] = J2.T
      J = blockshaped(JJ, 3, 3)
      
      iJ = stacked_inverse_3x3(J)
      
      for b in range(dof):
        f_el_block = field[el_block, b]
        
        dphi_xi = np.matmul(f_el_block, gn.T)
        
        # C_a,i = A_a,i,j * B a,j
        # Compute C_ik = Ai(jk) * Ci(k)
        # C[s]i = A[s]ij * B[s]j => C[i]_j = A[i]_jk * C[i]_k
        # note the result is transposed (-> ki) to give use dim x evec
        C = np.einsum('ijk, ik -> ji', iJ, dphi_xi)
        
        stuff = grad[b]
        stuff[:,e_block] = C
      
      ecnt += bs
    
    for b in range(dof):
      stuff = grad[b]
      grad[b] = stuff.T

    mt1 = perf_counter()
    dt = mt1 - mt0
    print('->',inspect.currentframe().f_code.co_name + '()','execution time:',('%1.4e'%dt),'(sec)')
    return grad


  # Supports Vec with any blocksize
  def compute_derivatives(self, field, **kwargs):
    """
    Equivalent to ParaView filter "Compute Derivatives".
    """

    nvec = 1000 # default
    keys = list(kwargs.keys())
    if "nvec" in keys:
      nvec = kwargs["nvec"]
    
    return FEMeshQ1.__compute_derivatives(self, field, nvec)


  # Loop over coarse mesh elements, insert into fine mesh vector slots
  def interp_coarse_to_fine(self, field_c, to_mesh):
    
    mt0 = perf_counter()
    if not self.is_vertex_field(field_c):
      raise RuntimeError('Only valid for vertex field')

    if vec_get_blocksize(field_c) != 1:
      raise RuntimeError('Only valid for blocksize = 1')

    def avg2d(f_c, f_f):
      nj, ni = f_c.shape
      nj_f, ni_f = f_f.shape
      #print('nj,ni',nj,ni)
      #print('nj_f,ni_f',nj_f,ni_f)
      
      ii = np.arange(ni, dtype=np.int32)
      jj = np.arange(nj, dtype=np.int32)
      iis = 2*ii[0:ni-1] + 1
      jjs = 2*jj[0:nj-1] + 1

      #print('ii',ii)
      #print('iis',iis)

      for i in range(ni-1):
        v0 = 0.5 * (f_c[jj,i] + f_c[jj,i+1])
        #print('v0',v0.shape)
        #print('jj',jj.shape)
        #print('i-sweep','avg i = ',i,i+1,'using jj = ',jj)
        f_f[2*jj, 2*i+1] = v0
        #print('i-sweep','insert J = ',2*jj,'I = ',2*i+1)
      """
      for j in range(nj-1):
        v0 = 0.5 * (f_f[2*j, iis] + f_f[2*(j+1), iis])
        #print('central j-sweep','avg j = ',2*j,2*(j+1),'using iis = ',iis)
        f_f[2*j+1, iis] = v0
        #print('central j-sweep','insert J = ',2*jj,'I = ',2*i+1)

      for j in range(nj-1):
        v0 = 0.5 * (f_c[j, ii] + f_c[j+1, ii])
        #print('j-sweep','avg j = ',j,j+1,'using ii = ',ii)
        f_f[2*j+1, 2*ii] = v0
        #print('j-sweep','insert J = ',2*j+1,'I = ',2*ii)
      """

      for j in range(nj-1):
        v0 = 0.5 * (f_f[2*j, iis] + f_f[2*(j+1), iis])
        f_f[2*j+1, iis] = v0

        v0 = 0.5 * (f_c[j, ii] + f_c[j+1, ii])
        f_f[2*j+1, 2*ii] = v0

    if 2*self.m != to_mesh.m:
      raise RuntimeError('[Fine mesh does not have mx = 2*mx_coarse')
    if 2*self.n != to_mesh.n:
      raise RuntimeError('[Fine mesh does not have my = 2*my_coarse')
    if 2*self.p != to_mesh.p:
      raise RuntimeError('[Fine mesh does not have mz = 2*mz_coarse')

    ni, nj, nk = self.m+1, self.n+1, self.p+1
    ni_f, nj_f, nk_f = to_mesh.m+1, to_mesh.n+1, to_mesh.p+1

    field_c_ijk = np.reshape(field_c, newshape=(nk, nj, ni))

    field_f = to_mesh.create_vertex_field(dof=1)
    field_f_ijk = np.reshape(field_f, newshape=(nk_f, nj_f, ni_f))

    # cell center
    #field_f_center = field_c[self.element[:,0]]
    #for i in range(1,self.basis_per_el):
    #  field_f_center += field_c[self.element[:,i]]
    #field_f_center[:] = (1.0/self.basis_per_el) * field_f_center[:]
    # insert

    #ii = np.linspace(0,ni-1,ni,dtype=np.int32)
    #jj = np.linspace(0,nj-1,nj,dtype=np.int32)
    #kk = np.linspace(0,nk-1,nk,dtype=np.int32)
    #print(ii,2*ii,ni,ni_f)

    # vertices
    #x = field_c_ijk[kk][jj][ii]
    #field_f_ijk[kk][jj][ii] = field_c


    vertid = np.linspace(0, self.nv-1, self.nv, dtype=np.int64)
    kk = vertid / (ni*nj)
    kk = kk.astype(np.int64)
    _ij = vertid - kk * (ni * nj)
    jj = _ij / ni
    jj = jj.astype(np.int64)
    ii = _ij - jj * ni
    del _ij
    
    # push vertices from coarse to fine
    vertid_fine = (2*ii) + (2*jj) * (to_mesh.m+1) + (2*kk) * (to_mesh.m+1) * (to_mesh.n+1)
    field_f[vertid_fine] = field_c[:]
    
    # --- push the interiors ------------------------------
    """
    cellid = np.linspace(0, self.ne-1, self.ne, dtype=np.int32)
    cellkk = cellid / (self.m * self.n)
    cellkk = cellkk.astype(np.int32)
    _cellij = cellid - cellkk * (self.m * self.n)
    celljj = _cellij / self.m
    celljj = celljj.astype(np.int32)
    cellii = _cellij - celljj * self.m
    del _cellij
    
    field_c_cell = self.create_cell_field(dof=1)
    for e in range(self.ne):
      el = self.element[e]
      
      f_el = field_c[el]
      field_c_cell[e] = np.average(f_el)
    
    cells = (2*cellii) + (2*celljj) * (to_mesh.m) + (2*cellkk) * (to_mesh.m) * (to_mesh.n)
    
    vertid_fine = to_mesh.element[cells, 7]
    field_f[vertid_fine] = field_c_cell[:]
    """
    # --- centroid insert ------------------------------
    cellid = np.linspace(0, self.ne-1, self.ne, dtype=np.int64)
    cellkk = cellid / (self.m * self.n)
    cellkk = cellkk.astype(np.int64)
    _cellij = cellid - cellkk * (self.m * self.n)
    celljj = _cellij / self.m
    celljj = celljj.astype(np.int64)
    cellii = _cellij - celljj * self.m
    del _cellij
    
    field_c_cell = self.create_cell_field(dof=1)
    for i in range(self.basis_per_el):
      field_c_cell += field_c[self.element[:,i]]
    field_c_cell[:] = field_c_cell[:] / float(self.basis_per_el)
    
    cells = (2*cellii) + (2*celljj) * (to_mesh.m) + (2*cellkk) * (to_mesh.m) * (to_mesh.n)
    
    vertid_fine = to_mesh.element[cells, 7]
    field_f[vertid_fine] = field_c_cell[:]
    # ------------------------------------------------------

    field_f_ijk = np.reshape(field_f, newshape=(nk_f, nj_f, ni_f))


    #print(2*(nk-1),nk_f-1)
    #avg2d( field_c_ijk[nk-1,:,:], field_f_ijk[2*(nk-1),:,:])
    #print(field_f_ijk[2*(nk-1),:,:])
    
    
    for k in range(nk):
      avg2d( field_c_ijk[k,:,:], field_f_ijk[2*k,:,:])
    
    for j in range(nj):
      avg2d( field_c_ijk[:,j,:], field_f_ijk[:,2*j,:])

    for i in range(ni):
      avg2d( field_c_ijk[:,:,i], field_f_ijk[:,:,2*i])
    
    mt1 = perf_counter()
    dt = mt1 - mt0
    print('->',inspect.currentframe().f_code.co_name + '()','execution time:',('%1.4e'%dt),'(sec)')
    return field_f_ijk.flatten()



  def extract_subset_face(self, faceid):
    """
    Extract an entire face (boundary) from the domain.
    
    Input
      faceid: Identifier for faces of a hex domain. 
              Valid values are `faceid` = ("imin", "imax", "jmin", "jmax", "kmin", "kmax").
            
    Output
      t_coor         : Coordinates of the extract mesh.
      (_ni, _nj, _nk): Number of vertices in each i, j, k direciton.
    """
    
    ni, nj, nk = self.m+1, self.n+1, self.p+1
    
    ii = np.arange(ni, dtype=np.int32)
    jj = np.arange(nj, dtype=np.int32)
    kk = np.arange(nk, dtype=np.int32)
    
    face_map =  {
                  "imin": (np.asarray([0]), jj, kk),
                  "imax": (np.asarray([ni-1]), jj, kk),
                  "jmin": (ii, np.asarray([0]), kk),
                  "jmax": (ii, np.asarray([nj-1]), kk),
                  "kmin": (ii, jj, np.asarray([0])),
                  "kmax": (ii, jj, np.asarray([nk-1])),
                }
    dir_map = { "imin": "i", "imax": "i",
                "jmin": "j", "jmax": "j",
                "kmin": "k", "kmax": "k"  }


    _ii = face_map[faceid][0]
    _jj = face_map[faceid][1]
    _kk = face_map[faceid][2]

    _ni, _nj, _nk = len(_ii), len(_jj), len(_kk)

    #print(_ii, _jj, _kk)
    #print(_ii.shape, _jj.shape, _kk.shape)

    coor = self.coor
    t_coor = np.zeros((_nk*_nj*_ni, 3), dtype=np.float32)
    for d in range(3):
      cx = self.coor[:, d]
      cx = np.reshape(cx, newshape=(nk, nj, ni))
      
      if dir_map[faceid] == "i":
        t_cx = cx[:, :, _ii].flatten()
      elif dir_map[faceid] == "j":
        t_cx = cx[:, _jj, :].flatten()
      elif dir_map[faceid] == "k":
        t_cx = cx[_kk, :, :].flatten()
      else:
        raise RuntimeError("Unknown direction")
      
      t_coor[:, d] = t_cx
    #print('>> t_coor',t_coor)

    return t_coor, (_ni, _nj, _nk)


  # Supports Vec with any blocksize
  def extract_subset(self, _irange, _jrange, _krange, sample_rate=(1, 1, 1), cellData=None, pointData=None):
    """
    Equivalent to ParaView "Extract Subset".
    
    When specifying i-, j-, k-ranges, the max index should be one larger than the index of the end point sought.
    E.g, if you wish to extract i-indices [0, 1, 2, 3, 4, 5], use `irange` = [0, 6].
    The extracted fields returned have the same type as the inputs `cellData` and `pointData`.
    Examples:
    (1) If you provided `cellData = [field1, field2]`, the returned cell fields will be a list,
    `cell_output = [sub_field1, sub_field2]`.
    (2) If you provided `cellData = {"f1": field1, "f2": field2}` the returned cell fields will be a dict,
    with identical keys, `cell_output = {"f1": sub_field1, "f2": sub_field2}`.
    
    
    Input
      irange     : The start/end range of indices to extract in the i-direction.
      jrange     : The start/end range of indices to extract in the j-direction.
      krange     : The start/end range of indices to extract in the k-direction.
      sample_rate: Stride to use when extracting vertices.
      cellData   : Cell field to extract (optional).
      pointData  : Vertex field to extract (optional).
      
    Output
      s_coor         : Coordinates of the extracted mesh.
      (_ni, _nj, _nk): Number of vertices in each i, j, k direciton.
      cell_output    : Cell fields for the extracted mesh (optional). None will be returned if `cellData` = None.
      point_output   : Vertex fields for the extracted mesh (optional). None will be returned if `pointData` = None.
    """
    
    mt0 = perf_counter()
    irange = _irange.copy()
    jrange = _jrange.copy()
    krange = _krange.copy()
    
    ni, nj, nk = self.m+1, self.n+1, self.p+1
 
    if irange[1] == -1: irange[1] = ni
    if jrange[1] == -1: jrange[1] = nj
    if krange[1] == -1: krange[1] = nk
 
    ii = np.arange(ni, dtype=np.int32)
    jj = np.arange(nj, dtype=np.int32)
    kk = np.arange(nk, dtype=np.int32)
    
    _ni, _nj, _nk = (irange[1]-irange[0]), (jrange[1]-jrange[0]), (krange[1]-krange[0])
    #print(_ni, _nj, _nk)
    
    _ii = np.linspace(irange[0],irange[1]-1,_ni, dtype=np.int32)
    _ii = _ii[::sample_rate[0]]

    _jj = np.linspace(jrange[0],jrange[1]-1,_nj, dtype=np.int32)
    _jj = _jj[::sample_rate[1]]
    
    _kk = np.linspace(krange[0],krange[1]-1,_nk, dtype=np.int32)
    _kk = _kk[::sample_rate[2]]

    _ni, _nj, _nk = len(_ii), len(_jj), len(_kk)
    #print(_ni, _nj, _nk)
  
    coor = self.coor
    s_coor = np.zeros((_nk*_nj*_ni, 3), dtype=np.float32)
    for d in range(3):
      cx = self.coor[:, d]
      cx = np.reshape(cx, newshape=(nk, nj, ni))
      
      f_cx = cx[krange[0]:krange[1]:sample_rate[2], jrange[0]:jrange[1]:sample_rate[1], irange[0]:irange[1]:sample_rate[0]].flatten()
      
      s_coor[:, d] = f_cx

    # Determine which fields are cell fields and which are vertex fields
    cellDataField = list()
    vertexDataField = list()

    subset_cellDataField, subset_vertexDataField = list(), list()

    if cellData is not None:
      if isinstance(cellData, dict):
        for key in cellData:
          f = cellData[key]
          if self.is_cell_field(f) == True: cellDataField.append(f)
          else:                             raise RuntimeError("Input is not a cell field, (name, size, type) =",key, f.shape, f.dtype)
      elif isinstance(cellData, list):
        for f in cellData:
          if self.is_cell_field(f) == True: cellDataField.append(f)
          else:                             raise RuntimeError("Input is not a cell field, (name, size, type) =",key, f.shape, f.dtype)
      elif isinstance(cellData, np.ndarray):
        f = cellData
        if self.is_cell_field(f) == True: cellDataField.append(f)
        else:                             raise RuntimeError("Input is not a cell field.")
      else:
        raise RuntimeError("cellData input is not valid. cellData must either a dict of fields, a list of fields, or an numpy.ndarray.")

    if pointData is not None:
      if isinstance(pointData, dict):
        for key in pointData:
          f = pointData[key]
          if self.is_vertex_field(f) == True: vertexDataField.append(f)
          else:                               raise RuntimeError("Input is not a vertex field, (size, type) =",f.shape, f.dtype)
      elif isinstance(pointData, list):
        for f in pointData:
          if self.is_vertex_field(f) == True: vertexDataField.append(f)
          else:                               raise RuntimeError("Input is not a vertex field, (size, type) =",f.shape, f.dtype)
      elif isinstance(pointData, np.ndarray):
        f = pointData
        if self.is_vertex_field(f) == True: vertexDataField.append(f)
        else:                               raise RuntimeError("Input is not a vertex field, size is",f.shape)
      else:
        raise RuntimeError("pointData input is not valid. pointData must either a dict of fields, a list of fields, or an numpy.ndarray.")



    if pointData is None and cellData is None:
      return s_coor, (_ni, _nj, _nk)


    for field in vertexDataField:  # vertex field inputs
      
      s_field = None
      if field is not None:
        if len(field.shape) == 1:
          s_field = np.zeros(_nk*_nj*_ni, dtype=field.dtype)
          fr = np.reshape(field, newshape=(nk, nj, ni))
          s_fr = fr[krange[0]:krange[1]:sample_rate[2], jrange[0]:jrange[1]:sample_rate[1], irange[0]:irange[1]:sample_rate[0]].flatten()
          s_field[:] = s_fr
        else:
          s_field = np.zeros((_nk*_nj*_ni, field.shape[1]), dtype=field.dtype)
          for d in range(field.shape[1]):
            fi = field[:, d]
            fir = np.reshape(fi, newshape=(nk, nj, ni))
            s_f = fir[krange[0]:krange[1]:sample_rate[2], jrange[0]:jrange[1]:sample_rate[1], irange[0]:irange[1]:sample_rate[0]].flatten()
            s_field[:, d] = s_f

      subset_vertexDataField.append(s_field)


    for field in cellDataField: # cell field inputs
      mi, mj, mk = self.m, self.n, self.p
      #_mi, _mj, _mk = (_ni+1), (_nj+1), (_nk+1)
      
      irange = _irange.copy()
      jrange = _jrange.copy()
      krange = _krange.copy()

      if irange[1] == -1: irange[1] = ni
      if jrange[1] == -1: jrange[1] = nj
      if krange[1] == -1: krange[1] = nk

      if krange[1] - krange[0] > 1: krange[1] -= 1
      if jrange[1] - jrange[0] > 1: jrange[1] -= 1
      if irange[1] - irange[0] > 1: irange[1] -= 1
      
      _mk = krange[1] - krange[0]
      _mj = jrange[1] - jrange[0]
      _mi = irange[1] - irange[0]
      
      _ii = np.linspace(irange[0],irange[1]-1,_mi, dtype=np.int32)
      _ii = _ii[::sample_rate[0]]
      #print('i',_ii,len(_ii))
      _mi = len(_ii)

      _ii = np.linspace(jrange[0],jrange[1]-1,_mj, dtype=np.int32)
      _ii = _ii[::sample_rate[1]]
      #print('j',_ii,len(_ii))
      _mj = len(_ii)

      _ii = np.linspace(krange[0],krange[1]-1,_mk, dtype=np.int32)
      _ii = _ii[::sample_rate[2]]
      #print('k',_ii,len(_ii))
      _mk = len(_ii)


      #print(irange,jrange,krange, self.m,self.n,self.p)
      s_field = None
      if len(field.shape) == 1:
        s_field = np.zeros(_mk*_mj*_mi, dtype=field.dtype)
        fr = np.reshape(field, newshape=(mk, mj, mi))
        #print(fr.shape)
        s_fr = fr[krange[0]:krange[1]:sample_rate[2], jrange[0]:jrange[1]:sample_rate[1], irange[0]:irange[1]:sample_rate[0]].flatten()
        #print(s_fr.shape, s_field.shape)
        s_field[:] = s_fr

      else:
        s_field = np.zeros((_mk*_mj*_mi, field.shape[1]), dtype=field.dtype)
        for d in range(field.shape[1]):
          fi = field[:, d]
          fir = np.reshape(fi, newshape=(mk, mj, mi))
          s_f = fir[krange[0]:krange[1]:sample_rate[2], jrange[0]:jrange[1]:sample_rate[1], irange[0]:irange[1]:sample_rate[0]].flatten()
          s_field[:, d] = s_f

      subset_cellDataField.append(s_field)

    mt1 = perf_counter()
    dt = mt1 - mt0
    print('->',inspect.currentframe().f_code.co_name + '()','execution time:',('%1.4e'%dt),'(sec)')
    
    # Prepare output
    if cellData is None:
      cell_output = None
    else:
      if isinstance(cellData, dict):
        keys = list(cellData.keys())
        cell_output = dict(zip(keys, subset_cellDataField))
      if isinstance(cellData, list):
        cell_output = subset_cellDataField
      if isinstance(cellData, np.ndarray):
        cell_output = subset_cellDataField[0]


    if pointData is None:
      point_output = None
    else:
      if isinstance(pointData, dict):
        keys = list(pointData.keys())
        point_output = dict(zip(keys, subset_vertexDataField))
      if isinstance(pointData, list):
        point_output = subset_vertexDataField
      if isinstance(pointData, np.ndarray):
        point_output = subset_vertexDataField[0]


    return s_coor, (_ni, _nj, _nk), cell_output, point_output
        
        


  # Supports Vec with any blocksize
  def cell_field_to_point_field(self, field_c, volume_scale=False, **kwargs):
    """
    Equivalent to ParaView filter "Cell Data to Point Data".
    Performs a simple projection to define a vertex field from cell data.
    The projection by default simply computes a vertex value at point i by computing the
    average of the cell field values which touching vertex i.
    A more accurate projection can be used in which we scale each cell contribution by
    the cell volume, e.g. f_i = \sum_c f_c vol(c) / \sum_c vol(c)
    
    Input
      field_c     : Cell field.
      volume_scale: Boolean to activate the cell volume weighting when performing vertex averages.
      nvec        : Number of elements to vectorize over. Performance tuning parameter.
    Output
      field: The vertex field obtained via a P0 projection.
    """
    
    if not self.is_cell_field(field_c):
      raise RuntimeError('Only valid for cell field')

    nvec = 1000 # default
    keys = list(kwargs.keys())
    if "nvec" in keys:
      nvec = kwargs["nvec"]

    _ = self.get_elements()
    
    mt0 = perf_counter()
    bs = vec_get_blocksize(field_c)
    field = self.create_vertex_field(dof=bs)
    adj = self.create_vertex_field(dof=1)

    if volume_scale == True:
      vol = self.get_cell_volume(nvec)
    else:
      vol = self.create_cell_field(dof=1)
      vol[:] = 1.0
    
    ecnt = 0
    while ecnt != self.ne:
      start = ecnt
      end = ecnt + nvec
      if end > self.ne:
        end = self.ne
  
      e_block = np.linspace(start, end-1, (end-start), dtype=np.int32)
      el_block = self.element[e_block]

      phi_cell_block = field_c[e_block]
      vol_block = vol[e_block]

      if bs == 1:
        for i in range(self.basis_per_el):
          field[ el_block[:, i] ] += phi_cell_block * vol_block
          adj[ el_block[:, i] ] += vol_block
      else:
        for i in range(self.basis_per_el):
          field[ el_block[:, i] ] += np.multiply(phi_cell_block, vol_block[:, np.newaxis])
          adj[ el_block[:, i] ] += vol_block

      ecnt += (end - start)
  
    if bs == 1:
      field = field / adj
    else:
      for b in range(bs):
        field[:, b] /= adj[:]
  
    mt1 = perf_counter()
    dt = mt1 - mt0
    print('->',inspect.currentframe().f_code.co_name + '()','execution time:',('%1.4e'%dt),'(sec)')
    return field



  # Deprecated methods
  def create_e2v_fast(self):
    raise DeprecationWarning('Method not supported. Use create_e2v()')
  
  def interp2cell_naive(self, field):
    raise DeprecationWarning('Method not supported. Use interp2cell()')
  
  def interp2cell_vec(self, field, nvec=1000):
    raise DeprecationWarning('Method not supported. Use interp2cell()')
  
  def interpgrad2cell_naive(self, field):
    raise DeprecationWarning('Method not supported. Use interpgrad2cell()')
  
  def interpgrad2cell_vec(self, field, nvec=1000):
    raise DeprecationWarning('Method not supported. Use interpgrad2cell()')
  
  def interpgrad2cell_vec2(self, field, nvec=1000):
    raise DeprecationWarning('Method not supported. Use interpgrad2cell()')
  
  def interp_coarse_to_fine_fast(self, field_c, to_mesh):
    raise DeprecationWarning('Method not supported. Use interp_coarse_to_fine()')

  def compute_derivatives_c(self, field):
    raise DeprecationWarning('Method not supported. Use compute_derivatives()')

