
import os

version = [0, 0, 1]

from . import register

clibvt, clib_ftable = register.load_clib()

#from .PetscBinaryIO import *
#from .petsc import *

from .femesh import FEMeshQ1
from .femeshvec import FEMeshQ1Vec
from .cfemesh import CFEMeshQ1

from .writers import simple_vts_writer
from .writers import simple_vtr_writer

from .ptatin_loaders import *

