
import numpy as np
import sys, os, json
from time import perf_counter
import inspect


class cnumbers:
  int32 = 'i4'
  uint32 = 'u4'
  
  int64 = 'i8'
  uint64 = 'u8'
  
  float32 = 'f4'
  float64 = 'f8'
  
  def __init__(self, endian='little'):
    if   endian == 'little':  self.e = '<'
    elif endian == 'big'   :  self.e = '>'
    else:
      raise RuntimeError('Arg 1 must be either "little" or "big"')
    self.map = {  "float": 'f4', "double": 'f8',
                  "short": 'i2', "int": 'i4',
                  "long int": 'i8', "long long int": 'i16',
                  "unsigned short": 'u2', "unsigned int": 'u4',
                  "unsigned long int": 'u8', "unsigned long long int": 'u16',
                  "bool": '?' }

  def convert(self, type):
    return self.e + self.map[type]


def dtype_MPntStd(endian='little'):
  """
  This uses `offsets` and `itemsize` to avoid the need for manually defining the padding
  """
  ctype = cnumbers(endian)
  mp_type = np.dtype({
                     'names': ['pid', 'coor', 'xi', 'region', 'wil'],
                     'formats': [
                                 ctype.convert("long int"),
                                 (ctype.convert("double"),3),
                                 (ctype.convert("double"),3),
                                 ctype.convert("int"),
                                 ctype.convert("int")
                                 ],
                     'offsets': [0, 8, 32, 56, 60], # obtained from offsetof(struct_name,struct_member)
                     'itemsize': 64, # obtained from sizeof(struct_name)
                     })
  return mp_type


def dtype_MPntPEnergy(endian='little'):
  ctype = cnumbers(endian)
  mp_type = np.dtype({'names': ['diffusivity', 'heat_source'],
                      'formats': [ctype.convert("double"), ctype.convert("double")],
                      'offsets': [0, 8 ], 'itemsize': 16 })
  return mp_type


def dtype_MPntPStokes(endian='little'):
  ctype = cnumbers(endian)
  mp_type = np.dtype({'names': ['eta', 'rho'],
                     'formats': [ctype.convert("double"), ctype.convert("double")],
                     'offsets': [0, 8 ], 'itemsize': 16 })
  return mp_type


def dtype_MPntPStokesPl(endian='little'):
  ctype = cnumbers(endian)
  mp_type = np.dtype({'names': ['e_plastic', 'is_yielding'],
                     'formats': [ctype.convert("float"), ctype.convert("short")],
                     'offsets': [0, 4 ], 'itemsize': 8 })
  return mp_type


class pTatinDataBucket:
  def __init__(self):
    self.npoints = np.int64(0)
    self.npoints_rank = np.zeros(1, dtype=np.int64)
    self.field_name = list()
    self.field_atomic_size = list()
    self.field_filename = list()

    self.dtype_field = dict() # keys -> member names | value -> numpy dtype
    self.data = dict() # keys -> member names | value -> raw data


  def init_data_dtype(self):
    self.dtype_db_field = dict() # keys -> member names | value -> numpy dtype
    self.data = dict() # keys -> member names | value -> raw data
    for f in self.field_name:
      self.dtype_field[f] = None
      self.data[f] = None

  def init_data(self):
    self.data = dict() # keys -> member names | value -> raw data
    for f in self.field_name:
      self.data[f] = None


  def _load_single_datafile(self, datafilename, members=None):
  
    _members = list()
    if members is None: # load everything
      for f in self.field_name:
        _members.append(f)
    else:
      for m in members: # load a subset of the data
        _members.append(m)

    print('[pTatinDataBucket] members',_members)
  
    fp = open(datafilename, "rb")
    byte_offset = 0
    for i in range(len(self.field_name)):
      fieldname = self.field_name[i]
      atomicsize = self.field_atomic_size[i]
      
      print('[pTatinDataBucket] field',fieldname,'--> npoints (domain):',self.npoints)
      
      # need to read header: long int (nbytes)
      header_bytes = np.dtype("int64").itemsize
      
      """
      byte_offset += header_bytes
      if fieldname in _members:
        rcnt = 0
        for np_rank in self.npoints_rank:
          print('  loading [rank',rcnt,'] --> (sub-domanin) npoints:',np_rank)
          print('  byte offset',byte_offset)
          self.data[fieldname] = np.fromfile(fp, dtype=self.dtype_field[fieldname], offset=byte_offset, count=np_rank)
          rcnt += 1
          
          byte_offset = 0
      else:
        byte_offset += atomicsize * self.npoints
      """

      if fieldname in _members:
        _header = np.fromfile(fp, dtype=np.int64, offset=byte_offset, count=1)
        print('[pTatinDataBucket]  loading npoints:', self.npoints, 'byte offset',byte_offset)
        self.data[fieldname] = np.fromfile(fp, dtype=self.dtype_field[fieldname], offset=byte_offset, count=self.npoints)
      else:
        byte_offset += header_bytes
        byte_offset += atomicsize * self.npoints

    fp.close()


  # load out of core
  def _load_single_datafile_out_of_core(self, datafilename, members=None):
    _members = list()
    if members is None: # load everything
      for f in self.field_name:
        _members.append(f)
    else:
      for m in members: # load a subset of the data
        _members.append(m)
    
    print('[pTatinDataBucket] members',_members)
    print('[pTatinDataBucket] memmap',datafilename)
    
    header_bytes = np.dtype("int64").itemsize
    
    data_dict = dict()
    for f in self.field_name:
      data_dict[f] = None

    for fieldname in _members:
      
      byte_offset = 0
      for i in range(len(self.field_name)):
        regfield = self.field_name[i]
        byte_offset += header_bytes
        if fieldname == regfield:
          break
        byte_offset += self.field_atomic_size[i] * self.npoints
      print('[pTatinDataBucket]  loading', fieldname, 'byte offset', byte_offset)
      
      data = np.memmap(datafilename, dtype=self.dtype_field[fieldname], mode='r', offset=byte_offset, shape=self.npoints)
      data_dict[fieldname] = data

    return data_dict


  def load(self, members=None):
    self.init_data()
  
    # check if all files are the same
    same_data_file = True
    for fn in self.field_filename:
      if fn != self.field_filename[0]:
        same_data_file = False
        break
  
    # Extract path and filename
    if same_data_file:
      datafile = self.field_filename[0]
      _datafile = os.path.basename(datafile)
      _path = os.path.dirname(self.json_filename)
      datafilename = os.path.join(_path, _datafile)
        
      self._load_single_datafile(datafilename, members)
      
    else:
      print('No support for loading data fields from different files')
      self.init_data()

    self.is_out_of_core()


  def load_out_of_core(self, members=None):
    self.init_data()

    # check if all files are the same
    same_data_file = True
    for fn in self.field_filename:
      if fn != self.field_filename[0]:
        same_data_file = False
        break
  
    # Extract path and filename
    if same_data_file:
      datafile = self.field_filename[0]
      _datafile = os.path.basename(datafile)
      _path = os.path.dirname(self.json_filename)
      datafilename = os.path.join(_path, _datafile)
      
      self.data = self._load_single_datafile_out_of_core(datafilename, members)
    
    else:
      print('No support for loading data fields from different files')
      self.init_data()
    
    self.is_out_of_core()


  def parse_json(self, json_filename):
    self.json_filename  = json_filename
    
    jfile = open(json_filename, "r")
    js = json.load(jfile)
    jfile.close()

    self.npoints = np.int64(0)
    self.npoints_rank = js["DataBucket"]["partition"]["length"]
    #print(self.npoints_rank)
    #for v in self.npoints_rank:
    #  self.npoints += v
    self.npoints_rank = np.asarray(self.npoints_rank, dtype=np.int64)
    self.npoints = np.sum(self.npoints_rank)

    # Extract the field names and their sizes (in bytes)
    self.field_name = list()
    self.field_atomic_size = list()
    fields = js["DataBucket"]["fields"]
    for f in fields:
      fieldname = f["fieldName"]
      atomicsize = f["atomicSize"]

      self.field_name.append(fieldname)
      self.field_atomic_size.append(atomicsize)

    self.field_filename = list()
    for f in fields:
      filename = f["fileName"]
      self.field_filename.append(filename)

    # initialize dicts()
    self.init_data_dtype()


  def is_out_of_core(self):
    ooc = False
    writeable = True
    for f in self.field_name:
      if self.data[f] is not None:
        writeable = self.data[f].flags.writeable
        if writeable == False:
          ooc = True
          break
    self.out_of_core = ooc
    return ooc



  def __str__(self):
    m = 'fields ' + str(self.field_name)
    m += '\n' + 'npoints ' + str(self.npoints)
    m += '\n' + 'sizeof() ' + str(self.field_atomic_size)
    m += '\n' + 'field file ['
    for v in self.field_filename:
      m += '\n  ' + str(v)
    m += '  ]'
    m += '\n' + 'dtypes {'
    for k in self.dtype_field.keys():
      m += '\n  ' +  '\"'+str(k)+'\" '  + str(self.dtype_field[k])
    m += '  }'

    m += '\n' + 'paritioned points ' + str(self.npoints_rank)
    try:
      m += '\n' + 'out-of-core ' + str(self.out_of_core)
    except:
      m += '\n' + 'out-of-core <unknown as data has not been loaded>'
    
    return m


class pTatinSwarmDB:

  def __init__(self, known_file_endian=None):
    self.db = pTatinDataBucket()

    if known_file_endian is None:
      print('[pTatinSwarmDB] Endianness was not provided.')
      print('[pTatinSwarmDB] Assuming DataBucket files were written on the current machine.')
      known_file_endian = sys.byteorder
      print('[pTatinSwarmDB] Using endian type:',known_file_endian)
    self.known_file_endian = known_file_endian
    
    self.dtype_swarm = dict()
    self.dtype_swarm["MPntStd"]       = dtype_MPntStd(known_file_endian)
    self.dtype_swarm["MPntPEnergy"]   = dtype_MPntPEnergy(known_file_endian)
    self.dtype_swarm["MPntPStokes"]   = dtype_MPntPStokes(known_file_endian)
    self.dtype_swarm["MPntPStokesPl"] = dtype_MPntPStokesPl(known_file_endian)

    self.npoints = np.int64(0)
    self.mask = None


  @property
  def data(self):
    return self.db.data


  def load(self, json_file, members=None, out_of_core=False):
    self.db.parse_json(json_file)
    
    # insert custom data types
    for k in self.dtype_swarm:
      self.db.dtype_field[k] = self.dtype_swarm[k]
    
    if out_of_core:
      self.db.load_out_of_core(members)
    else:
      self.db.load(members)

    self.npoints = self.db.npoints


  def create_mask(self):
    mask = np.linspace(0, self.npoints-1, self.npoints, np.bool_)
    mask[:] = False
    return mask


  def get_mask(self):
    if self.mask is None:
      self.mask = np.linspace(0, self.npoints-1, self.npoints, np.bool_)
      self.mask[:] = False
    return self.mask


  def reset_mask(self, mask=None):
    if mask is None:
      mask = self.get_mask()
    mask[:] = False


  def extract_subset_from_mask(self, mask=None):
    """
    Results in an in-core object, even if `self` is out-of-core.
    The subset defines all fields loaded in `self`.
    """


    print('[extract_subset_from_mask] DataBucket data is writable:',not self.db.out_of_core)

    if mask is None:
      mask = self.get_mask()
    
    _idx = np.nonzero(mask == True)
    idx = _idx[0]
    
    swarm = pTatinSwarmDB(known_file_endian=self.known_file_endian)
    db = swarm.db

    db.npoints           = len(idx)
    db.npoints_rank      = None # This could be determined by traversing rank-wise and apply a count on mask[slice]
    db.field_name        = self.db.field_name
    db.field_atomic_size = self.db.field_atomic_size
    db.field_filename    = self.db.field_filename

    db.init_data_dtype()

    # insert custom data types
    for k in self.dtype_swarm:
      db.dtype_field[k] = self.dtype_swarm[k]


    build_npoints_rank = True
    if build_npoints_rank:
      nranks = len(self.db.npoints_rank)
      db.npoints_rank = np.zeros(nranks, dtype=np.int64)
      start = 0
      for r in range(nranks):
        end = start + self.db.npoints_rank[r]
        
        mask_rank = mask[start:end]
        cnt = np.count_nonzero(mask_rank == True)
        db.npoints_rank[r] = cnt
        
        start += self.db.npoints_rank[r]


    # in-core
    if self.db.out_of_core == False:
      # traverse fields, extract points identify by mask[] = True
      print('[extract_subset_from_mask] extract all points at once')
      for f in self.db.field_name:
        if self.db.data[f] is not None:
          psub = self.db.data[f][idx]
          db.data[f] = psub

    else: # out-of-core batch insertition
      print('[extract_subset_from_mask] batch extraction rank-wise (out-of-core)')
      # traverse fields, extract points identify by mask[] = True
      for f in self.db.field_name:
        if self.db.data[f] is not None:

          db.data[f] = np.zeros(db.npoints, dtype=db.dtype_field[f])
          
          nranks = len(self.db.npoints_rank)
          start = 0
          pcnt = 0
          for r in range(nranks):
            end = start + self.db.npoints_rank[r]
            mask_rank = mask[start:end]
            _idx = np.nonzero(mask_rank == True)
            idx = _idx[0] + start

            db.data[f][pcnt:(pcnt+db.npoints_rank[r])] = self.db.data[f][idx]

            start += self.db.npoints_rank[r]
            pcnt += db.npoints_rank[r]

    db.is_out_of_core()
  
    swarm.npoints = db.npoints
    
    return swarm


  def extract_subset_from_region(self, select):
    region_mask = np.linspace(0, self.npoints-1, self.npoints, np.bool_)
    region_mask[:] = False
    region = self.db.data["MPntStd"]["region"]
    for m in select:
      _idx = np.nonzero(region == m)
      region_mask[_idx] = True
    swarm = self.extract_subset_from_mask(mask=region_mask)
    return swarm


  def filter_iterator(self, operation, mask=None, batch_size=1000, **kwargs):
    if mask is None:
      mask = self.get_mask()

    chunksize = np.int64(batch_size)
    if chunksize > self.npoints:
      chunksize = self.npoints
    nchunks = np.int64(self.npoints / chunksize)
    rem = self.npoints - chunksize * nchunks
    if rem > 0:
      nchunks += 1

    muser_dt = 0.0
    mt0 = perf_counter()
    for c in range(nchunks):
      start = c * chunksize
      end  = start + chunksize
      if end > self.npoints:
        end = self.npoints

      # Fetch a chunk of point data
      indices = np.linspace(start, end-1, end-start, dtype=np.int64)
      #mask_chunk = mask[indices] # copy
      mask_chunk = mask[start:end] # view (appears to be much faster)
      
      points_chunk = dict()
      for f in self.db.field_name:
        points_chunk[f] = None
      for f in self.db.field_name:
        if self.db.data[f] is not None:
          #points_chunk[f] = np.zeros(end-start, dtype=self.db.dtype_field[f]) # allocate and copy
          #points_chunk[f][:] = self.db.data[f][indices]
          points_chunk[f] = self.db.data[f][start:end] # view

      try:
        user_mt0 = perf_counter()
        operation(indices, points_chunk, mask_chunk, **kwargs)
        user_mt1 = perf_counter()
        muser_dt += (user_mt1 - user_mt0)
      except:
        msg = "Error calling user call-back"
        msg += '\n' + "Expected signature is"
        msg += '\n' + "  operation(indices, points, mask, kwargs)"
        msg += '\n' + "where"
        msg += '\n' + "  `indices` defines the index of each point associated with `self.db` [numpy.ndarray(batch_size, dtype = numpy.int64)]."
        msg += '\n' + "  `points` are the swarm points associated with a buffer of length `batch_size`."
        msg += '\n' + "  `mask` is mask array associated with the buffer [numpy.ndarray(batch_size, dtype = numpy._bool)]."
        raise RuntimeError(msg)
          
       # only required if we copied mask values
#      mask[indices] = mask_chunk[:]

    mt1 = perf_counter()
    dt = mt1 - mt0
    print('->',inspect.currentframe().f_code.co_name + '()','execution time:',('%1.4e'%dt),'(sec)')
    print('  ->',inspect.currentframe().f_code.co_name + '(user call-back)','execution time:',('%1.4e'%muser_dt),'(sec)')


  def filter_iterator_set(self, operation, maskSet, batch_size=1000, **kwargs):
    
    chunksize = np.int64(batch_size)
    if chunksize > self.npoints:
      chunksize = self.npoints
    nchunks = np.int64(self.npoints / chunksize)
    rem = self.npoints - chunksize * nchunks
    if rem > 0:
      nchunks += 1
  
    muser_dt = 0.0
    mt0 = perf_counter()
    for c in range(nchunks):
      start = c * chunksize
      end  = start + chunksize
      if end > self.npoints:
        end = self.npoints
  
      # Fetch a chunk of point data
      indices = np.linspace(start, end-1, end-start, dtype=np.int64)

      points_chunk = dict()
      for f in self.db.field_name:
        points_chunk[f] = None
      for f in self.db.field_name:
        if self.db.data[f] is not None:
          #points_chunk[f] = np.zeros(end-start, dtype=self.db.dtype_field[f]) # 1/ allocate and copy
          #points_chunk[f][:] = self.db.data[f][indices]
          #points_chunk[f] = self.db.data[f][indices] # 2/ alloc copy in one line
          points_chunk[f] = self.db.data[f][start:end] # view

      if c == 0:
        for f in self.db.field_name:
          if self.db.data[f] is not None:
            writeable = points_chunk[f].flags.writeable
            print('  +',f,'writeable',writeable)

      maskSet_chunk = list()
      for mask in maskSet:
        #mask_chunk = mask[indices] # copy
        mask_chunk = mask[start:end] # view
        maskSet_chunk.append(mask_chunk)

      try:
        user_mt0 = perf_counter()
        operation(indices, points_chunk, maskSet_chunk, **kwargs)
        user_mt1 = perf_counter()
        muser_dt += (user_mt1 - user_mt0)
      except:
        msg = "Error calling user call-back"
        msg += '\n' + "Expected signature is"
        msg += '\n' + "  operation(indices, points, maskSet, kwargs)"
        msg += '\n' + "where"
        msg += '\n' + "  `indices` defines the index of each point associated with `self.db` [numpy.ndarray(batch_size, dtype = numpy.int64)]."
        msg += '\n' + "  `points` are the swarm points associated with a buffer of length `batch_size`."
        msg += '\n' + "  `maskSet` is list of mask arrays associated with the buffer [numpy.ndarray(batch_size, dtype = numpy._bool)]."
        raise RuntimeError(msg)

       # only required if we copied mask values
#      for mk in range(len(maskSet)):
#        mask = maskSet[mk]
#        mask_chunk = maskSet_chunk[mk]
#        mask[indices] = mask_chunk[:]

    mt1 = perf_counter()
    dt = mt1 - mt0
    print('->',inspect.currentframe().f_code.co_name + '()','execution time:',('%1.4e'%dt),'(sec)')
    print('  ->',inspect.currentframe().f_code.co_name + '(user call-back)','execution time:',('%1.4e'%muser_dt),'(sec)')


#
# Define mask using regionid defined by target
# target can be a list, or list of list
#
def SwarmMask_Threshold_region_index(swarm, target):
  _target = list()
  if isinstance(target, np.int):
    _target.append(target)
  elif isinstance(target, range):
    for i in target:
      _target.append(i)
    _target.append(i+1)
  elif isinstance(target, list):
    for i in target:
      _target.append(i)
  else:
    print('Expected a list defining an interval')

  print('[SwarmMask_Threshold_region_index] target',_target)

  data = swarm.data
  swarm.get_mask()
  region = data["MPntStd"]["region"]
  for m in _target:
    _idx = np.nonzero(region == m)
    swarm.mask[_idx] = True

  cnt = np.count_nonzero(swarm.mask == True)
  print('[SwarmMask_Threshold_region_index] number of points found',cnt)


