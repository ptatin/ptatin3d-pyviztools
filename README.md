# Usage

## Use case 1

If you are analyzing pTatin3D output on the same machine
where you ran the simulations, you should follow these steps.

In the following `>` indicates the command should be executed on the terminal.

```
> git clone https://bitbucket.org/ptatin/ptatin3d-pyviztools
> export PYTHONPATH=${PWD}:${PYTHONPATH}

> export PETSC_DIR=/path/to/your/petsc
> export PETSC_ARCH=name-of-your-arch
> export PYTHONPATH=${PETSC_DIR}/lib/petsc/bin:${PYTHONPATH}
```

## Use case 2

If you generated pTatin3D output on "Machine X" (e.g. a cluster)
and you are analysing the data on "Machine Y" (e.g. a local machine, for example your laptop), you should follow these steps.

Note these steps should be followed even if you have a build of
PETSc on the machine where you are conducting the analysis.

In the following `>` indicates the command should be executed on the terminal. `>>>` indicates the command should be executed within the Python interpretter.

```
> git clone https://bitbucket.org/ptatin/ptatin3d-pyviztools
> export PYTHONPATH=${PWD}:${PYTHONPATH}

> python3
>>> from pyviztools import petsc
>>> lib = petsc.PETSc()
>>> lib.configure('real','double','int')
>>> lib.emit_conf()
```

### Notes

Below, the words "PETSc build" refers to the build of PETSc on "Machine X".

* If PETSc on "Machine X" was configured to use complex numbers, change `'real'` to `'complex'`.
* If PETSc on "Machine X" was configured to use anything other than double precision, change `'double'` to one of `'float'`, `'__float128'`, `'__fp16'`.
* If PETSc on "Machine X" was configured to use 64-bit integers, change `'int'` to `'long int'`.


# Notes

* The class `FEMesh` is pure Python. Some of the methods may be slow. Some methods have been vectorized in Python and maybe found in `FEMeshQ1Vec`. The most efficient implementation is `CFEMeshQ1` which replaces many Python method with C function calls.
* The class `CFEMeshQ1` is a mix of Python and C. Using this class requires you compile a small C library.
    1. Go here `ptatin3d-pyviztools/pyviztools/c`
    2. Type `make`. If that doesn't work, you will have to edit `Makefile`. Likely you need to twiddle with the manner in which `.so` libraries are created.
    3. When `pyviztools` is imported, the relevant C functions will be plucked from the C library.



